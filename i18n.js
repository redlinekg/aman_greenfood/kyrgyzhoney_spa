const NextI18Next = require("next-i18next").default;

module.exports = new NextI18Next({
    defaultLanguage: "ru",
    otherLanguages: ["en", "kg", "cn"],
    localeSubpaths: {
        en: "en",
        kg: "kg",
        ru: "ru",
        cn: "cn"
    }
});
