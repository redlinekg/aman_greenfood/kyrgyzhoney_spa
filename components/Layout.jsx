import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";

import { withTranslation } from "../i18n";
import { withLayout } from "../utils/LayoutProvider";
import { Footer, Header, MobileMenu } from "./index";
import { down, global } from "../config/var";

class Layout extends PureComponent {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        //header
        header_background_image: PT.string,
        header_background_pattern: PT.bool,
        header_sub_menu: PT.object,
        header_navigation: PT.object,
        header_type: PT.string,
        //global
        show_content_padding: PT.bool,
        dark_theme: PT.bool,
        //footer
        show_footer_margin: PT.bool,
        show_footer: PT.bool,
        //
        children: PT.any
    };

    render() {
        const {
            header_background_image,
            header_background_pattern,
            header_sub_menu,
            header_navigation,
            header_type,
            show_content_padding,
            dark_theme,
            show_footer_margin,
            show_footer,
            children
        } = this.props;
        return (
            <SLayout>
                <Header
                    dark_theme={dark_theme}
                    header_background_image={header_background_image}
                    header_background_pattern={header_background_pattern}
                    header_sub_menu={header_sub_menu}
                    header_navigation={header_navigation}
                    header_type={header_type}
                />
                <MobileMenu />
                <SContent
                    className={CN({
                        show_content_padding: show_content_padding,
                        header_sub_menu: header_sub_menu
                    })}
                >
                    {children}
                </SContent>
                {show_footer ? (
                    <Footer
                        dark_theme={dark_theme}
                        show_footer_margin={show_footer_margin}
                        show_footer={show_footer}
                    />
                ) : null}
            </SLayout>
        );
    }
}

const SLayout = styled.div`
    ${down("sm")} {
    }
`;

const SContent = styled.div`
    &.show_content_padding {
        padding: ${global.padding} 0 0;
        ${down("md")} {
            padding: ${global.padding_large} 0 0;
        }
        ${down("sm")} {
            padding: ${global.padding_small} 0 0;
        }
        &.header_sub_menu {
            padding: 5em 0 0;
            ${down("md")} {
                padding: ${global.padding_large} 0 0;
            }
            ${down("sm")} {
                padding: ${global.padding_small} 0 0;
            }
        }
    }
`;

export default withTranslation("common")(withLayout(Layout));
