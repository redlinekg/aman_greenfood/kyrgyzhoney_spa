import React from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";
import { LightgalleryItem } from "react-lightgallery";

import { ImageBg } from "~/components";
import { down, colors } from "~/config/var";

const ProductGalleryItem = ({ image, title }) => (
    <LightgalleryItem group="product_gallery" src={image}>
        <SProductGalleryItem title="product_gallery">
            <SImg>
                <ImageBg
                    src={image}
                    alt={title}
                    width={320}
                    height={320}
                    object_fit="contain"
                    sizes={{
                        lg: {
                            width: 250,
                            height: 250
                        }
                    }}
                />
            </SImg>
        </SProductGalleryItem>
    </LightgalleryItem>
);

ProductGalleryItem.propTypes = {
    image: PT.any,
    title: PT.string
};

const SProductGalleryItem = styled.div`
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 100%;
    display: block;
    background-color: ${colors.blue_light3};
    ${down("md")} {
    }
`;

const SImg = styled.figure`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    width: 65%;
    height: 80%;
`;

export default ProductGalleryItem;
