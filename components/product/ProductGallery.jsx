import React, { Component, createRef } from "react";
import styled from "@emotion/styled";
import Slider from "react-slick";
import { position } from "polished";
import { isBrowser } from "browser-or-node";

import { ArrowLeftIcon, ArrowRightIcon } from "~/components/icons";
import { ProductGalleryItem, ProductGalleryNav } from "~/components";
import PT from "prop-types";
import { down, colors } from "~/config/var";

const nextArrow = (
    <div className="slick-prev">
        <ArrowLeftIcon />
    </div>
);
const prevArrow = (
    <div className="slick-next">
        <ArrowRightIcon />
    </div>
);

const SETTINGS = {
    dots: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    afterChange: current => {
        this.setState({ slideIndex: current });
    },
    adaptiveHeight: false,
    prevArrow: nextArrow,
    nextArrow: prevArrow
};

class ProductGallery extends Component {
    static propTypes = {
        images: PT.array.isRequired,
        video: PT.object
    };
    constructor(props) {
        super(props);
        this.state = {
            slideIndex: 0
        };
    }
    slider = createRef();

    componentDidMount() {
        if (isBrowser && this.props.video) {
            const _video = document.getElementById("product_video");
            _video.play();
        }
    }

    render() {
        const { images, video } = this.props;
        return (
            <SProductGallery>
                <Slider
                    ref={this.slider}
                    {...SETTINGS}
                    afterChange={c =>
                        this.setState({
                            slideIndex: c
                        })
                    }
                >
                    {images.map(image => (
                        <ProductGalleryItem
                            key={image.id}
                            image={image.url_path}
                        />
                    ))}
                    {video ? (
                        <SVideo>
                            <video
                                id="product_video"
                                autoPlay
                                loop
                                muted
                                playsInline
                            >
                                <source
                                    src={video.absolute_path}
                                    type="video/mp4"
                                />
                            </video>
                        </SVideo>
                    ) : null}
                </Slider>
                {images.length === 1 ? null : (
                    <SNav>
                        {images.map((image, idx) => (
                            <ProductGalleryNav
                                onClick={() =>
                                    this.slider.current.slickGoTo(idx)
                                }
                                key={image.id}
                                image={image.url_path}
                                title={image.id + ""}
                                active={this.state.slideIndex == idx}
                            />
                        ))}
                        {video ? (
                            <ProductGalleryNav
                                onClick={() =>
                                    this.slider.current.slickGoTo(
                                        images.length + 1
                                    )
                                }
                                key={0}
                                title="Video"
                                active={
                                    this.state.slideIndex == images.length + 1
                                }
                            />
                        ) : null}
                    </SNav>
                )}
            </SProductGallery>
        );
    }
}

const SProductGallery = styled.div`
    position: sticky;
    top: 5em;
    ${down("md")} {
    }
    .slick-current {
        z-index: 10;
    }
`;

const SVideo = styled.div`
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 100%;
    display: block;
    background-color: ${colors.blue_light3};
    ${down("md")} {
    }
    ${down("lg")} {
        width: 240px;
        height: 240px;
    }
    ${down("sm")} {
        width: 280px;
        height: 280px;
    }
    video {
        ${position("absolute", "50%", null, null, "50%")};
        transform: translateX(-50%) translateY(-50%);
        width: 65%;
        height: 80%;
    }
`;

const SNav = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 1em;
`;

const SNavIcon = styled.div``;

export default ProductGallery;
