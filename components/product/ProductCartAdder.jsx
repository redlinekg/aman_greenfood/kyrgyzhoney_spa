import styled from "@emotion/styled";
import PT from "prop-types";
import { compose } from "ramda";
import React, { useState } from "react";
import { QuantityInput } from "~/components";
import { CartIcon } from "~/components/icons";
import { colors } from "~/config/var";
import { withTranslation } from "~/i18n";

const DEFAULT_COUNT = 10;

let ProductCartAdder = ({ t, onAdd, onChange }) => {
    const [count, _setCount] = useState(DEFAULT_COUNT);
    const setCount = value => {
        _setCount(value);
        onChange(value);
    };
    return (
        <>
            <SQuality>
                <QuantityInput
                    onChange={value => setCount(value)}
                    onIncrease={() => setCount(count + DEFAULT_COUNT)}
                    onDecrease={() => {
                        const next_value = count - DEFAULT_COUNT;
                        setCount(Math.max(next_value, 0));
                    }}
                    value={count}
                    min={1}
                    max={99999}
                />
            </SQuality>
            <SButton
                onClick={() => {
                    setCount(DEFAULT_COUNT);
                    onAdd(count);
                }}
            >
                <CartIcon />
                <span>{t("product_add_cart")}</span>
            </SButton>
        </>
    );
};

ProductCartAdder.propTypes = {
    t: PT.func.isRequired,
    onAdd: PT.func.isRequired,
    onChange: PT.func.isRequired
};

ProductCartAdder = compose(withTranslation("common"))(ProductCartAdder);

const SQuality = styled.div``;
const SButton = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 0.8em;
    cursor: pointer;
    svg {
        width: 1.4em;
        height: 1.4em;
    }
    span {
        padding-left: 0.5em;
        font-weight: 500;
    }
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
`;

export default ProductCartAdder;
