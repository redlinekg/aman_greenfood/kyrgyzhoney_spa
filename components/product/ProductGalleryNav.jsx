import React from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";

import { VideoIcon } from "~/components/icons";
import { ImageBg } from "~/components";
import { down, colors } from "~/config/var";

const ProductGalleryNav = ({ image, active, title, onClick }) => (
    <SProductGalleryNav
        className={CN({
            active: active
        })}
        onClick={onClick}
    >
        <SBlock>
            <SWrap>
                {image ? (
                    <SImg>
                        <ImageBg
                            src={image}
                            alt={title}
                            width={100}
                            height={100}
                            object_fit="contain"
                            sizes={{
                                lg: {
                                    width: 100,
                                    height: 100
                                },
                                md: {
                                    width: 200,
                                    height: 200
                                },
                                sm: {
                                    width: 100,
                                    height: 100
                                }
                            }}
                        />
                    </SImg>
                ) : (
                    <SIcon>
                        <VideoIcon />
                    </SIcon>
                )}
            </SWrap>
        </SBlock>
    </SProductGalleryNav>
);

ProductGalleryNav.propTypes = {
    image: PT.any,
    active: PT.bool,
    title: PT.string,
    onClick: PT.func
};
const SProductGalleryNav = styled.div`
    padding: 0 0.5em;
    width: 25%;
`;

const SBlock = styled.div`
    text-align: center;
    width: 100%;
    padding: 2px;
    opacity: 0.5;
    transition: all 0.2s;
    cursor: pointer;
    color: ${colors.grey_light};
    @media (hover) {
        &:hover {
            box-shadow: inset 0 0 0 2px ${colors.grey_light2};
            opacity: 0.8;
        }
    }
    ${SProductGalleryNav}.active & {
        box-shadow: inset 0 0 0 2px ${colors.black_one};
        opacity: 1;
        color: ${colors.black};
    }
`;

const SWrap = styled.figure`
    background-color: ${colors.blue_light3};
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 100%;
    display: block;
    ${down("md")} {
    }
`;
const SImg = styled.div`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    width: 65%;
    height: 80%;
`;

const SIcon = styled.div`
    svg {
        ${position("absolute", "50%", null, null, "50%")};
        transform: translateX(-50%) translateY(-50%);
        width: 2em;
        height: 2em;
        ${down("md")} {
            width: 1.4em;
            height: 1.4em;
        }
    }
`;

export default ProductGalleryNav;
