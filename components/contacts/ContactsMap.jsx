/*global google*/
import React, { PureComponent } from "react";
import styled from "@emotion/styled";
import { position } from "polished";
import { GoogleApiWrapper, Map, Marker } from "google-maps-react";

import marker_svg from "~/static/images/marker.svg";
import { down, up, colors } from "~/config/var";

@GoogleApiWrapper({
    apiKey: "AIzaSyAY--MosOQGXAr8K6lvNdUAUD_wYnS7AvM"
})
class ContactsMap extends PureComponent {
    state = {
        activeMarker: {},
        selectedPlace: {},
        showingInfoWindow: false
    };

    onMarkerClick = (props, marker) =>
        this.setState({
            activeMarker: marker,
            selectedPlace: props,
            showingInfoWindow: true
        });

    onInfoWindowClose = () =>
        this.setState({
            activeMarker: null,
            showingInfoWindow: false
        });

    onMapClicked = () => {
        if (this.state.showingInfoWindow)
            this.setState({
                activeMarker: null,
                showingInfoWindow: false
            });
    };

    get styles() {
        return [
            {
                featureType: "landscape",
                elementType: "all",
                stylers: [
                    { hue: "#ffa200" },
                    { saturation: "84" },
                    { lightness: -34.8 },
                    { gamma: 1 }
                ]
            },
            {
                featureType: "poi",
                elementType: "all",
                stylers: [
                    { hue: "#FFC300" },
                    { saturation: 54.2 },
                    { lightness: -14.4 },
                    { gamma: 1 }
                ]
            },
            {
                featureType: "road.highway",
                elementType: "all",
                stylers: [
                    { hue: "#FFAD00" },
                    { saturation: -19.8 },
                    { lightness: -1.8 },
                    { gamma: 1 }
                ]
            },
            {
                featureType: "road.arterial",
                elementType: "all",
                stylers: [
                    { hue: "#FFAD00" },
                    { saturation: 72.4 },
                    { lightness: -32.6 },
                    { gamma: 1 }
                ]
            },
            {
                featureType: "road.local",
                elementType: "all",
                stylers: [
                    { hue: "#FFAD00" },
                    { saturation: 74.4 },
                    { lightness: -18 },
                    { gamma: 1 }
                ]
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [
                    { hue: "#00FFA6" },
                    { saturation: -63.2 },
                    { lightness: 38 },
                    { gamma: 1 }
                ]
            }
        ];
    }

    render() {
        return (
            <SContactsMap>
                <SMap
                    // eslint-disable-next-line react/prop-types
                    google={this.props.google}
                    mapTypeControl={false}
                    zoom={4}
                    initialCenter={{
                        lat: 42.882991,
                        lng: 74.582258
                    }}
                    styles={this.styles}
                >
                    <Marker
                        icon={{
                            url: marker_svg,
                            anchor: new google.maps.Point(20, 25),
                            scaledSize: new google.maps.Size(40, 50)
                        }}
                        position={{
                            lat: 42.882991,
                            lng: 74.582258
                        }}
                    />
                </SMap>
            </SContactsMap>
        );
    }
}

const SContactsMap = styled.div`
    /* pointer-events: all; */
    width: 100%;
    ${up("lg")} {
        ${position("fixed", "0", "0", "0", "0")};
        height: 100%;
        z-index: 1;
    }
    ${down("md")} {
        height: 50vh;
        position: relative;
    }
    ${down("sm")} {
        height: 50vh;
    }
    background-color: ${colors.grey};
`;

const SMap = styled(Map)``;

export default ContactsMap;
