import styled from "@emotion/styled";
import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import { ContactsItem } from "~/components/index";
import { colors, down, global } from "~/config/var";

@inject("contacts_store")
@observer
class ContactsList extends Component {
    static propTypes = {
        contacts_store: PT.object.isRequired
    };
    render() {
        const {
            contacts_store: { objects }
        } = this.props;
        return (
            <>
                <SContactsList>
                    <SGlobal>
                        {objects
                            .filter(i => i.global === true)
                            .map(i => (
                                <ContactsItem key={i.id} item={i} />
                            ))}
                    </SGlobal>
                    <SOther>
                        {objects
                            .filter(i => i.global === false)
                            .map(i => (
                                <ContactsItem key={i.id} item={i} />
                            ))}
                    </SOther>
                </SContactsList>
            </>
        );
    }
}

const SContactsList = styled.div`
    width: 30%;
    position: relative;
    z-index: 2;
    pointer-events: all;
    min-width: 370px;
    pointer-events: all;
    ${down("lg")} {
    }
    ${down("md")} {
        min-width: auto;
        width: 100%;
    }
`;

const SGlobal = styled.div`
    padding: 2em;
    background-color: ${colors.white};
    ${down("md")} {
        padding: 15px 0 1em;
    }
    ${down("sm")} {
        padding: 15px;
    }
`;

const SOther = styled.div`
    padding: 2em;
    background-color: ${colors.blue};
    ${down("md")} {
        padding: 1.5em;
    }
    ${down("sm")} {
        padding: 15px;
        padding-bottom: ${global.padding};
    }
`;

export default ContactsList;
