import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";

import { colors, down } from "~/config/var";

class ContactsItem extends PureComponent {
    static propTypes = {
        item: PT.object
    };
    render() {
        const {
            item: { global, title, items }
        } = this.props;
        return (
            <>
                <SContactsItem
                    className={CN({
                        global: global === true
                    })}
                >
                    {global === false && <STitle>{title}</STitle>}
                    <SBlock>
                        {items.map((item, idx) => {
                            switch (item.type) {
                                case "text":
                                    return (
                                        <SItem key={idx + "text"}>
                                            <SText>{item.text}</SText>
                                        </SItem>
                                    );
                                case "link":
                                    return (
                                        <SItem key={idx + "link"}>
                                            <SLink href={`${item.text}`}>
                                                {item.text}
                                            </SLink>
                                        </SItem>
                                    );
                                case "phone":
                                    return (
                                        <SItem key={idx + "phone"}>
                                            <SLink href={`tel:${item.text}`}>
                                                {item.text}
                                            </SLink>
                                        </SItem>
                                    );
                                case "email":
                                    return (
                                        <SItem key={idx + "email"}>
                                            <SLink href={`email:${item.text}`}>
                                                {item.text}
                                            </SLink>
                                        </SItem>
                                    );
                            }
                        })}
                    </SBlock>
                </SContactsItem>
            </>
        );
    }
}

const SContactsItem = styled.div`
    margin-bottom: 1.2em;
    &:last-of-type {
        margin-bottom: 0;
    }
    &.global {
    }
    ${down("md")} {
    }
`;
const STitle = styled.div`
    margin-bottom: 0.4em;
    font-weight: 600;
    color: ${colors.white};
    ${SContactsItem}.global & {
        color: ${colors.black};
    }
`;
const SBlock = styled.div``;

const SItem = styled.div`
    margin-bottom: 0.2em;
`;

const SText = styled.div`
    color: ${colors.grey_light4};
    ${SContactsItem}.global & {
        color: ${colors.grey};
        margin-top: 1em;
    }
`;
const SLink = styled.a`
    color: ${colors.grey_light4};
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
    ${SContactsItem}.global & {
        font-size: 1.2em;
        font-weight: 600;
        color: ${colors.black};
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
        ${down("md")} {
            font-size: 1em;
        }
    }
`;

export default ContactsItem;
