import React, { PureComponent } from "react";

class HoneycombIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16.163"
                height="14"
                viewBox="0 0 16.163 14"
            >
                <g transform="translate(-1927.814 -404.347)">
                    <path
                        d="M220.871,187.18h-8.087l-4.043,6.995,4.043,7.005h8.087l4.033-7.005Z"
                        transform="translate(1719.074 217.167)"
                        fill="currentColor"
                    />
                </g>
            </svg>
        );
    }
}

export default HoneycombIcon;
