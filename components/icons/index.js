//global
export { default as CartIcon } from "./CartIcon";
export { default as CrossIcon } from "./CrossIcon";
export { default as HoneycombIcon } from "./HoneycombIcon";
export { default as PhoneIcon } from "./PhoneIcon";
export { default as PlanetIcon } from "./PlanetIcon";
export { default as UserIcon } from "./UserIcon";
export { default as LogoIcon } from "./LogoIcon";
export { default as NewwindowIcon } from "./NewwindowIcon";
export { default as VideoIcon } from "./VideoIcon";
export { default as PricelistIcon } from "./PricelistIcon";
//arrows
export { default as ArrowBottomIcon } from "./arrows/ArrowBottomIcon";
export { default as ArrowLeftIcon } from "./arrows/ArrowLeftIcon";
export { default as ArrowRightIcon } from "./arrows/ArrowRightIcon";
export { default as ArrowTopIcon } from "./arrows/ArrowTopIcon";
export { default as ThinArrowBottomIcon } from "./arrows/ThinArrowBottomIcon";
export { default as ThinArrowLeftIcon } from "./arrows/ThinArrowLeftIcon";
export { default as ThinArrowRightIcon } from "./arrows/ThinArrowRightIcon";
export { default as ThinArrowTopIcon } from "./arrows/ThinArrowTopIcon";
//social
export { default as InstagramIcon } from "./social/InstagramIcon";
export { default as SkypeIcon } from "./social/SkypeIcon";
export { default as TelegramIcon } from "./social/TelegramIcon";
export { default as TwitterIcon } from "./social/TwitterIcon";
export { default as VkIcon } from "./social/VkIcon";
export { default as WhatsappIcon } from "./social/WhatsappIcon";
export { default as FacebookIcon } from "./social/FacebookIcon";
