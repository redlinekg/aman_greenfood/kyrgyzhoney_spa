import React, { PureComponent } from "react";

class UserIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13.812"
                height="14"
                viewBox="0 0 13.812 14"
            >
                <path
                    d="M13.506,13.429c-2.333-.443-2.333-1.167-2.333-1.633v-.28a4.246,4.246,0,0,0,1.213-2.053h.023c.7,0,.887-1.47.887-1.7s.023-1.1-.7-1.1c1.517-4.2-2.566-5.88-5.646-3.733-1.26,0-1.377,1.867-.91,3.733-.723,0-.7.887-.7,1.1s.163,1.7.887,1.7A4.246,4.246,0,0,0,7.44,11.516v.28c0,.467,0,1.213-2.333,1.633A3.383,3.383,0,0,0,2.4,16H16.212a3.43,3.43,0,0,0-2.706-2.566Z"
                    transform="translate(-2.4 -1.996)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default UserIcon;
