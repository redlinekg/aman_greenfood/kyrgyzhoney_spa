import React, { PureComponent } from "react";

class FacebookIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 10.262 20.523"
            >
                <path
                    fill="currentColor"
                    d="M23.632,3.912h2.63V0H23.151C19.4.16,18.63,2.277,18.565,4.489V6.446H16v3.816h2.565V20.523h3.848V10.262h3.175L26.2,6.446H22.413V5.259a1.271,1.271,0,0,1,1.219-1.347Z"
                    transform="translate(-16)"
                />
            </svg>
        );
    }
}

export default FacebookIcon;
