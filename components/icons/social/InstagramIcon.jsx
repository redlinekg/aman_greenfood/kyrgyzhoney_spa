import React, { PureComponent } from "react";

class InstagramIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 20.152 20.151"
            >
                <g transform="translate(0 0)">
                    <path
                        fill="currentColor"
                        d="M20.6,16.7a5.2,5.2,0,1,0,5.2,5.2A5.21,5.21,0,0,0,20.6,16.7Zm0,8.532A3.332,3.332,0,1,1,23.932,21.9,3.347,3.347,0,0,1,20.6,25.232Z"
                        transform="translate(-10.525 -11.865)"
                    />
                    <circle
                        fill="currentColor"
                        cx="1.178"
                        cy="1.178"
                        r="1.178"
                        transform="translate(14.301 3.535)"
                    />
                    <path
                        fill="currentColor"
                        d="M21.926,6.466A5.786,5.786,0,0,0,17.66,4.8H9.291A5.566,5.566,0,0,0,3.4,10.691V19.02a5.846,5.846,0,0,0,1.706,4.347,5.929,5.929,0,0,0,4.225,1.584H17.62a6,6,0,0,0,4.266-1.584,5.816,5.816,0,0,0,1.666-4.307V10.691A5.824,5.824,0,0,0,21.926,6.466ZM21.764,19.06a3.97,3.97,0,0,1-1.178,2.966,4.2,4.2,0,0,1-2.966,1.056H9.332a4.2,4.2,0,0,1-2.966-1.056,4.084,4.084,0,0,1-1.1-3.006V10.691a4.05,4.05,0,0,1,1.1-2.966A4.132,4.132,0,0,1,9.332,6.669H17.7a4.05,4.05,0,0,1,2.966,1.1,4.192,4.192,0,0,1,1.1,2.925V19.06Z"
                        transform="translate(-3.4 -4.8)"
                    />
                </g>
            </svg>
        );
    }
}

export default InstagramIcon;
