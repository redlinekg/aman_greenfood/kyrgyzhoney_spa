import React, { PureComponent } from "react";

class VideoIcon extends PureComponent {
    render() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                <rect
                    x="2"
                    y="17"
                    width="36"
                    height="32"
                    rx="4"
                    ry="4"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                ></rect>
                <path
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="3"
                    d="M60 19l-14 8v12l14 8h2V19h-2z"
                ></path>
            </svg>
        );
    }
}

export default VideoIcon;
