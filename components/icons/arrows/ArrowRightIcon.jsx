import React, { PureComponent } from "react";

class ArrowRightIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13.998"
                height="21.775"
                viewBox="0 0 13.998 21.775"
            >
                <path
                    d="M10.888,14,21.775,3.111,18.287,0l-7.4,7.4L3.49,0,0,3.111Z"
                    transform="translate(0 21.775) rotate(-90)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default ArrowRightIcon;
