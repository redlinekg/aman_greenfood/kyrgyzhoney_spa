import React, { PureComponent } from "react";

class ArrowTopIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="21.775"
                height="13.998"
                viewBox="0 0 21.775 13.998"
            >
                <path
                    d="M10.888,14,21.775,3.111,18.287,0l-7.4,7.4L3.49,0,0,3.111Z"
                    transform="translate(21.775 13.998) rotate(180)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default ArrowTopIcon;
