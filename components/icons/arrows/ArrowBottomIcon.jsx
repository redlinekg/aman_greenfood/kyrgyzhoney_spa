import React, { PureComponent } from "react";

class ArrowBottomIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="21.775"
                height="13.998"
                viewBox="0 0 21.775 13.998"
            >
                <path
                    d="M28.888,36,39.775,25.111,36.287,22l-7.4,7.4L21.49,22,18,25.111Z"
                    transform="translate(-18 -22)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default ArrowBottomIcon;
