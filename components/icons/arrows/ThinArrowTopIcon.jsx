import React, { PureComponent } from "react";

class ThinArrowTopIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="23.521"
                height="15.022"
                viewBox="0 0 23.521 15.022"
            >
                <path
                    d="M20,26,31,38.834,42,26"
                    transform="translate(42.761 40.371) rotate(180)"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="2"
                />
            </svg>
        );
    }
}

export default ThinArrowTopIcon;
