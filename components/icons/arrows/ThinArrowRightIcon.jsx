import React, { PureComponent } from "react";

class ThinArrowRightIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="15.022"
                height="23.521"
                viewBox="0 0 15.022 23.521"
            >
                <path
                    d="M20,26,31,38.834,42,26"
                    transform="translate(-25.349 42.761) rotate(-90)"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="2"
                />
            </svg>
        );
    }
}

export default ThinArrowRightIcon;
