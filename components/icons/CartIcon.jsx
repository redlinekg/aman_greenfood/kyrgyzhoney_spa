import React, { PureComponent } from "react";

class CartIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="14.046"
                height="13.997"
                viewBox="0 0 14.046 13.997"
            >
                <ellipse
                    cx="1.312"
                    cy="1.312"
                    rx="1.312"
                    ry="1.312"
                    transform="translate(3.5 11.372)"
                    fill="currentColor"
                />
                <ellipse
                    cx="1.312"
                    cy="1.312"
                    rx="1.312"
                    ry="1.312"
                    transform="translate(9.186 11.372)"
                    fill="currentColor"
                />
                <path
                    d="M11.811,9.841h-7a.219.219,0,1,1,0-.437H11.84l2.206-6.342H3.78L2.9,0H.656a.656.656,0,1,0,0,1.312H1.912l.88,3.062H12.2L10.907,8.092h-6.1a1.531,1.531,0,1,0,0,3.062h7a.656.656,0,1,0,0-1.312Z"
                    transform="translate(0 0)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default CartIcon;
