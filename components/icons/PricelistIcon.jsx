import React, { PureComponent } from "react";

import { colors } from "~/config/var";

class PricelistIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="63"
                height="51"
                viewBox="0 0 63 51"
            >
                <g transform="translate(-0.5 -6.5)">
                    <path
                        d="M2,8H62V56H2V8ZM62,56h0V20"
                        fill="currentColor"
                        stroke={colors.black}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                    />
                    <path
                        d="M26,30H50M14,30h4m8,8H50M14,38h4m8,8H50M14,46h4"
                        fill="currentColor"
                        stroke={colors.black}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                    />
                    <path
                        d="M26,30H50M14,30h4"
                        transform="translate(0 -8)"
                        fill="currentColor"
                        stroke={colors.black}
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="3"
                    />
                </g>
            </svg>
        );
    }
}

export default PricelistIcon;
