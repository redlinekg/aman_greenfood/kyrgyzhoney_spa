import React, { PureComponent } from "react";

class CrossIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="23.414"
                height="23.416"
                viewBox="0 0 23.414 23.416"
            >
                <path
                    d="M42,20,20,42m22,0L20,20"
                    transform="translate(-19.292 -19.293)"
                    fill="none"
                    stroke="currentColor"
                    strokeMiterlimit="10"
                    strokeWidth="2"
                />
            </svg>
        );
    }
}

export default CrossIcon;
