import React, { PureComponent } from "react";

class PhoneIcon extends PureComponent {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13.995"
                height="14"
                viewBox="0 0 13.995 14"
            >
                <path
                    d="M12.859,10.959a1.121,1.121,0,0,0-1.518.3c-.56.677-1.238,1.8-3.783-.747S6.11,7.269,6.787,6.709a1.121,1.121,0,0,0,.3-1.518L5.5,2.762c-.21-.3-.49-.794-1.144-.7A3.189,3.189,0,0,0,2,5.214c0,2.1,1.658,4.671,3.923,6.936s4.834,3.9,6.912,3.9a3.385,3.385,0,0,0,3.153-2.335c.07-.467-.4-.934-.7-1.144Z"
                    transform="translate(-2 -2.05)"
                    fill="currentColor"
                />
            </svg>
        );
    }
}

export default PhoneIcon;
