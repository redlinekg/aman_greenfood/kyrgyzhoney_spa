import React, { PureComponent } from "react";

class NewwindowIcon extends PureComponent {
    render() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                <path
                    d="M33 21.996V10l28 20-28 20V38c-11.133.004-21.271.434-30 16 0-9.887 1.45-31.729 30-32.004z"
                    fill="currentColor"
                ></path>
            </svg>
        );
    }
}

export default NewwindowIcon;
