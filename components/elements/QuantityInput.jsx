import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ArrowLeftIcon, ArrowRightIcon } from "~/components/icons";
import { colors, fonts, down } from "~/config/var";

class QuantityInput extends Component {
    static propTypes = {
        onChange: PT.func,
        onIncrease: PT.func,
        onDecrease: PT.func,
        min: PT.number,
        max: PT.number,
        value: PT.number
    };

    state = {
        tmp_value: null
    };

    handleInputChange = e => {
        const {
            target: { value }
        } = e;
        const { onChange } = this.props;
        if (value) {
            onChange(parseInt(value, 10));
            this.setState({
                tmp_value: null
            });
        } else {
            this.setState({
                tmp_value: value
            });
        }
    };

    handleIncrease = () => {
        const { onIncrease } = this.props;
        onIncrease();
    };

    handleDecrease = () => {
        const { onDecrease } = this.props;
        onDecrease();
    };

    render() {
        const { min, max, value } = this.props;
        const { tmp_value } = this.state;
        return (
            <SQuantityInput className="SQuantityInput">
                <SButton onClick={this.handleDecrease}>
                    <ArrowLeftIcon />
                </SButton>
                <SInput
                    className="SQuantityInputInput"
                    type="number"
                    onChange={this.handleInputChange}
                    value={tmp_value === null ? value : tmp_value}
                    min={min}
                    max={max}
                />
                <SButton onClick={this.handleIncrease}>
                    <ArrowRightIcon />
                </SButton>
            </SQuantityInput>
        );
    }
}

const height = "2em";
const height_sm = "1.6em";

const SQuantityInput = styled.div`
    border: 2px solid ${colors.black};
    display: inline-flex;
    align-items: center;
`;
const SInput = styled.input`
    border: 0;
    font-size: 1em;
    text-align: center;
    width: 4em;
    height: ${height};
    background: transparent;
    font-family: ${fonts.ff_global};
    font-weight: 500;
    -moz-appearance: textfield;
    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    ${down("lg")} {
        width: 3em;
    }
    ${down("sm")} {
        height: ${height_sm};
    }
`;
const SButton = styled.div`
    width: 2em;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: all 0.2s;
    color: ${colors.black};
    height: ${height};
    ${down("sm")} {
        height: ${height_sm};
    }
    &:hover {
        color: ${colors.blue_light};
    }
    svg {
        width: 0.9em;
        height: 0.9em;
    }
`;

export default QuantityInput;
