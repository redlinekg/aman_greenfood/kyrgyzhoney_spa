// import IntersectionImage from "react-intersection-image";
import PT from "prop-types";
import React from "react";
import { isBrowser } from "browser-or-node";
import { position } from "polished";
import styled from "@emotion/styled";
import useWindowSize from "@rooks/use-window-size";
import { withTheme } from "emotion-theming";

const BREAKPOINTS = ["xs", "sm", "md", "lg", "xl", "xxl"];
const getPixels = str => parseInt(str);
const getIdx = brk => BREAKPOINTS.indexOf(brk);

const ImageBg = ({
    src,
    alt,
    width,
    height,
    type,
    sizes = {},
    object_fit,
    theme: { breakpoints }
}) => {
    let scale = 1;
    if (isBrowser) {
        scale = window.devicePixelRatio;
    }
    let current_breakpoint = "xxl";
    let available_breakpoint = Object.keys(sizes);
    const { innerWidth } = useWindowSize();
    // const filtred_breakpoints = BREAKPOINTS
    for (let cb of [...BREAKPOINTS].reverse()) {
        if (innerWidth <= getPixels(breakpoints[cb])) {
            current_breakpoint = cb;
        }
    }
    for (let cb of [...BREAKPOINTS].reverse()) {
        if (
            getIdx(current_breakpoint) <= getIdx(cb) &&
            available_breakpoint.includes(cb)
        ) {
            width = sizes[cb].width;
            height = sizes[cb].height;
        }
    }
    width = Math.round(width * Math.min(scale, 1.8));
    height = Math.round(height * Math.min(scale, 1.8));
    return type === "background" ? (
        <SImgBg
            className={`${type} ${object_fit}`}
            backgroundImage={`url("${src}?method=fit&width=${width}&height=${height}")`}
        />
    ) : (
        <SImg
            className={`${type} ${object_fit}`}
            itemProp="image"
            src={`${src}?method=fit&width=${width}&height=${height}`}
            alt={alt}
        />
    );
};

ImageBg.defaultProps = {
    type: "absolute",
    object_fit: "cover"
};

ImageBg.propTypes = {
    type: PT.oneOf(["absolute", "block", "background"]).isRequired,
    object_fit: PT.oneOf(["cover", "contain"]).isRequired,
    src: PT.string,
    alt: PT.string,
    width: PT.number,
    height: PT.number,
    sizes: PT.object,
    theme: PT.object
};

const SImg = styled.img`
    transition: all 0.3s;
    &.absolute {
        ${position("absolute", "0", null, null, "0")};
        width: 100%;
        height: 100%;
        &.cover {
            object-fit: cover;
        }
        &.contain {
            object-fit: contain;
        }
    }
    &.block {
    }
`;
const SImgBg = styled.div`
    ${position("absolute", "0", null, null, "0")};
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: center center;
    &.cover {
        object-fit: cover;
    }
    &.contain {
        object-fit: contain;
    }
`;

export default withTheme(ImageBg);
