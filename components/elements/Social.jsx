import React from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { compose } from "ramda";
import { inject, observer } from "mobx-react";

import {
    WhatsappIcon,
    TelegramIcon,
    FacebookIcon,
    InstagramIcon
} from "../icons";
import { colors, down } from "~/config/var";

let Social = ({ settings_store }) => {
    return (
        <>
            {settings_store.WHATSAPP ? (
                <SItem href={settings_store.WHATSAPP} className="whatsapp">
                    <SIcon>
                        <WhatsappIcon />
                    </SIcon>
                    <SText>WhatsApp</SText>
                </SItem>
            ) : null}
            {settings_store.TELEGRAM ? (
                <SItem href={settings_store.TELEGRAM} className="telegram">
                    <SIcon>
                        <TelegramIcon />
                    </SIcon>
                    <SText>Telegram</SText>
                </SItem>
            ) : null}
            {settings_store.FACEBOOK ? (
                <SItem href={settings_store.FACEBOOK} className="facebook">
                    <SIcon>
                        <FacebookIcon />
                    </SIcon>
                    <SText>Facebook</SText>
                </SItem>
            ) : null}
            {settings_store.INSTAGRAM ? (
                <SItem href={settings_store.INSTAGRAM} className="instagram">
                    <SIcon>
                        <InstagramIcon />
                    </SIcon>
                    <SText>Instagram</SText>
                </SItem>
            ) : null}
        </>
    );
};

Social.propTypes = {
    settings_store: PT.object.isRequired
};

Social.propTypes = {
    settings_store: PT.object.isRequired
};

Social = compose(inject("settings_store"), observer)(Social);

const SItem = styled.a`
    display: flex;
    align-items: center;
    color: ${colors.black};
    margin-right: 1.2em;
    font-size: 1em;
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
    &:nth-of-type(1),
    &:nth-of-type(2) {
        margin-bottom: 0.5em;
    }
    ${down("sm")} {
    }
    svg {
        width: 1.4em;
        max-height: 1.1em;
    }
    &.telegram {
        svg {
            width: 1.16em;
            max-height: 1.16em;
        }
    }
    &.facebook {
        svg {
            width: 0.6em;
            max-height: 1.1em;
        }
    }
    &.instagram {
        svg {
            width: 1.1em;
            max-height: 1.1em;
        }
    }
    &.twitter {
        svg {
            width: 1.2em;
            max-height: 1.3em;
        }
    }
    &.youtube {
        svg {
            width: 1.3em;
            max-height: 1.1em;
        }
    }
    &:hover {
    }
`;

const SIcon = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 1.4em;
    font-size: 0.9em;
`;
const SText = styled.div`
    padding-left: 0.3em;
    color: ${colors.grey};
    @media (hover) {
        ${SItem}:hover & {
            color: ${colors.blue_light};
        }
    }
`;

export default Social;
