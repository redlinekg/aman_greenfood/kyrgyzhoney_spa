import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import { keyframes } from "@emotion/core";
import styled from "@emotion/styled";

import { colors, down, global } from "~/config/var";

class Loader extends PureComponent {
    static propTypes = {
        type: PT.string,
        size: PT.string,
        bg: PT.bool
    };

    render() {
        const { type, size, bg } = this.props;
        return (
            <LoaderBlock
                className={CN(`${type ? type : null} ${size ? size : null} `, {
                    bg: bg
                })}
            >
                <Spinner />
            </LoaderBlock>
        );
    }
}

const LoaderBlock = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    z-index: 3;
    &.absolute {
        position: absolute;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
    }
    &.fixed {
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
    }
    &.block {
        height: 10em;
        margin-bottom: 5em;
        ${down("lg")} {
            height: 5em;
            margin-bottom: 5em;
        }
    }
    &.bg {
        background: rgba(255, 255, 255, 0.95);
        border-radius: ${global.border_radius};
    }
`;

const spin = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`;

const Spinner = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    z-index: 6;
    background: 0 none;
    margin: 0;
    &:after {
        content: "";
        height: 5em;
        width: 5em;
        display: block;
        border-radius: 100%;
        border-style: solid;
        border-color: ${colors.grey_light3};
        border-bottom-color: ${colors.blue};
        animation: ${spin} 1s linear 0s infinite;
    }
    ${LoaderBlock}.mini & {
        &:after {
            width: 1.4em;
            height: 1.4em;
            border-width: 2px;
        }
    }
    ${LoaderBlock}.large & {
        &:after {
            width: 2.4em;
            height: 2.4em;
            border-width: 3px;
        }
    }
    ${LoaderBlock}.big & {
        &:after {
            width: 3.4em;
            height: 3.4em;
            border-width: 3px;
        }
    }
`;

export default Loader;
