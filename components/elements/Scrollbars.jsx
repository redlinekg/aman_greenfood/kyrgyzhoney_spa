import React, { Component } from "react";
import PT from "prop-types";
import { Scrollbars } from "react-custom-scrollbars";
import { position } from "polished";
import styled from "@emotion/styled";

export default class ColoredScrollbars extends Component {
    static propTypes = {
        children: PT.any
    };

    constructor(props, ...rest) {
        super(props, ...rest);
        this.state = { top: 0 };
        this.handleUpdate = this.handleUpdate.bind(this);
        this.renderThumb = this.renderThumb.bind(this);
    }

    handleUpdate(values) {
        const { top } = values;
        this.setState({ top });
    }

    renderTrack({ style, ...props }) {
        return <STrack style={{ ...style }} {...props} />;
    }

    renderThumb({ style, ...props }) {
        const { top } = this.state;
        let color = 220 - top * 220;
        const thumbStyle = {
            backgroundColor: `rgb(${Math.round(color)}, ${Math.round(
                color
            )}, ${Math.round(color)})`
        };
        return <SThumb style={{ ...style, ...thumbStyle }} {...props} />;
    }

    render() {
        const { children, ...another } = this.props;
        return (
            <SBlock>
                <Scrollbars
                    renderThumbVertical={this.renderThumb}
                    renderTrackVertical={this.renderTrack}
                    onUpdate={this.handleUpdate}
                    {...another}
                    {...this.props}
                >
                    {children}
                </Scrollbars>
            </SBlock>
        );
    }
}

const SBlock = styled.div`
    height: 100%;
    display: flex;
    > div > div {
        overflow-x: hidden !important;
    }
`;
const SThumb = styled.div``;
const STrack = styled.div`
    ${position("absolute", "0", "0", "0", null)};
    width: 6px;
`;
