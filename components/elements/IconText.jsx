import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";

import { colors, down, global } from "~/config/var";

class IconText extends PureComponent {
    static propTypes = {
        horizontal: PT.bool,
        white: PT.bool,
        icon: PT.any,
        title: PT.string,
        text: PT.string,
        children: PT.any
    };

    render() {
        const { horizontal, white, icon, title, text, children } = this.props;
        return (
            <SIconText
                className={CN({
                    horizontal: horizontal,
                    white: white
                })}
            >
                <Block>
                    <IconBlock>
                        <img src={icon} alt={title} />
                    </IconBlock>
                    <Info>
                        <H4>{title}</H4>
                        <Text>{text}</Text>
                        {children ? (
                            <ChildrenBlock>{children}</ChildrenBlock>
                        ) : null}
                    </Info>
                </Block>
            </SIconText>
        );
    }
}

const SIconText = styled.div`
    margin: ${global.padding_big} 0;
    max-width: 500px;
`;
const Block = styled.div`
    text-align: center;
    ${SIconText}.horizontal & {
        display: flex;
        text-align: left;
        align-items: flex-start;
    }
`;
const IconBlock = styled.div`
    width: 4em;
    height: 4em;
    margin: 0 auto;
    ${SIconText}.horizontal & {
        width: 4em;
        height: 4em;
        flex: 0 0 4em;
        margin: 0;
    }

    img {
        width: 100%;
        display: block;
    }
`;
const Info = styled.div`
    ${SIconText}.horizontal & {
        padding-left: 1.2em;
    }
`;
const H4 = styled.h4`
    font-size: 1.6em;
    font-weight: 700;
    margin-bottom: 0.2em;
    margin-top: 0.5em;
    color: ${props => (props.white ? colors.white : colors.blue)};
    ${SIconText}.horizontal & {
        margin-bottom: 0.4em;
        margin-top: 0;
    }
    ${down("sm")} {
        font-size: 1.2em;
    }
`;
const Text = styled.div`
    font-size: 1em;
    line-height: 1.2;
    color: ${colors.grey};
    ${SIconText}.white & {
        color: ${colors.white};
    }
    ${SIconText}.horizontal & {
        font-size: 1em;
    }
`;
const ChildrenBlock = styled.div`
    margin-top: 1em;
`;

export default IconText;
