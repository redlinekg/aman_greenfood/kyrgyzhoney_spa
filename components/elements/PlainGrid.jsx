import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { css } from "@emotion/core";

import { down, up, only } from "~/config/var";

class PlainGrid extends PureComponent {
    static defaultProps = {
        xga: 3,
        xl: 3,
        lg: 3,
        md: 2,
        sm: 1
    };

    static propTypes = {
        xga: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        xl: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        lg: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        md: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        sm: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        padding_small: PT.bool,
        children: PT.any
    };
    render() {
        const { xga, xl, lg, md, sm, padding_small, children } = this.props;
        return (
            <SPlainGrid
                className={`SPlainGrid ${padding_small && "padding_small"}`}
            >
                <SBlock
                    className={`xga_${xga} xl_${xl} lg_${lg} md_${md} sm_${sm} ${padding_small &&
                        "padding_small"}`}
                    itemScope
                    itemType="http://schema.org/ItemList"
                >
                    {children}
                </SBlock>
            </SPlainGrid>
        );
    }
}

const One = css`
    > * {
        width: 100%;
    }
`;
const Two = css`
    > * {
        width: calc(100% / 2);
    }
`;
const Three = css`
    > * {
        width: calc(100% / 3);
    }
`;
const Four = css`
    > * {
        width: calc(100% / 4);
    }
`;
const Five = css`
    > * {
        width: calc(100% / 5);
    }
`;
const Six = css`
    > * {
        width: calc(100% / 6);
    }
`;

const SPlainGrid = styled.div`
    margin: 0 -1em;
    ${down("md")} {
        margin: 0 -0.8em;
    }
    ${down("sm")} {
        margin: 0 -8px;
    }
    &.padding_small {
        margin: 0 -0.5em;
        ${down("sm")} {
            margin: 0 -5px;
        }
    }
`;
const SBlock = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    /* /////////// */
    > * {
        padding: 0 1em;
        ${down("md")} {
            padding: 0 0.8em;
        }
        ${down("sm")} {
            padding: 0 8px;
        }
    }
    ${SPlainGrid}.padding_small & {
        > * {
            padding: 0 0.5em;
            ${down("sm")} {
                padding: 0 5px;
            }
        }
    }
    /* /////////// */
    ${up("xga")} {
        &.xga_1 {
            ${One};
        }
        &.xga_2 {
            ${Two};
        }
        &.xga_3 {
            ${Three};
        }
        &.xga_4 {
            ${Four};
        }
        &.xga_5 {
            ${Five};
        }
        &.xga_6 {
            ${Six};
        }
    }
    ${only("xl")} {
        &.xl_1 {
            ${One};
        }
        &.xl_2 {
            ${Two};
        }
        &.xl_3 {
            ${Three};
        }
        &.xl_4 {
            ${Four};
        }
        &.xl_5 {
            ${Five};
        }
        &.xl_6 {
            ${Six};
        }
    }
    ${only("lg")} {
        &.lg_1 {
            ${One};
        }
        &.lg_2 {
            ${Two};
        }
        &.lg_3 {
            ${Three};
        }
        &.lg_4 {
            ${Four};
        }
        &.lg_5 {
            ${Five};
        }
        &.lg_6 {
            ${Six};
        }
    }
    ${only("md")} {
        &.md_1 {
            ${One};
        }
        &.md_2 {
            ${Two};
        }
        &.md_3 {
            ${Three};
        }
        &.md_4 {
            ${Four};
        }
        &.md_5 {
            ${Five};
        }
        &.md_6 {
            ${Six};
        }
    }
    ${down("sm")} {
        &.sm_1 {
            ${One};
        }
        &.sm_2 {
            ${Two};
        }
        &.sm_3 {
            ${Three};
        }
        &.sm_4 {
            ${Four};
        }
        &.sm_5 {
            ${Five};
        }
        &.sm_6 {
            ${Six};
        }
    }
`;

export default PlainGrid;
