import PT from "prop-types";
import React from "react";
import dayjs from "dayjs";

import { i18n } from "~/i18n";

import "dayjs/locale/es";
import "dayjs/locale/ru";
import "dayjs/locale/ky";

const DateFormat = ({ date }) => {
    return (
        <time dateTime={date} className="CrearedAt">
            {dayjs(date)
                .locale(
                    i18n.language === "ru"
                        ? "ru"
                        : i18n.language === "en"
                        ? "en"
                        : "ky"
                )
                .format("DD MMMM YYYY")}
        </time>
    );
};

DateFormat.propTypes = {
    date: PT.string.isRequired
};

export default DateFormat;
