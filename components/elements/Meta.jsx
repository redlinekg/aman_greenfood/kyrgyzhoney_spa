import React, { Component } from "react";
import PT from "prop-types";
import Head from "next/head";
import { Helmet } from "react-helmet";

import { withTranslation } from "~/i18n";

import meta_share from "~/static/images/logo_share.jpg";

const concatMeta = (...meta_objects) =>
    meta_objects.reduce((result, current) => {
        if (current) {
            result.push(current);
        }
        return result;
    }, []);

class Meta extends Component {
    static defaultProps = {
        part: " - Kyrgyz Honey",
        name: "KG Honey",
        og_image: `${process.env.BASE_URL}${meta_share}`,
        width_image: "1200",
        height_image: "630",
        author: "Kyrgyz Honey"
    };

    static propTypes = {
        title: PT.string.isRequired,
        meta_title: PT.string,
        og_image: PT.string,
        og_url: PT.string,
        name: PT.string,
        description: PT.string,
        part: PT.string,
        width_image: PT.string,
        height_image: PT.string,
        published_time: PT.string,
        modified_time: PT.string,
        author: PT.string,
        i18n: PT.object
    };

    render() {
        const {
            title,
            meta_title,
            og_image,
            og_url,
            name,
            description,
            part,
            width_image,
            height_image,
            published_time,
            modified_time,
            author,
            i18n: { language }
        } = this.props;
        // const size = `?method=resize&width=${width_image}&height=${height_image}`;
        return (
            <>
                <Head>
                    <title>{`${title ? title : "Загрузка"} ${part}`}</title>
                </Head>
                <Helmet
                    encodeSpecialCharacters={false}
                    meta={concatMeta(
                        {
                            name: "viewport",
                            content:
                                "width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no"
                        },
                        {
                            property: "og:type",
                            content: "website"
                        },
                        {
                            property: "og:site_name",
                            content: name
                        },
                        {
                            property: "og:title",
                            content: meta_title
                        },
                        description
                            ? {
                                  property: "og:description",
                                  content: description
                              }
                            : null,
                        og_image
                            ? {
                                  property: "og:image",
                                  content: `${og_image}`
                              }
                            : null,
                        og_image
                            ? {
                                  property: "og:image:secure_url",
                                  content: `${og_image}`
                              }
                            : null,
                        og_image
                            ? {
                                  property: "og:image:width",
                                  content: `${width_image}px`
                              }
                            : null,
                        og_image
                            ? {
                                  property: "og:image:height",
                                  content: `${height_image}px`
                              }
                            : null,

                        published_time
                            ? {
                                  property: "article:published_time",
                                  content: published_time
                              }
                            : null,
                        modified_time
                            ? {
                                  property: "article:modified_time",
                                  content: modified_time
                              }
                            : null,
                        modified_time
                            ? {
                                  property: "og:updated_time",
                                  content: modified_time
                              }
                            : null,
                        modified_time
                            ? {
                                  property: "og:updated_time",
                                  content: modified_time
                              }
                            : null,
                        {
                            name: "author",
                            content: author
                        },
                        {
                            property: "og:locale",
                            content: language === "ru" ? "ru_RU" : "en_US"
                        },
                        og_url
                            ? {
                                  property: "og:url",
                                  content: og_url
                              }
                            : null,
                        og_url
                            ? {
                                  rel: "canonical",
                                  href: og_url
                              }
                            : null,
                        {
                            name: "description",
                            content: description
                        },
                        // {
                        //     name: "twitter:site",
                        //     content: "@"
                        // },
                        {
                            name: "twitter:title",
                            content: title
                        },
                        {
                            name: "twitter:description",
                            content: description
                        },
                        {
                            name: "twitter:card",
                            content: "summary_large_image"
                        },
                        {
                            name: "twitter:image",
                            content: `${og_image}`
                        }
                    )}
                />
            </>
        );
    }
}

export default withTranslation("common")(Meta);
