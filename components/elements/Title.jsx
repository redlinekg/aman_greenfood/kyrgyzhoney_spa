import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import { css } from "@emotion/core";
import styled from "@emotion/styled";

import { More } from "~/components";
import { down } from "~/config/var";

class Title extends PureComponent {
    static defaultProps = {
        level: 1
    };

    static propTypes = {
        level: PT.oneOf([1, 2, 3, 4]).isRequired,
        description: PT.string,
        children: PT.any,
        children_links: PT.any
    };

    render() {
        const {
            level,
            description,
            children,
            children_links,
            ...rest
        } = this.props;
        return (
            <>
                {level === 1 && (
                    <SH1
                        {...rest}
                        className={CN({
                            description: description
                        })}
                    >
                        {children}
                    </SH1>
                )}
                {level === 2 && <SH2 {...rest}>{children}</SH2>}
                {level === 3 && <SH3 {...rest}>{children}</SH3>}
                {level === 4 && <SH4 {...rest}>{children}</SH4>}
                {children_links}
                {description ? (
                    <SText>
                        <More>{description}</More>
                    </SText>
                ) : null}
            </>
        );
    }
}

const STitleStyle = css`
    font-weight: 700;
    margin-top: 0;
    &.description {
        margin-bottom: 0.4em;
    }
`;
const SH1 = styled.h1`
    ${STitleStyle};
    font-size: 3em;
    ${down("lg")} {
        font-size: 2.4em;
    }
    ${down("lg")} {
        font-size: 2.4em;
    }
    ${down("md")} {
        font-size: 2em;
    }
    ${down("sm")} {
        font-size: 1.6em;
    }
`;
const SH2 = styled.h2`
    ${STitleStyle};
    font-size: 1.6em;
    ${down("md")} {
        font-size: 1.4em;
    }
    ${down("sm")} {
        font-size: 1.2em;
    }
`;
const SH3 = styled.h3`
    ${STitleStyle};
`;
const SH4 = styled.h4`
    ${STitleStyle};
`;
const SText = styled.div`
    margin-bottom: 2em;
    font-size: 1.1em;
    font-weight: 500;
    max-width: 900px;
    ${down("md")} {
        font-size: 1em;
    }
    ${down("sm")} {
        font-size: 0.9em;
    }
`;

export default Title;
