import { Component } from "react";
import PT from "prop-types";
import ReactDOM from "react-dom";

class Portal extends Component {
    static propTypes = {
        children: PT.any,
        selector: PT.any
    };
    componentDidMount() {
        this.element = document.querySelector(this.props.selector);
    }
    render() {
        if (this.element === undefined) {
            return null;
        }
        return ReactDOM.createPortal(this.props.children, this.element);
    }
}

export default Portal;
