import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { down } from "~/config/var";

class H1 extends PureComponent {
    static propTypes = {
        children: PT.any
    };

    render() {
        const { children } = this.props;
        return (
            <Root>
                <Wrapper>{children}</Wrapper>
            </Root>
        );
    }
}

const Root = styled.div`
    ${down("md")} {
        width: calc(100% + 30px);
        margin: 0 -15px;
    }
`;

const Wrapper = styled.div`
    display: flex;
    ${down("md")} {
        flex-wrap: nowrap;
        overflow-x: auto;
        width: 100%;
        -webkit-overflow-scrolling: touch;
        &:after {
            content: "";
            display: block;
            flex: 0 0 auto;
            margin-right: 1.5em;
            width: 1px;
        }
        &::-webkit-scrollbar {
            display: none;
        }
    }
`;

export default H1;
