import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { up, max_width } from "~/config/var";

class Container extends PureComponent {
    static propTypes = {
        children: PT.any
    };

    render() {
        const { children } = this.props;
        return (
            <ContainerBlock className="ContainerBlock">
                {children}
            </ContainerBlock>
        );
    }
}

const ContainerBlock = styled.div`
    width: 100%;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    ${up("sm")} {
        max-width: ${max_width.sm}px;
    }
    ${up("md")} {
        max-width: ${max_width.md}px;
    }
    ${up("lg")} {
        max-width: ${max_width.lg}px;
    }
    ${up("xl")} {
        max-width: ${max_width.xl}px;
    }
    ${up("xga")} {
        max-width: ${max_width.xga}px;
    }
`;

export default Container;
