import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { darken } from "polished";

import { Loader } from "~/components";
import { colors, global, fonts } from "~/config/var";

class Button extends PureComponent {
    static defaultProps = {
        color: "yellow",
        disabled: false,
        loading: false,
        block: false,
        hide_icon: false
    };

    static propTypes = {
        color: PT.oneOf(["yellow", "blue", "blue_light"]).isRequired,
        active: PT.bool,
        disabled: PT.bool,
        loading: PT.bool,
        block: PT.bool,
        onClick: PT.func,
        children: PT.any.isRequired,
        hide_icon: PT.bool
    };

    render() {
        const {
            color,
            disabled,
            loading,
            active,
            children,
            onClick,
            block,
            ...another
        } = this.props;

        return (
            <SButton
                onClick={onClick}
                className={CN(`SButton ${color}`, {
                    disabled: disabled,
                    active: active,
                    loading: loading,
                    block: block
                })}
                {...another}
            >
                <SInfo>
                    <SText className="SBtnText">{children}</SText>
                </SInfo>
                {loading ? <Loader type="absolute" size="mini" /> : null}
            </SButton>
        );
    }
}

const SButton = styled.button`
    cursor: pointer;
    box-shadow: none;
    padding: 0.4em 1.2em;
    border: 0;
    position: relative;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: ${fonts.ff_global};
    font-size: 1em;
    border-radius: ${global.border_radius};
    font-weight: 500;
    background-color: transparent;
    &.blue {
        color: ${colors.white};
        background-color: ${colors.blue};
        @media (hover) {
            &:hover {
                background-color: ${darken(0.03, colors.blue)};
            }
        }
        @media (active) {
            &:active {
                background-color: ${darken(0.03, colors.blue)};
            }
        }
        &.disabled,
        &.loading {
            pointer-events: none;
            background-color: ${colors.grey_light4};
            color: ${colors.black};
        }
    }
    &.blue_light {
        color: ${colors.white};
        background-color: ${colors.blue_light};
        @media (hover) {
            &:hover {
                background-color: ${darken(0.03, colors.blue_light)};
            }
        }
        @media (active) {
            &:active {
                background-color: ${darken(0.03, colors.blue_light)};
            }
        }
        &.disabled,
        &.loading {
            pointer-events: none;
            background-color: ${colors.grey_light4};
            color: ${colors.black};
        }
    }
    &.yellow {
        color: ${colors.black};
        background-color: ${colors.yellow_light};
        @media (hover) {
            &:hover {
                background-color: ${darken(0.03, colors.yellow_light)};
            }
        }
        @media (active) {
            &:active {
                background-color: ${darken(0.03, colors.yellow_light)};
            }
        }
        &.disabled,
        &.loading {
            pointer-events: none;
            background-color: ${colors.grey_light4};
            color: ${colors.black};
        }
    }
    &.loading {
        pointer-events: none;
    }
    &.block {
        display: block;
        width: 100%;
    }
`;
const SInfo = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    z-index: 2;
    transition: all 0.2s;
    ${SButton}.loading & {
        opacity: 0;
    }
`;
const SText = styled.div`
    white-space: nowrap;
`;

export default Button;
