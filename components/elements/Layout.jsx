import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";

class Layout extends PureComponent {
    static defaultProps = {
        show_padding_top: true
    };

    static propTypes = {
        children: PT.any,
        show_padding_top: PT.bool
    };

    render() {
        const { children, show_padding_top } = this.props;
        return (
            <SLayout
                className={CN({
                    show_padding_top: show_padding_top
                })}
            >
                {children}
            </SLayout>
        );
    }
}

const SLayout = styled.div`
    &.show_padding_top {
        padding-top: 3em;
    }
`;

export default Layout;
