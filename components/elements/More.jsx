import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";
import ShowMoreText from 'react-show-more-text';

import { withTranslation } from "~/i18n";
import { colors } from "~/config/var";

class More extends PureComponent {
    static defaultProps = {
        lines: 3,
        button_show: true
    };

    static propTypes = {
        t: PT.any,
        lines: PT.number,
        button_show: PT.bool,
        children: PT.any
    };
    render() {
        const { t, lines, button_show, children } = this.props;
        return (
            <SMore
                className={CN({
                    button_show: button_show
                })}
            >
                <ShowMoreText
                    lines={lines}
                    more={t("readmore_show")}
                    less={t("readmore_hide")}
                    anchorClass="read_more"
                    onClick={this.executeOnClick}
                    expanded={false}
                >
                    {children}
                </ShowMoreText>
            </SMore>
        );
    }
}

const SMore = styled.div`
    .read_more {
        color: ${colors.blue_light};
        display: none;
        @media (hover) {
            &:hover {
                color: ${colors.blue};
                text-decoration: none;
            }
        }
    }
    &.button_show {
        .read_more {
            display: block;
        }
    }
`;

export default withTranslation(["common"])(More);
