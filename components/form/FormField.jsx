import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import Textarea from "react-textarea-autosize";
import Select from "react-select";

import { position } from "polished";
import { colors, down, fonts, global } from "~/config/var";

class FormField extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { focus: false };
        this.onFocus = this.onFocus.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }
    static propTypes = {
        icon: PT.any,
        label: PT.string,
        id: PT.string,
        name: PT.string,
        value: PT.string,
        fill: PT.string,
        type: PT.string,
        errors: PT.string,
        required: PT.bool,
        onChange: PT.any,
        onBlur: PT.any,
        options: PT.array,
        placeholder: PT.string
    };
    onFocus() {
        this.setState({ focus: true });
    }
    onBlur() {
        this.setState({ focus: false });
    }

    handleChange = value => {
        this.props.onChange(this.props.name, value);
    };

    handleBlur = () => {
        this.props.onBlur(this.props.name, true);
    };

    render() {
        const {
            icon,
            label,
            id,
            fill,
            name,
            value,
            type,
            errors,
            required,
            options,
            placeholder,
            ...another
        } = this.props;
        const { focus } = this.state;
        return (
            <SFormField
                className={CN({
                    select: type == "select",
                    focus: focus || fill,
                    error: errors
                })}
            >
                {icon && <SIcon>{icon}</SIcon>}
                {type == "select" ? null : (
                    <SLabel htmlFor={id}>
                        {label} {required && <SRequired>*</SRequired>}
                    </SLabel>
                )}
                {type == "textarea" ? (
                    <STextarea
                        onBlur={this.onBlur}
                        onFocus={this.onFocus}
                        error={errors}
                        id={id}
                        name={name}
                        {...another}
                    />
                ) : type == "select" ? (
                    <SSelect
                        className={CN({
                            focus: focus || fill
                        })}
                    >
                        <Select
                            classNamePrefix="react-select"
                            onFocus={this.onFocus}
                            name={name}
                            value={value}
                            id={id}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            options={options}
                            placeholder={placeholder}
                        />
                    </SSelect>
                ) : (
                    <SInput
                        onBlur={this.onBlur}
                        onFocus={this.onFocus}
                        error={errors}
                        id={id}
                        name={name}
                        type={type}
                        {...another}
                    />
                )}
            </SFormField>
        );
    }
}
const SFormField = styled.div`
    position: relative;
    margin-bottom: 1.2em;
    z-index: 10;
    &.select {
        z-index: 11;
    }
`;

const SIcon = styled.div`
    ${position("absolute", "0", null, null, "0")};
    width: 2.8em;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 3;
    svg {
        width: 1em;
        height: 1em;
    }
`;

const SLabel = styled.label`
    ${position("absolute", "18px", null, null, "52px")};
    font-weight: 500;
    font-size: 22px;
    color: ${colors.grey};
    z-index: 1;
    cursor: text;
    transition: all 0.18s ease-out;
    border-radius: ${global.border_radius};
    ${down("sm")} {
    }
    ${SFormField}.error & {
        color: ${colors.red};
    }

    ${SFormField}.focus & {
        top: -10px;
        left: 54px;
        padding: 2px 7px;
        font-size: 14px;
        color: ${colors.black};
        background-color: ${colors.yellow_light};
        &.error {
            color: ${colors.white};
            background-color: ${colors.red};
        }
    }
`;

const SRequired = styled.span`
    color: ${colors.red};
`;

const FormFieldStyle = css`
    font-family: ${fonts.ff_global};
    font-weight: 400;
    font-size: 22px;
    padding: 0.7em 1em 0.7em 52px;
    border: 2px solid ${colors.black};
    background: ${colors.white};
    color: ${colors.black};
    transition: all 0.2s;
    border-radius: ${global.border_radius};
    display: block;
    -webkit-appearance: none;
    transition: all 0.3s ease;
    width: 100%;
    @media (hover) {
        &:hover {
            border: 2px solid ${colors.blue};
        }
    }
    ${SFormField}.focus & {
        box-shadow: none;
        border-color: ${colors.blue};
        outline: none;
        transition: all 0.2s ease;
    }
`;

const SInput = styled.input`
    ${FormFieldStyle};
    ${down("sm")} {
    }
`;

const STextarea = styled(Textarea)`
    ${FormFieldStyle};
    ${down("sm")} {
    }
    min-height: 100px;
    resize: none;
    overflow: hidden;
`;
//////////////
//////////////
//////////////
const SSelect = styled.div`
    width: 100%;
    ${FormFieldStyle};
    padding: 0 0;
    font-family: ${fonts.ff_global};
    position: relative;
    z-index: 2;
    .react-select__control {
        border: 0 !important;
        box-shadow: none !important;
        padding: 0.4em 1em 0.5em 48px;
        background-color: transparent;
    }
    .react-select__menu {
        box-shadow: none;
        border: 2px solid ${colors.blue};
        border-radius: 0;
        width: calc(100% + 4px);
        margin-left: -2px;
    }
    .react-select__value-container,
    .react-select__placeholder {
        padding-left: 0;
        color: ${colors.grey};
        font-weight: 500;
    }
    ${SFormField}.error & {
        .react-select__placeholder {
            color: ${colors.red};
        }
    }
    .react-select__single-value {
    }
    .react-select__indicator-separator {
        display: none;
    }
    .react-select__indicator {
        color: ${colors.grey};
        padding: 0;
    }
    .react-select__option {
        &.react-select__option--is-focused {
            background-color: ${colors.bg_hover};
            color: ${colors.black};
            cursor: pointer;
        }
        &.react-select__option--is-selected {
            background-color: ${colors.bg_active};
            color: ${colors.white};
            cursor: default;
        }
        &:active {
            background-color: ${colors.bg_hover};
            color: ${colors.black};
        }
    }
`;

export default FormField;
