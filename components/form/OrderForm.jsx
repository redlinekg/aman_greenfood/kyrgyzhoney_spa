import React, { Component } from "react";
import PT from "prop-types";
import * as Yup from "yup";
import styled from "@emotion/styled";
import { Formik } from "formik";

import { PlanetIcon, UserIcon, PhoneIcon } from "../icons";
import { FormField, FormNotice } from "./";
import { withTranslation } from "~/i18n";
import { Link } from "~/routes";
import { Button } from "~/components";
import { colors, down, fonts } from "~/config/var";

import country from "~/config/temp/country";

const initialValues = {
    form_country: "",
    form_name: "",
    form_phone: ""
};

const CreateFormSchema = t =>
    Yup.object().shape({
        form_country: Yup.string().required(t("modal_form_country_required")),
        form_name: Yup.string().required(t("modal_form_name_required")),
        form_phone: Yup.string().required(t("modal_form_phone_required"))
    });

class OrderForm extends Component {
    static propTypes = {
        name: PT.string,
        title: PT.string,
        text: PT.string,
        button: PT.string,
        success_title: PT.string,
        success_text: PT.string,
        t: PT.any.isRequired,
        onSubmit: PT.func.isRequired
    };

    state = {
        status: "VIRGIN"
    };

    onSubmit = async (values, { setSubmitting }) => {
        const { onSubmit } = this.props;
        try {
            await onSubmit(values);
            setSubmitting(false);
            this.setState({
                status: "SUCCESS"
            });
        } catch (error) {
            console.log("error");
        }
    };

    render() {
        const {
            name,
            title,
            text,
            button,
            success_title,
            success_text,
            t
        } = this.props;
        return (
            <Formik
                initialValues={initialValues}
                validationSchema={CreateFormSchema(t)}
                onSubmit={this.onSubmit}
            >
                {({
                    values,
                    touched,
                    errors,
                    handleChange,
                    handleSubmit,
                    isValid,
                    setFieldValue,
                    setFieldTouched
                }) => (
                    <SForm>
                        <STop>
                            <STitle>{title}</STitle>
                            {text ? <SText>{text}</SText> : null}
                        </STop>
                        {this.state.status === "SUCCESS" ? (
                            <SBlockSuccess>
                                <FormNotice
                                    icon={<PlanetIcon />}
                                    title={success_title}
                                    text={success_text}
                                />
                            </SBlockSuccess>
                        ) : (
                            <SBlockForm>
                                <FormField
                                    id={`${name}_country`}
                                    name="form_country"
                                    type="select"
                                    value={values.form_country}
                                    onChange={setFieldValue}
                                    onBlur={setFieldTouched}
                                    touched={touched.form_country}
                                    errors={errors.form_country}
                                    placeholder={t("modal_form_country")}
                                    options={country}
                                    icon={<PlanetIcon />}
                                />
                                <FormField
                                    id={`${name}_name`}
                                    label={t("modal_form_name")}
                                    name="form_name"
                                    type="text"
                                    onChange={handleChange}
                                    errors={errors.form_name}
                                    fill={values.form_name}
                                    icon={<UserIcon />}
                                />
                                <FormField
                                    id={`${name}_phone`}
                                    label={t("modal_form_phone")}
                                    name="form_phone"
                                    type="tel"
                                    onChange={handleChange}
                                    errors={errors.form_phone}
                                    fill={values.form_phone}
                                    icon={<PhoneIcon />}
                                />
                                <SAgreement>
                                    {t("modal_form_privat")}{" "}
                                    <Link
                                        route="page"
                                        params={{ slug: "privacy-policy" }}
                                    >
                                        <a>{t("modal_form_privat2")}</a>
                                    </Link>
                                </SAgreement>
                                <SSend>
                                    <Button
                                        color="blue"
                                        onClick={handleSubmit}
                                        disabled={!isValid}
                                    >
                                        {button}
                                    </Button>
                                </SSend>
                            </SBlockForm>
                        )}
                    </SForm>
                )}
            </Formik>
        );
    }
}

const SForm = styled.div`
    position: sticky;
    top: 2em;
`;
const STop = styled.div`
    margin: 0 0 1.5em;
`;
const STitle = styled.div`
    font-size: 2em;
    font-weight: 600;
    font-family: ${fonts.title_font};
    ${down("lg")} {
        font-size: 1.6em;
    }
    ${down("sm")} {
        font-size: 1.2em;
    }
`;
const SText = styled.div`
    margin: 0.6em 0 0;
    ${down("sm")} {
        font-size: 0.9em;
    }
`;
const SBlockForm = styled.div``;
const SAgreement = styled.div`
    width: 100%;
    text-align: center;
    padding-bottom: 1em;
    font-size: 0.9em;
    color: ${colors.grey};
    ${down("sm")} {
    }
    @media (hover) {
        a {
            color: ${colors.blue};
        }
    }
`;
const SSend = styled.div`
    display: flex;
    justify-content: center;
    font-size: 1.1em;
`;
const SBlockSuccess = styled.div``;

export default withTranslation(["common"])(OrderForm);
