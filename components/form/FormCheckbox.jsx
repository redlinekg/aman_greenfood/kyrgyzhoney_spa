import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { Link } from "~/routes";
import { colors, global } from "~/config/var";

class FormCheckbox extends PureComponent {
    static propTypes = {
        label: PT.string,
        name: PT.string,
        checked: PT.bool,
        onChange: PT.func,
        route: PT.string,
        slug: PT.string,
        link_text: PT.string,
        errors: PT.string,
        white: PT.bool
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {
            label,
            name,
            checked,
            onChange,
            route = "",
            slug = undefined,
            link_text,
            errors,
            white
        } = this.props;
        // TODO: Change links
        return (
            <FormCheck>
                <FormCheckBlock>
                    <FormCheckInput
                        name={name}
                        type="checkbox"
                        defaultChecked={checked}
                        onChange={onChange}
                    />
                    <FormCheckFix>
                        <FormCheckArrow
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 10.889 10.508"
                        >
                            <path
                                fill="currentColor"
                                d="M13.684,4.09l-.977-.664a.593.593,0,0,0-.821.155L7.1,10.642l-2.2-2.2a.591.591,0,0,0-.834,0l-.836.836a.593.593,0,0,0,0,.836L6.611,13.5a1.148,1.148,0,0,0,.755.334.909.909,0,0,0,.713-.421l5.763-8.5A.59.59,0,0,0,13.684,4.09Z"
                                transform="translate(-3.055 -3.324)"
                            />
                        </FormCheckArrow>
                    </FormCheckFix>
                    <FormCheckLabel>
                        <FormCheckText>{label}</FormCheckText>
                        {route && slug && link_text ? (
                            <Link route={route} params={{ slug }}>
                                <FormCheckLink white={white}>
                                    {/* <NewwindowIcon /> */}
                                    <FormCheckLinkText>
                                        {link_text}
                                    </FormCheckLinkText>
                                </FormCheckLink>
                            </Link>
                        ) : null}
                    </FormCheckLabel>
                </FormCheckBlock>
                <FormCheckError>{errors}</FormCheckError>
            </FormCheck>
        );
    }
}

const FormCheck = styled.div``;

const FormCheckBlock = styled.label`
    position: relative;
    cursor: pointer;
    display: flex;
    align-items: center;
`;

const FormCheckFix = styled.div`
    transition: all 0.1s;
    position: relative;
    flex: 0 0 2em;
    width: 2em;
    height: 2em;
    border: 2px solid ${colors.grey_light};
    border-radius: ${global.border_radius};
    display: flex;
    align-items: center;
    justify-content: center;
    /* ${FormCheckBlock}:hover & {
        border: 2px solid ${colors.red};
    } */
`;

const FormCheckArrow = styled.svg`
    transition: all 0.1s;
    color: ${colors.grey_light};
    width: 0.8em;
    height: 0.8em;
    opacity: 0.4;
`;

const FormCheckInput = styled.input`
    opacity: 0;
    margin: 0;
    padding: 0;
    width: 1px;
    height: 0;
    display: block;
    &:checked + ${FormCheckFix} {
        border: 2px solid ${colors.red};
    }
    &:checked + ${FormCheckFix} ${FormCheckArrow} {
        color: ${colors.red};
        opacity: 1;
    }
`;

const FormCheckLabel = styled.div`
    display: block;
    padding-left: 0.8em;
`;
const FormCheckText = styled.div`
    font-size: 0.9em;
`;
const FormCheckLink = styled.a`
    display: inline-flex;
    align-items: center;
    font-size: 0.9em;
    color: ${props => (props.white ? colors.green : colors.blue)};
    &:hover {
        color: ${props => (props.white ? colors.red : colors.green)};
    }
    svg {
        width: 1em;
        height: 1em;
        flex: 1em;
        margin-right: 0.3em;
    }
`;
const FormCheckLinkText = styled.span``;

const FormCheckError = styled.div`
    margin-top: 0;
    padding-left: 2.6em;
    font-weight: 200;
    color: ${colors.red};
`;

export default FormCheckbox;
