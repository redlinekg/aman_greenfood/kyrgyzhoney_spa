import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { colors, down } from "~/config/var";

export default class NoticeBlock extends PureComponent {
    static propTypes = {
        icon: PT.any,
        title: PT.string,
        text: PT.string,
        children: PT.any
    };

    render() {
        const { icon, title, text, children } = this.props;
        return (
            <SNotice>
                <SBlock>
                    <SIcon>{icon}</SIcon>
                    <SInfo>
                        <SH4>{title}</SH4>
                        {text && <SText>{text}</SText>}
                        {children ? <SChildren>{children}</SChildren> : null}
                    </SInfo>
                </SBlock>
            </SNotice>
        );
    }
}

const SNotice = styled.div`
    max-width: 600px;
    border: 2px solid ${colors.grey_light4};
    padding: 2em 1em;
    background-color: ${colors.white};
    ${down("md")} {
        padding: 15px;
    }
`;
const SBlock = styled.div`
    text-align: center;
    display: flex;
    text-align: left;
    align-items: center;
    ${down("md")} {
    }
`;
const SIcon = styled.div`
    width: fit-content;
    margin: 0 auto;
    margin: 0;
    svg {
        width: 3.6em;
        height: 3.6em;
        flex: 0 0 3.6em;
        ${down("md")} {
            width: 2.8em;
            height: 2.8em;
            flex: 0 0 2.8em;
        }
    }
`;
const SInfo = styled.div`
    padding-left: 1.2em;
    ${down("md")} {
        padding-left: 0.5em;
    }
`;
const SH4 = styled.h4`
    font-size: 1.2em;
    font-weight: 700;
    margin-bottom: 0.2em;
    margin-top: 0.5em;
    color: ${colors.black};
    margin-bottom: 0.4em;
    margin-top: 0;
    ${down("md")} {
        font-size: 1.1em;
    }
`;
const SText = styled.div`
    font-size: 0.9em;
    color: ${colors.grey};
    max-width: 480px;
    ${down("md")} {
        display: none;
    }
`;
const SChildren = styled.div`
    margin-top: 1em;
`;
