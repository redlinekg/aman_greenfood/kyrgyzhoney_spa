import styled from "@emotion/styled";
import { inject, observer } from "mobx-react";
import { position } from "polished";
import PT from "prop-types";
import { compose } from "ramda";
import React from "react";
import { ImageBg, QuantityInput } from "~/components";
import { CrossIcon } from "~/components/icons";
import { colors, down, global, only, up } from "~/config/var";
import { withTranslation } from "~/i18n";
import { Link } from "~/routes";

let Card = ({ line: { product, quantity } }) => {
    return (
        <SCard>
            <SImgBlock>
                <Link
                    route="product"
                    params={{
                        slug: product.slug
                    }}
                >
                    <a>
                        <SImg>
                            <ImageBg
                                src={product.image.url_path}
                                alt={product.title}
                                width={320}
                                height={320}
                                object_fit="contain"
                                sizes={{
                                    lg: {
                                        width: 250,
                                        height: 250
                                    }
                                }}
                            />
                        </SImg>
                    </a>
                </Link>
            </SImgBlock>
            <SInfo>
                <SName>
                    <Link
                        route="product"
                        params={{
                            slug: product.slug
                        }}
                    >
                        <a>{product.title}</a>
                    </Link>
                </SName>
                <SVolume>{product.volume}</SVolume>
            </SInfo>
            <SQuality>
                <QuantityInput
                    onChange={product.setCartCount}
                    onIncrease={product.cartIncrease}
                    onDecrease={product.cartDecrease}
                    value={quantity}
                    min={1}
                    max={99999}
                />
            </SQuality>
            <SDelete onClick={product.cartRemove}>
                <CrossIcon />
            </SDelete>
        </SCard>
    );
};

Card.propTypes = {
    line: PT.object
};

Card = compose(observer)(Card);

const SCard = styled.div`
    display: flex;
    align-items: center;
    ${up("md")} {
        margin-bottom: 1em;
        &:last-of-type {
            margin-bottom: 0;
        }
    }
    ${down("sm")} {
        flex-wrap: wrap;
        padding-bottom: 1em;
        margin-bottom: 1em;
        border-bottom: 1px solid ${colors.grey_light4};
    }
`;
const SImgBlock = styled.div`
    background-color: ${colors.blue_light3};
    border-radius: ${global.border_radius};
    position: relative;
    overflow: hidden;
    height: 0;
    display: block;
    width: 3.6em;
    height: 3.6em;
    flex: 0 0 3.6em;
    ${only("lg")} {
        display: none;
    }
    ${down("sm")} {
        width: 20%;
        height: 3em;
        flex: 0 0 20%;
    }
`;
const SImg = styled.div`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    width: 65%;
    height: 80%;
`;
const SInfo = styled.div`
    width: 100%;
    padding: 0 1em 0 1em;
    ${only("lg")} {
        padding-left: 0;
    }
    ${down("sm")} {
        width: 80%;
    }
    ${down("sm")} {
    }
`;
const SName = styled.div`
    width: 100%;
    font-size: 1em;
    a {
        color: ${colors.black};
        font-weight: 500;
        display: block;
        margin-bottom: 0.2em;
        ${down("sm")} {
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
`;
const SVolume = styled.div`
    font-weight: 600;
    color: ${colors.grey};
    font-size: 0.7em;
    ${down("sm")} {
        font-size: 0.8em;
    }
`;
const SQuality = styled.div`
    margin-left: auto;
    ${down("sm")} {
        margin-left: 0;
        width: calc(100% - 3em);
        margin-top: 0.8em;
        .SQuantityInput,
        .SQuantityInputInput {
            width: 100%;
        }
    }
`;

const SDelete = styled.div`
    padding-left: 2em;
    color: ${colors.red};
    cursor: pointer;
    transition: color 0.2s ease-out;
    ${down("sm")} {
        width: 3em;
        display: flex;
        justify-content: center;
        padding-left: 0;
        margin-top: 0.8em;
    }
    svg {
        width: 0.9em;
        height: 0.9em;
    }
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
`;

const ModalOrderCart = compose(
    withTranslation(["common"]),
    inject("cart_store"),
    observer
)(({ t, cart_store }) => {
    return (
        <SModalOrderCart>
            <STitle>{t("modal_order_cart")}</STitle>
            {cart_store.lines.map((l, idx) => (
                <Card key={idx} line={l} />
            ))}
            {cart_store.lines.length ? null : "Ваша корзина пуста"}
        </SModalOrderCart>
    );
});

ModalOrderCart.propTypes = {
    t: PT.any
};

const SModalOrderCart = styled.div`
    ${down("sm")} {
    }
`;

const STitle = styled.div`
    font-size: 1.8em;
    font-weight: 600;
    margin: 0 0 0.5em;
    ${down("lg")} {
        font-size: 1.6em;
    }
    ${down("sm")} {
        font-size: 1.2em;
    }
`;

export default ModalOrderCart;
