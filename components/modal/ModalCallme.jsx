import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import { Modal } from "~/components";
import { OrderForm } from "~/components/form";
import { withTranslation } from "~/i18n";

@inject("callme_form_store")
@observer
class ModalCallme extends Component {
    static propTypes = {
        t: PT.any.isRequired,
        callme_form_store: PT.object.isRequired
    };

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }

    handleSubmit = async values => {
        const { callme_form_store } = this.props;
        await callme_form_store.sendOrder(values);
    };

    render() {
        const { t } = this.props;
        return (
            <Modal
                name="callme"
                loading={false}
                form={
                    <OrderForm
                        title={t("modal_callme_title")}
                        text={t("modal_callme_text")}
                        button={t("modal_callme_send")}
                        success_title={t("modal_callme_success_title")}
                        success_text={t("modal_callme_success_text")}
                        name="callme"
                        onSubmit={this.handleSubmit}
                    />
                }
            />
        );
    }
}

export default withTranslation(["common"])(ModalCallme);
