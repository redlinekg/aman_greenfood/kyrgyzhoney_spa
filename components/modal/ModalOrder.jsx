import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import ModalOrderCart from "./ModalOrderCart";
import { Modal } from "~/components";
import { OrderForm } from "~/components/form";
import { withTranslation } from "~/i18n";
import { down } from "~/config/var";
import { inject, observer } from "mobx-react";

@withTranslation(["common"])
@inject("cart_form_store", "cart_store")
@observer
class ModalOrder extends Component {
    static propTypes = {
        t: PT.any.isRequired,
        cart_form_store: PT.object.isRequired,
        cart_store: PT.object.isRequired
    };

    handleOrder = async values => {
        const { cart_form_store, cart_store } = this.props;
        await cart_form_store.sendOrder({
            ...values,
            lines: cart_store.lines.map(line => ({
                quantity: line.quantity,
                product_id: line.product.id
            }))
        });
        cart_store.clean();
    };

    render() {
        const { t } = this.props;
        return (
            <Modal
                name="order"
                loading={false}
                padding_show={false}
                form={
                    <OrderForm
                        title={t("modal_order_title")}
                        text={t("modal_order_text")}
                        button={t("modal_order_send")}
                        success_title={t("modal_order_success_title")}
                        success_text={t("modal_order_success_text")}
                        name="order"
                        onSubmit={this.handleOrder}
                    />
                }
            >
                <SModalOrder>
                    <ModalOrderCart />
                </SModalOrder>
            </Modal>
        );
    }
}

const SModalOrder = styled.div`
    ${down("sm")} {
    }
`;

export default ModalOrder;
