import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { Component } from "react";
import { Modal } from "~/components";
import { OrderForm } from "~/components/form";
import { withTranslation } from "~/i18n";

@inject("cart_form_store", "product_store")
@observer
@withTranslation(["common"])
class ModalOneClick extends Component {
    static propTypes = {
        t: PT.any.isRequired,
        cart_form_store: PT.object.isRequired,
        product_store: PT.object.isRequired,
        quantity: PT.number.isRequired
    };

    handleOrder = async values => {
        const { cart_form_store, product_store, quantity } = this.props;
        await cart_form_store.sendOrder({
            ...values,
            lines: [
                {
                    quantity,
                    product_id: product_store.item.id
                }
            ]
        });
    };

    render() {
        const { t } = this.props;
        return (
            <Modal
                name="one_click"
                loading={false}
                form={
                    <OrderForm
                        title={t("modal_one_click_title")}
                        text={t("modal_one_click_text")}
                        button={t("modal_one_click_send")}
                        success_title={t("modal_one_click_success_title")}
                        success_text={t("modal_one_click_success_text")}
                        name="one_click"
                        onSubmit={this.handleOrder}
                    />
                }
            />
        );
    }
}

export default ModalOneClick;
