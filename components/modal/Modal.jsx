import React from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";
import { Global, css } from "@emotion/core";
import { position } from "polished";

import { Container } from "~/components";
import { useModalManager, manager } from "@vlzh/react-modal-manager";
import { withTranslation } from "~/i18n";
import { up, down, colors, global } from "~/config/var";

let scroll_point = 0;

function addBodyClass() {
    if (scroll_point) {
        return;
    }
    scroll_point = window.scrollY;
    document.querySelector("#global_content").style.top = `-${scroll_point}px`;
    document.querySelector("body").classList.add("open_modal");
    window.scrollTo(0, 0);
}

function removeBodyClass() {
    if (scroll_point === null) {
        return;
    }
    document.querySelector("#global_content").style.top = "0px";
    document.querySelector("body").classList.remove("open_modal");
    window.scrollTo(0, scroll_point);
    scroll_point = null;
}

const Modal = ({ t, children, name, form }) => {
    const { isOpen, closeModal } = useModalManager(name);
    const is_open = isOpen(name);

    manager.on("afterOpen", () => {
        addBodyClass();
    });
    manager.on("afterClose", () => {
        removeBodyClass();
    });
    return (
        <SModal
            ariaHideApp={false}
            className={CN({
                open: is_open,
                form: form,
                children: children
            })}
        >
            <SBody>
                <Container>
                    <SBlock>
                        <SClose>
                            <SCloseBlock onClick={() => closeModal(name)}>
                                {t("modal_close")}
                            </SCloseBlock>
                        </SClose>
                        {form && <SForm>{form}</SForm>}
                        {children && <SContent>{children}</SContent>}
                    </SBlock>
                </Container>
            </SBody>
            <SOverlay onClick={() => closeModal(name)} />
            <ModalStyle />
        </SModal>
    );
};

Modal.defaultProps = {
    padding_show: true
};

Modal.propTypes = {
    t: PT.any,
    form: PT.any,
    children: PT.any,
    name: PT.string.isRequired,
    padding_show: PT.bool,
    // hoooks
    onOpen: PT.func,
    onClose: PT.func
};

Modal.getInitialProps = async () => {
    return { namespacesRequired: ["common"] };
};

const padding_y = "3.5em";
const padding_x = "2.4em";
const padding_y_lg = "2.5em";
const padding_x_lg = "1.4em";
const padding_y_sm = "1.5em";
const padding_x_sm = "15px";
const width_close = "6.6em";

const SModal = styled.div`
    ${position("absolute", "0", "0", null, null)};
    z-index: 26;
    min-height: 100%;
    width: 100%;
    visibility: hidden;
    backface-visibility: hidden;
    height: 0;
    padding: 3em 1em;
    &.open {
        visibility: visible;
        height: auto;
    }
    ${up("lg")} {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }
    ${down("sm")} {
        padding: 0;
    }
`;
const SOverlay = styled.div`
    ${position("fixed", "0", null, null, "0")};
    width: 100%;
    height: 0;
    background-color: ${colors.black};
    z-index: 1;
    transition: opacity 0.3s cubic-bezier(0.645, 0.045, 0.355, 1) 0.2s;
    opacity: 0;
    ${SModal}.open & {
        opacity: 0.9;
        height: 100%;
    }
    ${down("sm")} {
        background-color: ${colors.white};
        ${SModal}.open & {
            opacity: 1;
        }
    }
`;
const SBody = styled.div`
    position: relative;
    z-index: 2;
    opacity: 0;
    transition: top 0.3s cubic-bezier(0.645, 0.045, 0.355, 1) 0.2s,
        opacity 0.3s cubic-bezier(0.645, 0.045, 0.355, 1) 0.2s;
    top: -400px;
    ${SModal}.children & {
        width: 100%;
    }
    ${SModal}.open & {
        top: 0;
        opacity: 1;
    }
    ${down("sm")} {
        .ContainerBlock {
            padding: 0;
        }
    }
`;

const SBlock = styled.div`
    display: flex;
    flex-wrap: wrap;
    background-color: ${colors.white};
    border-radius: ${global.border_radius};
    position: relative;
    width: 640px;
    padding-right: ${width_close};
    overflow: hidden;
    height: 0;
    ${SModal}.open & {
        height: auto;
        overflow: inherit;
    }
    ${SModal}.children & {
        width: 100%;
    }
    ${down("md")} {
        width: 100%;
    }
    ${down("md")} {
        padding-right: 0;
    }
`;

const SForm = styled.div`
    background-color: ${colors.blue_light3};
    padding: ${padding_y} ${padding_x} ${padding_y} ${padding_x};
    ${down("lg")} {
        padding: ${padding_y_lg} ${padding_x_lg} ${padding_y_lg} ${padding_x_lg};
    }
    ${down("sm")} {
        padding: ${padding_y_sm} ${padding_x_sm} 8em ${padding_x_sm};
    }
    width: 100%;
    ${down("md")} {
        position: relative;
        order: 2;
    }
    ${SModal}.children & {
        width: 40%;
        flex: 0 0 40%;
        ${down("md")} {
            width: 100%;
            flex: 0 0 100%;
        }
    }
`;

const SContent = styled.div`
    opacity: 0;
    transition: opacity 0.1s cubic-bezier(0.645, 0.045, 0.355, 1);
    width: 60%;
    flex: 0 0 60%;
    padding: ${padding_y} ${padding_x} ${padding_y} ${padding_x};
    ${down("lg")} {
        padding: ${padding_y_lg} ${padding_x_lg} ${padding_y_lg} ${padding_x_lg};
    }
    ${down("md")} {
        position: relative;
        order: 1;
        width: 100%;
        flex: 0 0 100%;
    }
    ${down("sm")} {
        padding: ${padding_y_sm} ${padding_x_sm} ${padding_y_sm} ${padding_x_sm};
    }
    ${SModal}.open & {
        opacity: 1;
    }
`;
const SClose = styled.div`
    ${up("md")} {
        ${position("absolute", "0", "0", null, null)};
        height: 100%;
        padding: ${padding_y} 0;
        background-color: ${colors.grey_light5};
        width: ${width_close};
        text-align: center;
    }
    ${down("sm")} {
        height: 2.6em;
        width: 100%;
    }
`;
const SCloseBlock = styled.div`
    ${position("sticky", padding_y, null, null, null)};
    font-size: 1.1em;
    color: ${colors.black};
    cursor: pointer;
    transition: color 0.2s ease-out;
    font-weight: 600;
    z-index: 10;
    ${down("md")} {
        top: ${padding_y_lg};
    }
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
    ${down("sm")} {
        ${position("fixed", "0", null, null, "0")};
        background-color: ${colors.blue};
        color: ${colors.white};
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100%;
        height: 2.6em;
    }
`;

const ModalStyle = () => (
    <Global
        styles={css`
            body.open_modal {
                #global_content {
                    position: fixed !important;
                    width: 100%;
                }
                .sticky {
                    position: relative;
                    top: 0;
                }
            }
        `}
    />
);

/////////////////////

export default withTranslation(["common"])(Modal);
