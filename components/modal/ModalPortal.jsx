import PT from "prop-types";
import ReactDOM from "react-dom";
import { isNode } from "browser-or-node";

const ModalPortal = ({ children }) => {
    if (isNode) return null;
    return ReactDOM.createPortal(
        children,
        document.querySelector("#modal_content")
    );
};

ModalPortal.propTypes = {
    childrend: PT.any
};

export default ModalPortal;
