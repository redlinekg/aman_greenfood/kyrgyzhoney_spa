import React from "react";
import PropTypes from "prop-types";
import { position } from "polished";
import styled from "@emotion/styled";
import { Global, css, keyframes } from "@emotion/core";

import { colors, down, main_slider } from "~/config/var";

class MainSliderItem extends React.Component {
    static defaultProps = {
        slide: {
            image: ""
        }
    };

    static propTypes = {
        slide: PropTypes.object
    };

    render() {
        const {
            slide: { image }
        } = this.props;
        return (
            <Slide>
                <ImageBlock>
                    <ImageBlockFigure className="ImageBlockFigure">
                        <ImageBlockBg
                            className="ImageBlockBg"
                            style={{ backgroundImage: "url(" + image + ")" }}
                        />
                    </ImageBlockFigure>
                </ImageBlock>
                <SlideStyle />
            </Slide>
        );
    }
}

const spin = keyframes`
    0% {
        height: 0%;
    }
    50% {
        height: 100%;
    }
    100% {
        height: 0%;
    }
`;

const SlideStyle = () => (
    <Global
        styles={css`
            .main_slider_image {
                .slick-slide {
                    pointer-events: none;
                    opacity: 1 !important;
                    position: relative;
                    &:last-of-type {
                        &:before {
                            z-index: 3;
                        }
                    }
                    .ImageBlockFigure {
                        /* transform: scale(0.95); */
                        transition: transform 0.3s ease-in-out;
                        &:before {
                            content: "";
                            ${position("absolute", "0", "0", null, null)};
                            width: 100%;
                            height: 0;
                            background-color: ${colors.grey_light2};
                            z-index: 3;
                        }
                    }
                    .ImageBlockBg {
                        opacity: 0;
                        /* transition: opacity 0.3s ease-in-out; */
                        z-index: 1;
                    }
                    &.slick-active {
                        pointer-events: auto;
                        .ImageBlockFigure {
                            /* transform: scale(1); */
                            transition: transform 0.3s ease-in-out;
                            &:before {
                                animation: ${spin} 0.5s linear;
                                animation-iteration-count: 1;
                            }
                        }
                        .ImageBlockBg {
                            opacity: 1;
                            transition: opacity 0.3s ease-in-out 0.3s;
                        }
                    }
                }
            }
        `}
    />
);

const Slide = styled.div`
    display: flex;
    justify-content: flex-end;
    ${down("md")} {
    }
`;

const ImageBlock = styled.div`
    width: 100%;
    height: ${main_slider.height};
    min-height: ${main_slider.min_height};
    position: relative;
    overflow: hidden;
    ${down("md")} {
        height: ${main_slider.height_md};
        min-height: ${main_slider.min_height_md};
    }
    ${down("sm")} {
        width: 100%;
        height: ${main_slider.height_sm};
        min-height: ${main_slider.min_height_sm};
    }
`;
const ImageBlockFigure = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    margin: 0 auto;
    height: 100%;
    width: 100%;
    transition: transform 0.5s cubic-bezier(0.19, 1, 0.22, 1);
    z-index: 2;
`;
const ImageBlockBg = styled.div`
    position: relative;
    display: block;
    background-size: cover;
    background-position: center center;
    height: 100%;
    width: 100%;
    ${down("sm")} {
        &:before {
            content: "";
            ${position("absolute", "0", "0", null, null)};
            width: 100%;
            height: 100%;
            background-color: ${colors.black};
            opacity: 0.45;
        }
    }
`;

export default MainSliderItem;
