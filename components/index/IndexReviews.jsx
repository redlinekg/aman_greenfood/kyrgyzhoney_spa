import styled from "@emotion/styled";
import Slider from "@onedior/react-slick";
import CN from "classnames";
import { inject, observer } from "mobx-react";
import { position } from "polished";
import PT from "prop-types";
import React, { Component, createRef } from "react";
import { Container, IndexBlock, IndexReviewsCard } from "~/components";
import { ArrowLeftIcon, ArrowRightIcon } from "~/components/icons";
import { colors, down } from "~/config/var";
import { withTranslation } from "~/i18n";

@withTranslation(["common"])
@inject("reviews_store")
@observer
class IndexReviews extends Component {
    static propTypes = {
        t: PT.any.isRequired,
        reviews_store: PT.object.isRequired
    };
    state = {
        slideIndex: 0
    };
    slider = createRef();

    next = () => {
        this.slider.current.slickNext();
    };
    previous = () => {
        this.slider.current.slickPrev();
    };

    render() {
        const {
            t,
            reviews_store: { objects }
        } = this.props;
        const SETTINGS = {
            lazyLoad: true,
            dots: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            swipe: true,
            autoplay: false,
            afterChange: current => {
                this.setState({ slideIndex: current });
            },
            adaptiveHeight: true
        };
        return (
            <IndexBlock
                title={t("index_reviews_title")}
                text={t("index_reviews_text")}
                bg_color="#FBFBF6"
                children_right={
                    <SThumb>
                        <SArrow onClick={() => this.previous()}>
                            <ArrowLeftIcon />
                        </SArrow>
                        <SThumbBlock>
                            {objects.map((review, idx) => (
                                <SThumbItem
                                    onClick={() =>
                                        this.slider.current.slickGoTo(idx)
                                    }
                                    key={idx}
                                    className={CN({
                                        active: this.state.slideIndex == idx
                                    })}
                                >
                                    <SThumbImg
                                        src={review.avatar.url_path}
                                        alt={review.name}
                                    />
                                </SThumbItem>
                            ))}
                            <SThumbItem className="SThumbActive" />
                        </SThumbBlock>
                        <SArrow onClick={() => this.next()}>
                            <ArrowRightIcon />
                        </SArrow>
                    </SThumb>
                }
            >
                <Container>
                    <Block>
                        <Slider ref={this.slider} {...SETTINGS}>
                            {objects.map(review => (
                                <IndexReviewsCard
                                    key={review.id}
                                    card={review}
                                />
                            ))}
                        </Slider>
                    </Block>
                </Container>
            </IndexBlock>
        );
    }
}

// const SIndexReviews = styled.div``;

const Block = styled.div`
    .main_slider_reviews {
        .slick-slider {
            /* padding-top: 5em; */
        }
        .slick-slide {
            z-index: 1;
            &.slick-active {
                z-index: 2;
            }
        }
    }
`;
///////////////
///////////////
///////////////
const SThumb = styled.div`
    display: flex;
    align-items: center;
    position: relative;
    z-index: 1;
    ${down("sm")} {
        justify-content: space-between;
    }
`;
const SArrow = styled.div`
    padding: 0 1em;
    cursor: pointer;
    transition: all 0.1s ease-out;
    &:hover {
        color: ${colors.blue};
    }
    ${down("sm")} {
        padding: 0;
    }
    svg {
        width: 1.2em;
        height: 1.2em;
    }
`;
const SThumbBlock = styled.ul`
    display: flex;
    align-items: center;
    position: relative;
`;
const SThumbItem = styled.li`
    position: relative;
    z-index: 2;
    display: block;
    float: left;
    margin: 0 0.8em;
    flex: 0 0 2.4em;
    width: 2.4em;
    height: 2.4em;
    cursor: pointer;
    opacity: 2;
    border-radius: 50%;
    overflow: hidden;
    ${down("sm")} {
        margin: 0 0.6em;
    }
    &.SThumbActive {
        ${position("absolute", null, null, null, "0")};
        margin: 0;
        width: 4em;
        transition: transform 0.3s ease;
        cursor: default;
        &:before {
            content: "";
            ${position("absolute", "0", null, null, "50%")};
            width: 2.4em;
            height: 2.4em;
            border-radius: 50%;
            background-color: ${colors.yellow};
            transform: translateX(-50%);
            opacity: 1;
            cursor: default;
        }
        ${down("sm")} {
            width: 3.6em;
            &:before {
            }
        }
    }
    &.active {
        > div {
            opacity: 0;
        }
        &:nth-of-type(1) ~ li.SThumbActive {
            transform: translateX(0);
        }
        &:nth-of-type(2) ~ li.SThumbActive {
            transform: translateX(100%);
        }
        &:nth-of-type(3) ~ li.SThumbActive {
            transform: translateX(200%);
        }
        &:nth-of-type(4) ~ li.SThumbActive {
            transform: translateX(300%);
        }
        &:nth-of-type(5) ~ li.SThumbActive {
            transform: translateX(400%);
        }
        &:nth-of-type(6) ~ li.SThumbActive {
            transform: translateX(500%);
        }
    }
`;
const SThumbImg = styled.img`
    width: 100%;
    display: block;
    transition: opacity 0.6s;
    ${SThumbBlock}.active & {
        opacity: 0.5;
    }
`;

export default IndexReviews;
