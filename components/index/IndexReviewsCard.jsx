import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position, lighten } from "polished";

import {
    FacebookIcon,
    InstagramIcon,
    TwitterIcon,
    VkIcon,
    NewwindowIcon
} from "~/components/icons";
import { EditorJs, ImageBg } from "~/components";
import { colors, down } from "~/config/var";

class IndexReviewsCard extends Component {
    static defaultProps = {
        card: {
            icon: null
        }
    };
    static propTypes = {
        card: PT.object
    };
    render() {
        const {
            card: {
                avatar,
                name,
                country,
                title,
                body,
                social_type,
                social_link
            }
        } = this.props;
        return (
            <Root>
                <SInfo>
                    <SPhotoBlock>
                        <SPhoto>
                            <ImageBg
                                src={avatar.absolute_path}
                                alt={name}
                                width={200}
                                height={200}
                                sizes={{
                                    lg: {
                                        width: 130,
                                        height: 130
                                    },
                                    md: {
                                        width: 130,
                                        height: 130
                                    },
                                    sm: {
                                        width: 104,
                                        height: 104
                                    }
                                }}
                            />
                        </SPhoto>
                        <SInfoLink href={social_link} target="blank">
                            <SIcon>
                                {social_type == "facebook" ? (
                                    <FacebookIcon />
                                ) : social_type == "instagram" ? (
                                    <InstagramIcon />
                                ) : social_type == "twitter" ? (
                                    <TwitterIcon />
                                ) : social_type == "vkontakte" ? (
                                    <VkIcon />
                                ) : (
                                    <NewwindowIcon />
                                )}
                            </SIcon>
                        </SInfoLink>
                    </SPhotoBlock>
                    <SContacts>
                        <SName>{name}</SName>
                        <SCountry>{country}</SCountry>
                    </SContacts>
                </SInfo>
                <STextBlock>
                    <STitle>{title}</STitle>
                    <SText>
                        <EditorJs blocks_json={body} container_show={false} />
                    </SText>
                </STextBlock>
            </Root>
        );
    }
}

const Root = styled.div`
    display: flex;
    align-items: flex-start;
    ${down("sm")} {
        display: block;
    }
`;
const SInfo = styled.div`
    flex: 0 0 18%;
    width: 18%;
    padding: 0 2em 0 0em;
    ${down("md")} {
        padding: 0;
        width: 100%;
    }
    ${down("sm")} {
        width: 100%;
        display: flex;
        align-items: center;
    }
`;
const SPhotoBlock = styled.div`
    position: relative;
    margin-bottom: 1.5em;
    ${down("sm")} {
        padding-bottom: 0;
        width: 26%;
        flex: 0 0 26%;
    }
`;
const SPhoto = styled.div`
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 100%;
    display: block;
    border-radius: 50%;
    img {
        ${position("absolute", "0", null, null, "0")}
        width: 100%;
        height: 100%;
    }
`;
const SContacts = styled.div`
    ${down("sm")} {
        padding-left: 0.8em;
    }
`;
const SInfoLink = styled.a`
    ${position("absolute", null, "0.3em", "0.3em", null)};
    display: block;
    color: ${lighten(0.26, colors.black)};
    &:hover {
        color: ${colors.black};
    }
    ${down("sm")} {
        right: -0.2em;
        bottom: -0.2em;
        font-size: 0.7em;
    }
`;
const SIcon = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${colors.white};
    border-radius: 50%;
    width: 2.4em;
    height: 2.4em;
    box-shadow: 0 6px 14px 5px rgba(0, 0, 0, 0.1);
    svg {
        width: 1.2em;
        height: 1.2em;
    }
`;
const SName = styled.div`
    text-align: right;
    font-weight: 800;
    font-weight: 1.2em;
    font-size: 1.3em;
    ${down("sm")} {
        text-align: left;
        font-size: 1em;
    }
`;
const SCountry = styled.div`
    text-align: right;
    font-weight: 400;
    color: ${colors.grey};
    font-size: 0.9em;
    ${down("sm")} {
        text-align: left;
    }
`;

const STextBlock = styled.div`
    flex: 0 0 82%;
    width: 82%;
    padding: 0 2em 0 3em;
    ${down("md")} {
        padding: 0 2em 0 1em;
    }
    ${down("sm")} {
        padding: 0;
        width: 100%;
    }
`;
const STitle = styled.div`
    font-size: 1.8em;
    margin-bottom: 0.4em;
    font-weight: 800;
    ${down("md")} {
        font-size: 1em;
    }
`;
const SText = styled.div`
    .SContentContainer {
        max-width: none;
    }
    ${down("md")} {
        font-size: 0.9em;
    }
`;

export default IndexReviewsCard;
