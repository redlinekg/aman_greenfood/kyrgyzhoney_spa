import React from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { Global, css } from "@emotion/core";
import { position } from "polished";

import { ArrowRightIcon } from "~/components/icons";
import { Container, ImageBg } from "~/components";
import { Link } from "~/routes";
import { colors, down, fonts } from "~/config/var";

const IndexSliderItem = ({ slide, id }) => {
    return (
        <SSlide
            style={{
                backgroundImage: slide.background_image
                    ? `url("${slide.background_image}"`
                    : undefined,
                backgroundColor: slide.background_color
            }}
        >
            <Container>
                <SBlock>
                    <SInfo>
                        <SSubTitle className="SSliderSubTitle">
                            {slide.sub_title}
                        </SSubTitle>
                        <STitle>
                            <TitleBlock>
                                <STitleOne>
                                    <STitleOneText className="SSTitleOneText">
                                        {slide.title}
                                    </STitleOneText>
                                </STitleOne>
                                <STitleTwo>
                                    <STitleTwoText className="SSTitleTwoText">
                                        {slide.title2}
                                    </STitleTwoText>
                                </STitleTwo>
                            </TitleBlock>
                        </STitle>
                        {slide.button_text && slide.link ? (
                            <Link href={slide.link}>
                                <a>
                                    <SButton className="SSliderButton">
                                        <SButtonText>
                                            {slide.button_text}
                                        </SButtonText>
                                        <ArrowRightIcon />
                                    </SButton>
                                </a>
                            </Link>
                        ) : null}
                    </SInfo>
                    <SMedia className="SSliderMedia">
                        {slide.video.absolute_path ? (
                            <SVideo>
                                <video
                                    id={`index_slide_${id}`}
                                    autoPlay
                                    loop
                                    muted
                                    playsInline
                                >
                                    <source
                                        src={slide.video.absolute_path}
                                        type="video/mp4"
                                    />
                                </video>
                            </SVideo>
                        ) : slide.image ? (
                            <SImage>
                                <ImageBg
                                    src={slide.image.absolute_path}
                                    alt={slide.title}
                                    width={320}
                                    height={320}
                                    object_fit="contain"
                                    sizes={{
                                        lg: {
                                            width: 250,
                                            height: 250
                                        }
                                    }}
                                />
                            </SImage>
                        ) : null}
                    </SMedia>
                    <SDescription className="SSliderDescription">
                        {slide.description}
                    </SDescription>
                </SBlock>
            </Container>
            <SlideItemStyle />
        </SSlide>
    );
};

IndexSliderItem.propTypes = {
    slide: PT.object,
    id: PT.number
};

const SSlide = styled.div`
    ${down("md")} {
    }
`;
const SBlock = styled.div`
    display: flex;
    align-items: center;
    height: 640px;
    padding: 2em 5em 0;
    ${down("xl")} {
        padding: 2em 4em 0;
    }
    ${down("lg")} {
        padding: 3em 4em 0;
    }
    ${down("md")} {
        padding: 0 0 2em;
        height: 420px;
    }
    ${down("sm")} {
        height: 520px;
        display: block;
        padding: 1.5em 0;
    }
    @media (max-width: 330px) {
        height: 460px;
    }
`;
///////////////
const SInfo = styled.div`
    width: 40%;
    ${down("lg")} {
        width: 52%;
    }
    ${down("sm")} {
        width: 100%;
        margin-bottom: 1em;
    }
`;
const SSubTitle = styled.div`
    font-weight: 600;
    color: ${colors.grey};
    /* !!!!!!!! */
    opacity: 0;
    transition: all 0.4s ease-out 0.1s;
    ${down("sm")} {
        margin-bottom: 0.1em;
    }
`;

const STitle = styled.div`
    margin-bottom: 0.5em;
    height: 130px;
    margin-bottom: 2em;
    line-height: 1.2;
    display: block;
    position: relative;
    z-index: 3;
    ${down("xl")} {
        margin-bottom: 1em;
    }
    ${down("sm")} {
        height: 80px;
        margin-bottom: 0.7em;
    }
`;

const SSTitleText = css`
    display: inline-block;
    transform: translate3d(0, 140%, 0);
    opacity: 0;
    transition: transform 0.4s ease-out;
    font-family: ${fonts.ff_title};
    color: ${colors.gold_light};
    font-size: 3.4em;
`;

const TitleBlock = styled.div``;
const STitleOne = styled.div`
    display: block;
    overflow-y: hidden;
`;
const STitleOneText = styled.div`
    ${SSTitleText};
    ${down("xl")} {
        font-size: 2.8em;
    }
    ${down("sm")} {
        font-size: 1.8em;
    }
`;
const STitleTwo = styled.div`
    display: block;
    overflow-y: hidden;
`;
const STitleTwoText = styled.div`
    ${SSTitleText};
    ${down("xl")} {
        font-size: 2.8em;
    }
    ${down("sm")} {
        font-size: 1.8em;
    }
`;
/////////////////////
/////////////////////
/////////////////////
/////////////////////
const SButton = styled.div`
    display: inline-flex;
    align-items: center;
    color: ${colors.blue_light};
    @media (hover) {
        &:hover {
            color: ${colors.blue};
        }
    }
    svg {
        width: 0.6em;
        height: 0.6em;
    }
    transform: translate3d(30px, 0, 0);
    opacity: 0;
    transition: all 0.4s ease-out 0.3s;
`;
const SButtonText = styled.div`
    font-weight: 500;
    padding-right: 0.3em;
`;
///////////////
const SMedia = styled.div`
    width: 30%;
    transform: translate3d(50px, 0, 0);
    opacity: 0;
    transition: all 0.4s ease-out 0.2s;

    position: relative;
    overflow: hidden;
    height: 0;
    display: block;
    height: 360px;
    ${down("xl")} {
        height: 300px;
    }
    ${down("lg")} {
        width: 38%;
    }
    ${down("md")} {
    }
    ${down("sm")} {
        width: 300px;
        height: 300px;
    }
    @media (max-width: 330px) {
        width: 240px;
        height: 240px;
    }
`;
const SImage = styled.div`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    width: 95%;
    height: 90%;
`;
const SVideo = styled.div`
    video {
        ${position("absolute", "0", null, null, "0")};
        width: 100%;
        height: 100%;
    }
    video::-webkit-media-controls-panel-container {
        display: none !important;
    }
    video::-webkit-media-controls {
        display: none !important;
        opacity: 0;
    }
    video::-webkit-media-controls-start-playback-button {
        display: none !important;
    }
`;
///////////////
const SDescription = styled.div`
    width: 40%;
    color: ${colors.white};
    padding-left: 2em;
    /* !!!!!!!! */
    opacity: 0;
    transition: all 0.4s ease-out 0.4s;
    ${down("lg")} {
        display: none;
    }
`;

const SlideItemStyle = () => (
    <Global
        styles={css`
            .index_slider {
                .slick-slide {
                    opacity: 1 !important;
                    position: relative;
                    &.slick-active {
                        pointer-events: auto;
                        z-index: 2;
                        .SSliderSubTitle {
                            transform: translate3d(0, 0%, 0);
                            opacity: 1;
                        }
                        .SSTitleOneText,
                        .SSTitleTwoText {
                            transform: translate3d(0, 0%, 0);
                            opacity: 1;
                        }
                        .SSliderMedia {
                            transform: translate3d(0, 0%, 0);
                            opacity: 1;
                        }
                        .SSliderButton {
                            transform: translate3d(0, 0%, 0);
                            opacity: 1;
                        }
                        .SSliderDescription {
                            transform: translate3d(0, 0%, 0);
                            opacity: 1;
                        }
                    }
                }
            }
        `}
    />
);

export default IndexSliderItem;
