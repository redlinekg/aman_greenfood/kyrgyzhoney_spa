import React from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { withTranslation } from "~/i18n";
import { Container, PlainGrid, ProductCard, IndexBlock } from "~/components";

import { inject, observer } from "mobx-react";
import { compose } from "ramda";

let IndexProducts = ({ t, products_store: { objects } }) => {
    return (
        <SIndexProducts>
            <IndexBlock
                title={t("index_products_title")}
                text={t("index_products_text")}
                button_route="catalog"
                button_text={t("index_products_button")}
                show_padding_bottom={true}
            >
                <Container>
                    <PlainGrid xga={5} xl={5} lg={4} md={3} sm={2}>
                        {objects.map(product => (
                            <ProductCard
                                key={product.id}
                                card={product}
                                small={true}
                            />
                        ))}
                    </PlainGrid>
                </Container>
            </IndexBlock>
        </SIndexProducts>
    );
};

IndexProducts.propTypes = {
    t: PT.any,
    products_store: PT.object
};

IndexProducts = compose(
    withTranslation(["common"]),
    inject("products_store"),
    observer
)(IndexProducts);

const SIndexProducts = styled.div``;

export default IndexProducts;
