import React from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { compose } from "ramda";
import { inject, observer } from "mobx-react";

import { withTranslation } from "~/i18n";
import { IndexBlock, CompanyMap } from "~/components";

let IndexGeography = ({ t, settings_store, i18n }) => {
    let meta_title = settings_store.RU_INDEX_META_TITLE;

    if (i18n.language === "en") {
        meta_title = settings_store.EN_INDEX_META_TITLE;
    } else if (i18n.language === "kg") {
        meta_title = settings_store.KG_INDEX_META_TITLE;
    } else if (i18n.language === "cn") {
        meta_title = settings_store.CN_INDEX_META_TITLE;
    }

    return (
        <SIndexGeography>
            <IndexBlock
                title_level={1}
                title={meta_title}
                text={t("index_geography_text")}
            >
                <CompanyMap />
            </IndexBlock>
        </SIndexGeography>
    );
};

IndexGeography.propTypes = {
    t: PT.any,
    settings_store: PT.object.isRequired,
    i18n: PT.any
};

IndexGeography = compose(
    inject("settings_store"),
    withTranslation(["common"]),
    observer
)(IndexGeography);

const SIndexGeography = styled.div``;

export default IndexGeography;
