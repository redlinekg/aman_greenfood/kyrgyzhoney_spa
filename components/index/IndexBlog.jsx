import styled from "@emotion/styled";
import { inject, observer } from "mobx-react";
import PT from "prop-types";
import { compose } from "ramda";
import React from "react";
import { Container, IndexBlock, PlainGrid, PostCard } from "~/components";
import { withTranslation } from "~/i18n";

let IndexBlog = ({ t, articles_store: { objects } }) => {
    return (
        <SIndexBlog>
            <IndexBlock
                button_route="blog"
                button_text={t("index_blog_button")}
                title={t("index_blog_title")}
            >
                <Container>
                    <PlainGrid xga={3} xl={3} lg={3} md={3} sm={1}>
                        {objects.map(article => (
                            <PostCard key={article.id} publication={article} />
                        ))}
                    </PlainGrid>
                </Container>
            </IndexBlock>
        </SIndexBlog>
    );
};

IndexBlog.propTypes = {
    t: PT.any,
    articles_store: PT.object
};

IndexBlog = compose(
    withTranslation(["common"]),
    inject("articles_store"),
    observer
)(IndexBlog);

const SIndexBlog = styled.div``;

export default IndexBlog;
