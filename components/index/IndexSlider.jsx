import React, { PureComponent } from "react";
import styled from "@emotion/styled";
import { position } from "polished";
import { css } from "@emotion/core";
import Slider from "react-slick";
import { isBrowser } from "browser-or-node";

import { ArrowLeftIcon, ArrowRightIcon } from "~/components/icons";
import { Container, IndexSliderItem, Above } from "~/components";
import { colors, down, grid, header } from "~/config/var";
import { inject, observer } from "mobx-react";

import PT from "prop-types";

const SETTINGS = {
    dots: true,
    fade: true,
    infinite: true,
    speed: 0,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: false,
    // autoplay: true,
    autoplaySpeed: 4000,
    appendDots: dots => <SDots className="slick_dots">{dots}</SDots>,
    customPaging: i => <div className={i} />
};

@inject("slides_store")
@observer
class IndexSlider extends PureComponent {
    static propTypes = {
        slides_store: PT.object.isRequired
    };

    state = {
        index: 0
    };

    next = () => {
        this.slider.slickNext();
    };

    previous = () => {
        this.slider.slickPrev();
    };

    componentDidMount() {
        if (isBrowser) {
            setTimeout(() => {
                const _video = document.getElementById("index_slide_0");
                _video.play();
            }, 2000);
        }
    }

    render() {
        const {
            slides_store: { objects }
        } = this.props;
        const { index } = this.state;
        return (
            <SIndexSlider className="index_slider">
                <Slider
                    ref={c => (this.slider = c)}
                    {...SETTINGS}
                    afterChange={c =>
                        this.setState({
                            index: c
                        })
                    }
                >
                    {objects.map(slide => (
                        <IndexSliderItem
                            key={slide.id}
                            id={index}
                            slide={slide}
                        />
                    ))}
                </Slider>
                <Above from={grid.lg}>
                    <SArrows>
                        <Container>
                            <SArrowsBlock>
                                <SArrowLeft onClick={() => this.previous()}>
                                    <ArrowLeftIcon />
                                </SArrowLeft>
                                <SArrowRight onClick={() => this.next()}>
                                    <ArrowRightIcon />
                                </SArrowRight>
                            </SArrowsBlock>
                        </Container>
                    </SArrows>
                </Above>
            </SIndexSlider>
        );
    }
}

const SIndexSlider = styled.div`
    position: relative;
    ${down("md")} {
        margin-top: ${header.height_md};
    }
    ${down("sm")} {
        margin-top: ${header.height_sm};
    }
`;

const SDots = styled.ul`
    ${position("absolute", null, null, "2em", "0")};
    width: 100%;
    display: flex;
    justify-content: center;
    ${down("sm")} {
        bottom: auto;
        top: 1.4em;
        justify-content: flex-end;
        padding-right: 15px;
    }
    li {
        margin-right: 0.5em;
        &.slick-active {
            pointer-events: none;
            div {
                width: 26px;
                background-color: ${colors.white};
                ${down("sm")} {
                    width: 20px;
                }
            }
        }
        &:last-of-type {
            margin-right: 0;
        }
        div {
            height: 12px;
            width: 12px;
            display: inline-block;
            background-color: rgba(255, 255, 255, 0.38);
            border-radius: 6px;
            cursor: pointer;
            transition: all 300ms ease;
            ${down("sm")} {
                height: 10px;
                width: 10px;
            }
        }
    }
`;

const SArrows = styled.div`
    ${position("absolute", "50%", null, null, null)};
    transform: translateY(-50%);
    width: 100%;
    pointer-events: none;
`;

const SArrowsBlock = styled.div`
    position: relative;
`;

const SArrowStyle = css`
    position: absolute;
    color: ${colors.white};
    cursor: pointer;
    transition: all 0.2s;
    pointer-events: all;
    svg {
        width: 1.4em;
        height: 1.4em;
    }
`;
const SArrowLeft = styled.div`
    ${SArrowStyle};
    left: 0;
    opacity: 0.6;
    &:hover {
        opacity: 1;
    }
    margin-right: 0.5em;
    svg {
    }
`;
const SArrowRight = styled.div`
    ${SArrowStyle};
    right: 0;
    opacity: 0.6;
    &:hover {
        opacity: 1;
    }
    svg {
    }
`;

export default IndexSlider;
