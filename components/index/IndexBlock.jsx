import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { darken } from "polished";

import { Title, Button, Container, More } from "~/components";
import { Link } from "~/routes";
import { down, colors, global } from "~/config/var";

class IndexBlock extends PureComponent {
    static defaultProps = {
        title_level: 2,
        bg_color: colors.white
    };

    static propTypes = {
        bg_color: PT.string,
        title_level: PT.number,
        title: PT.string,
        text: PT.string,
        //
        button_route: PT.string,
        button_text: PT.string,
        //
        show_padding_bottom: PT.bool,
        children_right: PT.any,
        children: PT.any
    };

    executeOnClick(isExpanded) {
        console.log(isExpanded);
    }

    render() {
        const {
            bg_color,
            title_level,
            title,
            text,
            button_route = "",
            button_text,
            children_right,
            show_padding_bottom,
            children
        } = this.props;
        return (
            <SIndexBlock
                style={{ backgroundColor: `${bg_color}` }}
                className={CN({
                    show_padding_bottom: show_padding_bottom,
                    children_right: children_right
                })}
            >
                <Container>
                    <STop>
                        <SLeft>
                            <STitle
                                className={CN({
                                    title_margin_bottom: text && button_text
                                })}
                            >
                                <STitleText>
                                    <Title level={title_level}>{title}</Title>
                                </STitleText>
                                {button_route && (
                                    <SButton>
                                        <Link route={button_route}>
                                            <a>
                                                <Button color="yellow">
                                                    {button_text}
                                                </Button>
                                            </a>
                                        </Link>
                                    </SButton>
                                )}
                            </STitle>
                            {text ? (
                                <SText>
                                    <More>{text}</More>
                                </SText>
                            ) : null}
                        </SLeft>
                        {children_right && <SRight>{children_right}</SRight>}
                    </STop>
                </Container>
                <SBlock>{children}</SBlock>
            </SIndexBlock>
        );
    }
}

const SIndexBlock = styled.div`
    padding: ${global.padding} 0;
    ${down("md")} {
        padding: ${global.padding_large} 0;
    }
    ${down("sm")} {
        padding: ${global.padding_small} 0;
    }
    &.show_padding_bottom {
        padding-bottom: 0;
    }
`;
const STop = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 2em;
    ${down("sm")} {
        display: block;
        margin-bottom: 1.2em;
    }
`;
const SText = styled.div`
    ${down("sm")} {
        margin-top: 0.4em;
    }
`;
const SLeft = styled.div`
    &.children_right {
        width: 50%;
    }
    ${down("sm")} {
        width: 100%;
    }
`;
const STitle = styled.div`
    display: flex;
    align-items: center;
    line-height: 1.5;
    h1,
    h2 {
        line-height: 1.5;
        margin: 0;
        ${down("sm")} {
            line-height: 1.2;
        }
        @media (max-width: 330px) {
            font-size: 1em;
        }
    }
    &.title_margin_bottom {
        margin-bottom: 0.8em;
    }
`;
const STitleText = styled.div`
    font-size: 1.2em;
`;
const SButton = styled.div`
    padding-left: 2em;
    ${down("sm")} {
        padding-left: 1em;
    }
    @media (max-width: 330px) {
        font-size: 0.9em;
    }
`;
const SRight = styled.div`
    margin-left: auto;
    ${down("sm")} {
        width: 100%;
        margin: 1em 0;
        padding-bottom: 1em;
        border-bottom: 1px solid ${darken(0.05, "#FBFBF6")};
    }
`;
const SBlock = styled.div``;

export default IndexBlock;
