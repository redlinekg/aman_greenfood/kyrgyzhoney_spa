import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { Container, DateFormat, ImageBg, Title } from "~/components";
import { down, colors } from "~/config/var";

export default class ArticleHeader extends Component {
    static propTypes = {
        name: PT.string,
        created_at: PT.string,
        category: PT.string,
        image: PT.string,
        avatar: PT.string,
        title: PT.string
    };

    render() {
        const { name, created_at, category, image, avatar, title } = this.props;
        return (
            <Container>
                <SArticleHeader>
                    <Title level={1}>{title}</Title>
                    <STop>
                        <SAvatarBlock>
                            {avatar && (
                                <ImageBg
                                    src={avatar}
                                    alt={name}
                                    width={60}
                                    height={60}
                                    object_fit="contain"
                                    sizes={{
                                        lg: {
                                            width: 250,
                                            height: 250
                                        }
                                    }}
                                />
                            )}
                        </SAvatarBlock>
                        <STopInfo>
                            {name ? (
                                <SInfoName className="ArtcileAuthorName">
                                    {name}
                                </SInfoName>
                            ) : (
                                <SInfoNameLoading>
                                    <div />
                                </SInfoNameLoading>
                            )}
                            <SInfoBottom>
                                <SInfoDate>
                                    {created_at ? (
                                        <DateFormat date={created_at} />
                                    ) : (
                                        <SInfoDateLoading>
                                            <div />
                                        </SInfoDateLoading>
                                    )}
                                </SInfoDate>

                                {category ? (
                                    <SInfoCategory>{category}</SInfoCategory>
                                ) : (
                                    <SInfoCategoryLoading>
                                        <div />
                                    </SInfoCategoryLoading>
                                )}
                            </SInfoBottom>
                        </STopInfo>
                    </STop>
                    <SImageBlock>
                        {image && (
                            <ImageBg
                                src={image}
                                alt={title}
                                width={720}
                                height={720}
                                object_fit="contain"
                                sizes={{
                                    lg: {
                                        width: 250,
                                        height: 250
                                    }
                                }}
                            />
                        )}
                    </SImageBlock>
                </SArticleHeader>
            </Container>
        );
    }
}

const SArticleHeader = styled.div`
    max-width: 900px;
    margin: 0 auto;
`;
const STop = styled.div`
    display: flex;
    align-items: center;
    margin: 1em 0;
    ${down("sm")} {
        margin: 1em 0 0.5em;
    }
`;
const SAvatarBlock = styled.div`
    background-color: ${colors.placeholder};
    position: relative;
    width: 3em;
    height: 3em;
    overflow: hidden;
    border-radius: 50%;
    ${down("sm")} {
        width: 2.2em;
        height: 2.2em;
    }
`;

const STopInfo = styled.div`
    padding-left: 1em;
    ${down("sm")} {
        padding-left: 0.5em;
    }
`;
const SInfoName = styled.div`
    font-weight: 700;
    margin-bottom: 0.1em;
`;
const SInfoNameLoading = styled.div`
    height: 22px;
    margin-bottom: 0.1em;
    div {
        background-color: ${colors.placeholder};
        width: 10em;
        height: 17px;
    }
`;

const SInfoBottom = styled.div`
    display: flex;
    color: ${colors.grey};
    font-size: 0.9em;
`;
const SInfoDate = styled.div``;
const SInfoDateLoading = styled.div`
    height: 22px;
    div {
        background-color: ${colors.placeholder};
        width: 7em;
        height: 17px;
    }
`;
const SInfoCategory = styled.div`
    color: ${colors.red};
    ${down("sm")} {
        display: none;
    }
`;
const SInfoCategoryLoading = styled.div`
    height: 22px;
    div {
        background-color: ${colors.placeholder};
        width: 5em;
        height: 17px;
        ${down("sm")} {
            width: 2.6em;
        }
    }
`;
/////////////////
/////////////////
/////////////////
const SImageBlock = styled.div`
    background-color: ${colors.placeholder};
    position: relative;
    padding-top: 60%;
    overflow: hidden;
    border-radius: ${global.border_radius};
    margin-bottom: 1em;
    ${down("sm")} {
    }
`;
