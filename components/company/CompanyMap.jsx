import React, { PureComponent } from "react";
import styled from "@emotion/styled";

import map from "~/static/images/company/map.svg";

class CompanyMap extends PureComponent {
    render() {
        return (
            // <ContentContainer>
            <SCompanyMap>
                <SImg src={map} alt="Geography" />
            </SCompanyMap>
            // </ContentContainer>
        );
    }
}

const SCompanyMap = styled.div`
    padding: var(--editor-block-padding);
`;
const SImg = styled.img`
    width: 100%;
`;

export default CompanyMap;
