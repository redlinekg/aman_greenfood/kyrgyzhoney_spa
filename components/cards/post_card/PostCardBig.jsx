import React, { PureComponent } from "react";
import PT from "prop-types";
import { lighten, position } from "polished";
import styled from "@emotion/styled";

import { DateFormat, ImageBg } from "~/components";
import { Link } from "~/routes";
import { colors, down } from "~/config/var";

class PostCard extends PureComponent {
    static propTypes = {
        publication: PT.object.isRequired
    };

    render() {
        const {
            publication: { title, created_at, description, image, slug = "#" }
        } = this.props;
        return (
            <SPostCard>
                <SImg>
                    <Link route="article" params={{ slug }}>
                        <a>
                            <ImageBg
                                src={image.url_path}
                                alt={title}
                                width={560}
                                height={560}
                                sizes={{
                                    lg: {
                                        width: 560,
                                        height: 560
                                    },
                                    md: {
                                        width: 416,
                                        height: 416
                                    },
                                    sm: {
                                        width: 500,
                                        height: 500
                                    }
                                }}
                            />
                        </a>
                    </Link>
                </SImg>
                <SInfo>
                    <STitle>
                        <Link route="article" params={{ slug }}>
                            <a>{title}</a>
                        </Link>
                    </STitle>
                    <SDesc>{description}</SDesc>
                    <SDate>
                        <DateFormat date={created_at} />
                    </SDate>
                </SInfo>
            </SPostCard>
        );
    }
}

const SPostCard = styled.div`
    display: flex;
    align-items: center;
    ${down("sm")} {
    }
`;

const SImg = styled.div`
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 46.25%;
    display: block;
    width: 30%;
    img {
        ${position("absolute", "0", null, null, "0")}
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: center center;
        /* margin-left: 1.4em; */
        transform: scale(1.3);
        transition: 0.4s ease-in-out;
    }
    @media (hover) {
        &:hover {
            img {
                transform: scale(1.2);
            }
        }
    }
`;

const SInfo = styled.div`
    width: 70%;
    padding-left: 2em;
    ${down("sm")} {
        padding-left: 1em;
    }
`;

const STitle = styled.div`
    a {
        color: ${colors.black};
        font-size: 1.3em;
        font-weight: 600;
        display: block;
        margin-bottom: 0.5em;
        ${down("sm")} {
            font-size: 0.95em;
            line-height: 1.4;
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
`;
const SDesc = styled.div`
    margin-bottom: 0.8em;
    ${down("sm")} {
        display: none;
    }
`;
const SDate = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 0.8em;
    color: ${lighten(0.3, colors.grey)};
    font-size: 0.9em;
    ${down("sm")} {
    }
`;

export default PostCard;
