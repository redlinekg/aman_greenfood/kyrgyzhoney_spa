import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import { lighten, position } from "polished";
import styled from "@emotion/styled";

import { DateFormat, ImageBg } from "~/components";
import { Link } from "~/routes";
import { colors, down } from "~/config/var";

class PostCard extends PureComponent {
    static propTypes = {
        publication: PT.object.isRequired,
        mini: PT.bool
    };

    render() {
        const {
            publication: { title, created_at, image, slug = "#" },
            mini
        } = this.props;
        return (
            <SPostCard
                className={CN({
                    mini: mini
                })}
            >
                <Link route="article" params={{ slug }}>
                    <a>
                        <SImg>
                            <ImageBg
                                src={image.url_path}
                                alt={title}
                                width={560}
                                height={560}
                                sizes={{
                                    lg: {
                                        width: 560,
                                        height: 560
                                    },
                                    md: {
                                        width: 416,
                                        height: 416
                                    },
                                    sm: {
                                        width: 500,
                                        height: 500
                                    }
                                }}
                            />
                        </SImg>
                    </a>
                </Link>
                <SInfo>
                    <SDate>
                        <DateFormat date={created_at} />
                    </SDate>
                    <STitle>
                        <Link route="article" params={{ slug }}>
                            <a>{title}</a>
                        </Link>
                    </STitle>
                </SInfo>
            </SPostCard>
        );
    }
}

const SPostCard = styled.div`
    padding: 0 20px;
    margin-bottom: 1em;
    ${down("sm")} {
        margin-bottom: 1em;
        padding: 0 10px;
    }
    &.mini {
        padding: 0;
        margin-bottom: 2em;
        &:last-of-type {
            margin-bottom: 0;
        }
    }
    &:not(.mini) {
        &:nth-last-of-type(1),
        &:nth-last-of-type(2) {
            margin-bottom: 0;
        }
    }
`;
const SImg = styled.div`
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 46.25%;
    display: block;
    img {
        ${position("absolute", "0", null, null, "0")}
        width: 100%;
        height: 100%;
        background-size: cover;
        background-position: center center;
        /* margin-left: 1.4em; */
        transform: scale(1.3);
        transition: 0.4s ease-in-out;
    }
    @media (hover) {
        &:hover {
            img {
                transform: scale(1.2);
            }
        }
    }
`;

const SInfo = styled.div`
    margin-top: 1em;
    ${down("sm")} {
        margin-top: 0.5em;
    }
`;
const SDate = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 0.5em;
    color: ${lighten(0.3, colors.grey)};
    font-size: 0.9em;
    ${down("sm")} {
    }
`;
const STitle = styled.div`
    a {
        color: ${colors.black};
        font-size: 1.1em;
        font-weight: 600;
        display: block;
        margin-bottom: 0.5em;
        ${down("sm")} {
            font-size: 0.95em;
            line-height: 1.4;
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
`;

export default PostCard;
