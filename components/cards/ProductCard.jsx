import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import { position } from "polished";
import styled from "@emotion/styled";

import { ImageBg } from "../index";
import { Link } from "~/routes";
import { colors, down } from "~/config/var";

class ProductCard extends PureComponent {
    static propTypes = {
        card: PT.object.isRequired,
        small: PT.bool
    };

    render() {
        const {
            card: { title, volume, description, image, slug = "#" }
        } = this.props;
        const { small } = this.props;
        return (
            <SProductCard
                className={CN({
                    small: small
                })}
            >
                <SImageBlock>
                    <Link route="product" params={{ slug }}>
                        <a>
                            <SImg>
                                <ImageBg
                                    src={image ? image.url_path : ""}
                                    alt={title}
                                    width={320}
                                    height={320}
                                    object_fit="contain"
                                    sizes={{
                                        lg: {
                                            width: 200,
                                            height: 200
                                        }
                                    }}
                                />
                            </SImg>
                        </a>
                    </Link>
                </SImageBlock>
                <SInfo>
                    <STop>
                        <STitle>
                            <Link route="product" params={{ slug }}>
                                <a>{title}</a>
                            </Link>
                        </STitle>
                        <SVolume>{volume}</SVolume>
                    </STop>
                    {small ? null : <SDesc>{description}</SDesc>}
                </SInfo>
            </SProductCard>
        );
    }
}

const SProductCard = styled.div`
    margin-bottom: 2em;
    ${down("sm")} {
    }
`;
const SImageBlock = styled.div`
    background-color: ${colors.blue_light3};
    position: relative;
    overflow: hidden;
    height: 0;
    padding-top: 100%;
    display: block;
`;
const SImg = styled.div`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    width: 65%;
    height: 80%;
    img {
        object-position: bottom;
    }
`;
const SInfo = styled.div`
    margin-top: 1em;
    ${down("sm")} {
        margin-top: 0.5em;
    }
`;
const STop = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    ${SProductCard}.small & {
        display: block;
    }
    ${down("sm")} {
        display: block;
    }
`;
const STitle = styled.div`
    width: 100%;
    a {
        color: ${colors.black};
        font-weight: 500;
        display: block;
        margin-bottom: 0.5em;
        ${down("sm")} {
            font-size: 0.9em;
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
`;
const SVolume = styled.div`
    font-weight: 600;
    color: ${colors.grey};
    font-size: 0.9em;
    ${down("sm")} {
        font-size: 0.8em;
    }
`;
const SDesc = styled.div`
    font-size: 0.9em;
    ${down("sm")} {
        display: none;
    }
`;

export default ProductCard;
