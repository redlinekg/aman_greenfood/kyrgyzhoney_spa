import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";

import { Title, PlainGrid, ProductCard } from "~/components";
import { colors, down } from "~/config/var";

class Category extends PureComponent {
    static propTypes = {
        category: PropTypes.object
    };

    render() {
        const {
            category: { title, slug, description, products }
        } = this.props;
        return (
            <SCategory>
                <SAnchor id={`${slug}`} />
                <STop>
                    <Title level={2}>{title}</Title>
                    <SDesc>{description}</SDesc>
                </STop>
                <PlainGrid xga={4} xl={4} lg={3} md={3} sm={2}>
                    {products.map(product => (
                        <ProductCard key={product.id} card={product} />
                    ))}
                </PlainGrid>
            </SCategory>
        );
    }
}

const SCategory = styled.div`
    margin-bottom: 2em;
    ${down("sm")} {
        margin-bottom: 1.2em;
    }
`;

const SAnchor = styled.div`
    position: relative;
    height: 1px;
    width: 1px;
    top: -7em;
    ${down("md")} {
        top: -6.5em;
    }
    ${down("sm")} {
        top: -6em;
    }
`;

const STop = styled.div`
    margin-bottom: 2em;
    ${down("sm")} {
        margin-bottom: 1.2em;
    }
`;

const SDesc = styled.div`
    color: ${colors.grey};
    margin-top: 0.5em;
`;

export default Category;
