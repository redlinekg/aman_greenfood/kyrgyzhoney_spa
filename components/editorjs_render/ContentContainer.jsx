import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { Container } from "../";

class ContentContainer extends PureComponent {
    static propTypes = {
        children: PT.any,
        show: PT.bool
    };

    render() {
        const { children, show } = this.props;
        return (
            <>
                {show ? (
                    <Container>
                        <SContentContainer className="SContentContainer">
                            {children}
                        </SContentContainer>
                    </Container>
                ) : (
                    <SContentContainer className="SContentContainer">
                        {children}
                    </SContentContainer>
                )}
            </>
        );
    }
}

const SContentContainer = styled.div`
    max-width: 900px;
`;

export default ContentContainer;
