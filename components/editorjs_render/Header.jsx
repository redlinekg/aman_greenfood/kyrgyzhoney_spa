import React, { PureComponent } from "react";
import PT from "prop-types";
import { css } from "@emotion/core";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global } from "./config";
import { colors, down } from "~/config/var";

class Header extends PureComponent {
    static defaultProps = {
        level: 1
    };

    static propTypes = {
        level: PT.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
        children: PT.any,
        container_show: PT.bool
    };

    render() {
        const { level, children, container_show, ...rest } = this.props;
        return (
            <ContentContainer show={container_show}>
                {level === 1 && <SH1 {...rest}>{children}</SH1>}
                {level === 2 && <SH2 {...rest}>{children}</SH2>}
                {level === 3 && <SH3 {...rest}>{children}</SH3>}
                {level === 4 && <SH4 {...rest}>{children}</SH4>}
                {level === 5 && <SH5 {...rest}>{children}</SH5>}
                {level === 6 && <SH6 {...rest}>{children}</SH6>}
            </ContentContainer>
        );
    }
}

const SHeaderStyle = css`
    padding: ${global.header_padding};
    margin: ${global.header_margin};
    line-height: ${global.header_lineheight};
    outline: none;
    font-weight: 800;
`;
const SH1 = styled.h1`
    ${SHeaderStyle};
    margin: -0.2em 0 0.2em;
    padding: 0;
    font-size: 3em;
    ${down("lg")} {
        font-size: 2.4em;
    }
    ${down("lg")} {
        font-size: 2.4em;
    }
    ${down("md")} {
        font-size: 2em;
    }
    ${down("sm")} {
        font-size: 1.6em;
    }
`;
const SH2 = styled.h2`
    ${SHeaderStyle};
    font-size: 1.5em;
    color: ${colors.blue};
`;
const SH3 = styled.h3`
    ${SHeaderStyle};
    font-weight: 500;
    font-size: 1.1em;
`;
const SH4 = styled.h4`
    ${SHeaderStyle}
`;
const SH5 = styled.h5`
    ${SHeaderStyle};
`;
const SH6 = styled.h6`
    ${SHeaderStyle}
`;

export default Header;
