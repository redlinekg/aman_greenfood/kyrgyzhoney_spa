import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";

export default class ChecklistItem extends PureComponent {
    static defaultProps = {
        checked: false
    };

    static propTypes = {
        children: PT.any,
        checked: PT.bool
    };
    render() {
        const { children, checked, ...rest } = this.props;
        return (
            <SChecklistItem>
                <SCheck
                    className={CN({
                        active: checked
                    })}
                />
                <SText {...rest}>{children}</SText>
            </SChecklistItem>
        );
    }
}

const SChecklistItem = styled.div`
    display: flex;
    align-items: flex-start;
    padding: 0.3em 0;
`;
const SCheck = styled.div`
    flex: 0 0 1.4em;
    width: 1.4em;
    height: 1.4em;
    position: relative;
    border: var(--checklist-border-width) solid var(--editor-border-color);
    &:after {
        content: "";
        position: absolute;
        top: 0.3em;
        left: 0.12em;
        transform: rotate(-45deg);
        opacity: 0;
        width: 0.6em;
        height: 0.3em;
        border: var(--checklist-border-width) solid
            var(--editor-background-text-color);
        border-top: none;
        border-right: none;
        margin-left: 0.1em;
    }
    &.active {
        border-color: var(--editor-background-color);
        background-color: var(--editor-background-color);
        &:after {
            opacity: 1;
        }
    }
`;
const SText = styled.div`
    padding: 0.1em 0 0 0.6em;
`;
