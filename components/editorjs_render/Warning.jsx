import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global, text } from "./config";
import { down } from "~/config/var";

import warning from "~/static/images/editorjs/warning.png";

export default class Warning extends PureComponent {
    static propTypes = {
        title: PT.string,
        message: PT.string,
        container_show: PT.bool
    };
    render() {
        const { title, message, container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SWarning>
                    <SIcon src={warning} alt="warning" />
                    <STitle>{title}</STitle>
                    <SMessage>{message}</SMessage>
                </SWarning>
            </ContentContainer>
        );
    }
}

const SWarning = styled.div`
    padding: ${global.block_padding};
    display: flex;
    align-items: flex-start;
    ${down("sm")} {
        flex-wrap: wrap;
    }
`;
const SIcon = styled.img`
    width: 0.8em;
`;
const STitle = styled.div`
    font-weight: var(${text.bold});
    padding-left: 0.5em;
`;
const SMessage = styled.div`
    padding-left: 1em;
    ${down("sm")} {
        width: 100%;
        padding-left: 1.2em;
        margin-top: 0.4em;
    }
`;
