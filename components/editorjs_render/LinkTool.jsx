import React, { PureComponent } from "react";
import PT from "prop-types";
import { position } from "polished";
import styled from "@emotion/styled";
import IntersectionImage from "react-intersection-image";

import { ContentContainer } from "./";
import { global, border, text } from "./config";

export default class LinkTool extends PureComponent {
    static propTypes = {
        url: PT.string,
        domain: PT.string,
        title: PT.string,
        description: PT.string,
        image: PT.string,
        container_show: PT.bool
    };
    render() {
        const {
            url,
            domain,
            title,
            description,
            image,
            container_show
        } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SLinkTool>
                    <SBlock
                        href={url ? url : "#"}
                        className={url ? "" : "loading"}
                        target="_blank"
                    >
                        <SInfo>
                            <STitle>{title}</STitle>
                            {description && (
                                <SDescription>{description}</SDescription>
                            )}
                            <SAnchor>{domain}</SAnchor>
                        </SInfo>
                        {image && (
                            <SImgBlock>
                                <SImg src={image} alt={title} />
                            </SImgBlock>
                        )}
                    </SBlock>
                </SLinkTool>
            </ContentContainer>
        );
    }
}

const SLinkTool = styled.div`
    padding: ${global.block_padding};
`;
const SBlock = styled.a`
    display: flex;
    align-items: flex-start;
    padding: 0.6em 0.6em;
    transition: ${global.transition};
    border-style: solid;
    border-width: ${border.width};
    border-color: ${border.color};
    @media (hover) {
        &:hover {
            border-color: ${text.color_link_hover};
        }
    }
    &.loading {
        pointer-events: none;
    }
`;
const SInfo = styled.div`
    padding-right: 1em;
`;
const STitle = styled.div`
    font-weight: ${text.bold};
`;
const SDescription = styled.div`
    margin-bottom: 0.5em;
    color: ${text.color};
`;
const SAnchor = styled.div`
    margin-top: 1em;
    color: ${text.color_grey};
`;

const SImgBlock = styled.div`
    position: relative;
    overflow: hidden;
    display: block;
    background-color: ${text.color_grey_light2};
    width: 4em;
    height: 4em;
    flex: 0 0 4em;
`;

const SImg = styled(IntersectionImage)`
    ${position("absolute", "0", null, null, "0")};
    width: 100%;
    height: 100%;
    object-fit: cover;
    transition: ${global.transition};
`;
