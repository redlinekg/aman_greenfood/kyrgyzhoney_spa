import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global, text } from "./config";

export default class Paragraph extends PureComponent {
    static propTypes = {
        children: PT.any,
        container_show: PT.bool
    };
    render() {
        const { children, container_show, ...rest } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SParagraph {...rest}>{children}</SParagraph>
            </ContentContainer>
        );
    }
}

const SParagraph = styled.p`
    padding: ${global.block_padding};
    color: ${text.color};
    margin-bottom: 0;
    mark {
        background: rgba(245, 235, 111, 0.29);
        padding: 3px 0;
    }
`;
