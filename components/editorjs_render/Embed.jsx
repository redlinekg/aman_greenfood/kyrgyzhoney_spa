import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import { position } from "polished";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global } from "./config";

export default class Embed extends PureComponent {
    static defaultProps = {
        service: "default"
    };

    static propTypes = {
        embed: PT.string.isRequired,
        service: PT.oneOf(["default", "youtube"]).isRequired,
        caption: PT.string.isRequired,
        container_show: PT.bool
    };
    render() {
        const { embed, service, container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SEmbed>
                    <SEmbedBlock
                        className={CN({
                            youtube: service
                        })}
                    >
                        <iframe
                            src={embed}
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                        />
                    </SEmbedBlock>
                </SEmbed>
            </ContentContainer>
        );
    }
}

const SEmbed = styled.div`
    padding: ${global.block_padding};
`;
const SEmbedBlock = styled.div`
    position: relative;
    height: 0;
    width: 100%;
    &.youtube {
        padding-bottom: 56.25%; /* 16:9 */
    }
    iframe {
        ${position("absolute", "0", "0", "0", "0")};
        width: 100%;
        height: 100%;
    }
`;
