import React, { PureComponent } from "react";
import PT from "prop-types";
import humanSize from "human-size";
import styled from "@emotion/styled";

import {
    DocIcon,
    DownloadIcon,
    FileIcon,
    PdfIcon,
    XlsxIcon,
    ZipIcon
} from "./icons";
import { ContentContainer } from "./";
import { global, text, border } from "./config";

const DOCX = ".docx";
const PDF = ".pdf";
const XLSX = ".xlsx";
const ZIP = ".zip";

export default class File extends PureComponent {
    static defaultProps = {
        type: "default"
    };

    static propTypes = {
        type: PT.string.isRequired,
        href: PT.string,
        name: PT.string,
        size: PT.number,
        container_show: PT.bool
    };
    render() {
        const { type, href, name, size, container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SFile>
                    <SBlock
                        href={href ? href : "#"}
                        className={href ? "" : "loading"}
                        target="_blank"
                    >
                        <SIcon>
                            {type == DOCX ? (
                                <DocIcon />
                            ) : type == PDF ? (
                                <PdfIcon />
                            ) : type == XLSX ? (
                                <XlsxIcon />
                            ) : type == ZIP ? (
                                <ZipIcon />
                            ) : (
                                <FileIcon />
                            )}
                        </SIcon>
                        <SInfo>
                            <STitle>{name}</STitle>
                            <SSize>{humanSize(size)}</SSize>
                        </SInfo>
                        <SDownload>
                            <DownloadIcon />
                        </SDownload>
                    </SBlock>
                </SFile>
            </ContentContainer>
        );
    }
}

const SFile = styled.div`
    display: block;
    padding: ${global.block_padding};
`;
const SBlock = styled.a`
    display: flex;
    align-items: center;
    padding: 0.6em 0.6em;
    transition: all 0.2s;
    border-style: solid;
    border-width: ${border.width};
    border-color: ${border.color};
    @media (hover) {
        &:hover {
            border-color: ${border.color_hover};
        }
    }
    &.loading {
        pointer-events: none;
    }
`;
const SIcon = styled.div`
    svg {
        width: 1.9em;
    }
`;
const SInfo = styled.div`
    padding-left: 1em;
    width: 100%;
`;
const STitle = styled.div`
    font-weight: ${text.bold};
    transition: ${global.transition};
    color: ${text.color_link};
    @media (hover) {
        ${SBlock}:hover & {
            color: ${text.color_link_hover};
        }
    }
`;
const SSize = styled.div`
    color: ${text.color_grey};
    font-size: 0.9em;
`;

const SDownload = styled.div`
    margin-left: auto;
    color: ${text.color_grey_light};
    transition: all 0.2s;
    svg {
        width: 1.4em;
        height: 1.4em;
    }
    @media (hover) {
        ${SBlock}:hover & {
            color: ${text.color_link_hover};
        }
    }
`;
