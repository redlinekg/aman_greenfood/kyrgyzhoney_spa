import { colors } from "~/config/var";

export const global = {
    block_padding: "0.6em 0",
    //header
    header_padding: "1em 0",
    header_margin: "0 0 -1.2em",
    header_lineheight: "1.1em",
    //
    transition: "all 0.3s ease-out",
    lineheight: "1.4"
    //
};

export const text = {
    color: colors.black,
    color_grey: colors.grey,
    color_grey_light: colors.grey_light,
    color_grey_light2: colors.grey_light4,
    //font weight
    light: "200",
    normal: "300",
    medium: "400",
    bold: "700",
    color_link: colors.blue_light,
    color_link_hover: colors.blue
};

export const border = {
    width: "1px",
    color: colors.grey_light4,
    color_hover: colors.grey_light4
};
