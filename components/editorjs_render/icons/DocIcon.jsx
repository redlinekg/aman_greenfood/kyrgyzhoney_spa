import React, { Component } from "react";
import PT from "prop-types";

class DocIcon extends Component {
    static propTypes = {
        style: PT.object
    };
    render() {
        const { style } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 55.948 73.431"
                style={style}
            >
                <g transform="translate(-8 -3)">
                    <path
                        d="M63.948,76.431H8V3H46.464L63.948,20.484Z"
                        fill="#1976d2"
                    />
                    <path
                        d="M45.609,21.109H29V4.5Z"
                        transform="translate(15.716 1.123)"
                        fill="#fff"
                    />
                    <path
                        d="M34.7,36.231,37.267,22.2h5.348L37.868,44.577H32.275l-3-12.758L26.34,44.577H20.767L16,22.2h5.361l2.568,14.028L27,22.2h4.576Z"
                        transform="translate(5.987 14.371)"
                        fill="#fff"
                    />
                </g>
            </svg>
        );
    }
}

export default DocIcon;
