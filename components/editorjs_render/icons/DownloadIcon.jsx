import React, { Component } from "react";

class DownloadIcon extends Component {
    render() {
        return (
            <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M49.189 31.052l-4.378-4.104L35 37.414V2h-6v35.066l-9.751-11.05-4.498 3.969 17.182 19.473 17.256-18.406z"
                    fill="currentColor"
                />
                <path d="M52 45v11H12V45H6v17h52V45h-6z" fill="currentColor" />
            </svg>
        );
    }
}

export default DownloadIcon;
