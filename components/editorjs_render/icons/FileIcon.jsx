import React, { Component } from "react";
import PT from "prop-types";

class FileIcon extends Component {
    static propTypes = {
        style: PT.object
    };
    render() {
        const { style } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 55.948 73.431"
                style={style}
            >
                <g transform="translate(-8 -3)">
                    <path
                        d="M63.948,76.431H8V3H46.464L63.948,20.484Z"
                        fill="#dedede"
                    />
                    <path
                        d="M45.609,21.109H29V4.5Z"
                        transform="translate(15.716 1.123)"
                        fill="#fff"
                    />
                </g>
            </svg>
        );
    }
}

export default FileIcon;
