import React, { Component } from "react";
import PT from "prop-types";

class PdfIcon extends Component {
    static propTypes = {
        style: PT.object
    };
    render() {
        const { style } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 55.948 73.431"
                style={style}
            >
                <g transform="translate(-8 -3)">
                    <path
                        d="M63.948,76.431H8V3H46.464L63.948,20.484Z"
                        fill="#ff5722"
                    />
                    <path
                        d="M45.609,21.109H29V4.5Z"
                        transform="translate(15.716 1.123)"
                        fill="#fff"
                    />
                    <path
                        d="M17.311,34.329v6.119H13.8v-17.4h5.928a5.427,5.427,0,0,1,4.118,1.6,5.751,5.751,0,0,1,1.537,4.159,5.4,5.4,0,0,1-1.523,4.036,5.749,5.749,0,0,1-4.207,1.482Zm0-2.93h2.418a1.9,1.9,0,0,0,1.55-.656,2.889,2.889,0,0,0,.553-1.912,3.51,3.51,0,0,0-.56-2.076,1.807,1.807,0,0,0-1.509-.779H17.311Z"
                        transform="translate(4.341 15.002)"
                        fill="#fff"
                    />
                    <path
                        d="M21.766,40.449v-17.4h4.6a6.338,6.338,0,0,1,4.863,1.933,7.593,7.593,0,0,1,1.844,5.307v2.821a7.62,7.62,0,0,1-1.81,5.388,6.462,6.462,0,0,1-4.992,1.953Zm3.51-14.472V37.532h1.052A2.929,2.929,0,0,0,28.8,36.6a5.52,5.52,0,0,0,.758-3.2V30.389a6.213,6.213,0,0,0-.683-3.4,2.765,2.765,0,0,0-2.322-1.011Z"
                        transform="translate(10.302 15.002)"
                        fill="#fff"
                    />
                    <path
                        d="M38.653,33.346H33.189v7.1H29.672v-17.4h9.636v2.93H33.189v4.46h5.464Z"
                        transform="translate(16.218 15.002)"
                        fill="#fff"
                    />
                </g>
            </svg>
        );
    }
}

export default PdfIcon;
