import React, { Component } from "react";
import PT from "prop-types";

class ZipIcon extends Component {
    static propTypes = {
        style: PT.object
    };
    render() {
        const { style } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 55.948 73.431"
                style={style}
            >
                <g transform="translate(-8 -3)">
                    <path
                        d="M63.948,76.431H8V3H46.464L63.948,20.484Z"
                        fill="#f2f2f2"
                    />
                    <path
                        d="M45.609,21.109H29V4.5Z"
                        transform="translate(15.716 1.123)"
                        fill="#dedede"
                    />
                    <path
                        d="M28.006,29.64,31.667,22.2h6.16L31.571,33.3,38,44.577H31.769L28.006,37l-3.763,7.574H18.035L24.441,33.3,18.206,22.2h6.147Z"
                        transform="translate(7.51 14.371)"
                        fill="#388e3c"
                    />
                </g>
            </svg>
        );
    }
}

export default ZipIcon;
