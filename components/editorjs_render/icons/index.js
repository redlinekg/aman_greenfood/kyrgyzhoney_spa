//file
import DocIcon from "./DocIcon";
import FileIcon from "./FileIcon";
import PdfIcon from "./PdfIcon";
import XlsxIcon from "./XlsxIcon";
import ZipIcon from "./ZipIcon";
//
import DownloadIcon from "./DownloadIcon";
import QuoteIcon from "./QuoteIcon";
import WarningIcon from "./WarningIcon";

export {
    //file
    DocIcon,
    FileIcon,
    PdfIcon,
    XlsxIcon,
    ZipIcon,
    //
    DownloadIcon,
    QuoteIcon,
    WarningIcon
};
