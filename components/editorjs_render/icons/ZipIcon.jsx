import React, { Component } from "react";
import PT from "prop-types";

class CheckIcon extends Component {
    static propTypes = {
        style: PT.object
    };
    render() {
        const { style } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 55.948 73.431"
                style={style}
            >
                <g transform="translate(-8 -3)">
                    <path
                        d="M63.948,76.431H8V3H46.464L63.948,20.484Z"
                        fill="#ff9100"
                    />
                    <path
                        d="M45.609,21.109H29V4.5Z"
                        transform="translate(15.716 1.123)"
                        fill="#fff"
                    />
                    <path
                        d="M19.186,37.532h7.171v2.916H15.075V38.331l7.1-12.355H15v-2.93H26.221v2.069Z"
                        transform="translate(5.239 15.002)"
                        fill="#fff"
                    />
                    <path
                        d="M26.331,40.449H22.82v-17.4h3.51Z"
                        transform="translate(11.091 15.002)"
                        fill="#fff"
                    />
                    <path
                        d="M30.03,34.329v6.119H26.52v-17.4h5.928a5.4,5.4,0,0,1,4.111,1.6A5.75,5.75,0,0,1,38.1,28.811a5.391,5.391,0,0,1-1.516,4.036,5.748,5.748,0,0,1-4.207,1.482Zm0-2.93h2.418A1.9,1.9,0,0,0,34,30.744a2.89,2.89,0,0,0,.553-1.912,3.463,3.463,0,0,0-.56-2.076,1.807,1.807,0,0,0-1.509-.779H30.037V31.4Z"
                        transform="translate(13.859 15.002)"
                        fill="#fff"
                    />
                </g>
            </svg>
        );
    }
}

export default CheckIcon;
