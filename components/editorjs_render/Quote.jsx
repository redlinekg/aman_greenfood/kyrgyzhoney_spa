import React, { PureComponent } from "react";
import PT from "prop-types";
import { position } from "polished";
import styled from "@emotion/styled";
import { QuoteIcon } from "./icons";

import { ContentContainer } from "./";
import { global, border, text } from "./config";
import { colors, down } from "~/config/var";

export default class Quote extends PureComponent {
    static propTypes = {
        text: PT.any,
        caption: PT.string,
        alignment: PT.string,
        text_html: PT.string,
        caption_html: PT.string,
        container_show: PT.bool
    };
    render() {
        const {
            text_html,
            caption_html,
            alignment,
            container_show
        } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SQuote>
                    <SQuoteBlock className={alignment}>
                        <SQuoteText
                            dangerouslySetInnerHTML={{ __html: text_html }}
                        />
                        {caption_html && (
                            <SQuoteCaption>
                                <QuoteIcon />
                                <span
                                    dangerouslySetInnerHTML={{
                                        __html: caption_html
                                    }}
                                />
                            </SQuoteCaption>
                        )}
                    </SQuoteBlock>
                </SQuote>
            </ContentContainer>
        );
    }
}

const SQuote = styled.div`
    padding: ${global.block_padding};
`;
const SQuoteBlock = styled.div`
    padding-left: 2.2em;
    position: relative;
    ${down("sm")} {
        padding-left: 1.2em;
    }
    &:before {
        content: "";
        ${position("absolute", "0", null, null, "0")};
        height: 100%;
        width: 4px;
        background-color: ${border.color};
    }
`;
const SQuoteText = styled.div`
    font-size: 1.1em;
    font-weight: ${text.bold};
    color: ${colors.blue};
    ${down("sm")} {
        font-size: 1em;
    }
    ${SQuoteBlock}.center & {
        text-align: center;
    }
`;
const SQuoteCaption = styled.div`
    display: flex;
    align-items: center;
    margin-top: 0.8em;
    ${SQuoteBlock}.center & {
        text-align: center;
        justify-content: center;
    }
    svg {
        width: 1.4em;
        height: 1.4em;
        color: ${text.color_grey_light2};
    }
    span {
        padding-left: 0.5em;
        font-weight: 500;
    }
`;
