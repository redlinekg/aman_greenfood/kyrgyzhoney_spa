import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { HoneycombIcon } from "../icons";
import { text } from "./config";
import { colors } from "~/config/var";

export default class ListItem extends PureComponent {
    static propTypes = {
        type: PT.oneOf(["unordered", "ordered"]).isRequired,
        children: PT.any
    };
    render() {
        const { children, type, ...rest } = this.props;
        return (
            <SListItem className={`${type}`}>
                {type === "unordered" ? <HoneycombIcon /> : null}
                <span {...rest}>{children}</span>
            </SListItem>
        );
    }
}

const SListItem = styled.li`
    padding: 0.3em 0;
    span {
        color: ${text.color};
    }
    &.unordered {
        display: flex;
        svg {
            width: 0.8em;
            height: 0.8em;
            flex: 0 0 0.8em;
            color: ${colors.yellow_light};
            margin-right: 0.5em;
            margin-top: 0.2em;
        }
    }
    &.ordered {
        list-style: decimal;
    }
`;
