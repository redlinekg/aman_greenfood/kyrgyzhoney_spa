import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { colors, down } from "~/config/var";

export default class Delimiter extends PureComponent {
    static propTypes = {
        container_show: PT.bool
    };

    render() {
        const { container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SDelimiter />
            </ContentContainer>
        );
    }
}

const SDelimiter = styled.div`
    padding: 2em 0;
    width: 100%;
    text-align: center;
    ${down("sm")} {
        padding: 1.2em 0;
    }
    &:before {
        content: "";
        display: block;
        height: 2px;
        width: 100%;
        background-color: ${colors.grey_light5};
    }
`;
