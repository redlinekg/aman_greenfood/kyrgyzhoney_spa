import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global } from "./config";

export default class Checklist extends PureComponent {
    static propTypes = {
        children: PT.any.isRequired,
        container_show: PT.bool
    };

    render() {
        const { children, container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SChecklist>{children}</SChecklist>
            </ContentContainer>
        );
    }
}

const SChecklist = styled.div`
    padding: ${global.block_padding};
`;
