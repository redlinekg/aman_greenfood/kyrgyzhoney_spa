import React, { PureComponent } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global, border, text } from "./config";

class Text extends PureComponent {
    static defaultProps = {
        type: "default"
    };
    static propTypes = {
        withBorder: PT.bool,
        stretched: PT.bool,
        withBackground: PT.bool,
        src: PT.string,
        caption: PT.string,
        container_show: PT.bool
    };
    render() {
        const {
            withBorder,
            stretched,
            withBackground,
            src,
            caption,
            container_show,
            ...rest
        } = this.props;
        return (
            <>
                {stretched ? (
                    <SImage
                        className={CN({
                            border: withBorder,
                            stretched: stretched,
                            background: withBackground
                        })}
                    >
                        <SImageWrap>
                            <SImageBlock>
                                <SImg src={src} alt={caption} />
                            </SImageBlock>
                            <ContentContainer>
                                <SCaption>{caption}</SCaption>
                            </ContentContainer>
                        </SImageWrap>
                    </SImage>
                ) : (
                    <ContentContainer show={container_show}>
                        <SImage
                            className={CN({
                                border: withBorder,
                                background: withBackground
                            })}
                        >
                            <SImageWrap>
                                <SImageBlock>
                                    <SImg src={src} alt={caption} />
                                </SImageBlock>
                                <SCaption {...rest}>{caption}</SCaption>
                            </SImageWrap>
                        </SImage>
                    </ContentContainer>
                )}
            </>
        );
    }
}

const SImage = styled.div`
    padding: ${global.block_padding};
`;
const SImageWrap = styled.figure``;
const SImageBlock = styled.div`
    ${SImage}.border & {
        img {
            border: ${border.width} solid ${border.color};
        }
    }
    ${SImage}.background & {
        background-color: ${text.color_grey_light2};
        padding: 15px;
    }
    ${SImage}.stretched & {
    }
`;
const SImg = styled.img`
    width: 100%;
    display: block;
    ${SImage}.background & {
        max-width: 60%;
        margin: 0 auto;
    }
`;
const SCaption = styled.figcaption`
    padding: 0.5em 0 0 0;
    text-align: center;
    color: ${text.color_grey};
    font-size: 0.9em;
`;

export default Text;
