import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import {
    Checklist,
    ChecklistItem,
    Delimiter,
    Embed,
    File,
    Header,
    Image,
    LinkTool,
    List,
    ListItem,
    Paragraph,
    Quote,
    Warning
} from "./";
import { CompanyMap } from "../";
import { global } from "./config";

export default class EditorJs extends PureComponent {
    static defaultProps = {
        container_show: true
    };

    static propTypes = {
        blocks: PT.array,
        blocks_json: PT.string,
        container_show: PT.bool
    };
    render() {
        let { blocks, blocks_json, container_show } = this.props;
        if (blocks_json) {
            blocks = JSON.parse(blocks_json);
        }
        return (
            <SEditorJs>
                {blocks.map((block, idx) => {
                    switch (block.type) {
                        case "header":
                            return (
                                <Header
                                    key={idx}
                                    level={block.data.level}
                                    dangerouslySetInnerHTML={{
                                        __html: block.data.text
                                    }}
                                    container_show={container_show}
                                />
                            );
                        case "paragraph":
                            return (
                                <Paragraph
                                    key={idx}
                                    dangerouslySetInnerHTML={{
                                        __html: block.data.text
                                    }}
                                    container_show={container_show}
                                />
                            );
                        case "list":
                            return (
                                <List
                                    key={idx}
                                    type={block.data.style}
                                    container_show={container_show}
                                >
                                    {block.data.items.map((i, idx) => (
                                        <ListItem
                                            type={block.data.style}
                                            key={idx}
                                            dangerouslySetInnerHTML={{
                                                __html: i
                                            }}
                                        />
                                    ))}
                                </List>
                            );
                        case "delimiter":
                            return (
                                <Delimiter
                                    key={idx}
                                    container_show={container_show}
                                />
                            );
                        case "link":
                            return (
                                <LinkTool
                                    key={idx}
                                    url={block.data.link}
                                    domain={block.data.meta.domain}
                                    title={block.data.meta.title}
                                    description={block.data.meta.description}
                                    image={block.data.meta.image.url}
                                    container_show={container_show}
                                />
                            );
                        case "quote":
                            return (
                                <Quote
                                    key={idx}
                                    alignment={block.data.alignment}
                                    text_html={block.data.text}
                                    caption_html={block.data.caption}
                                    container_show={container_show}
                                />
                            );
                        case "image":
                            return (
                                <Image
                                    key={idx}
                                    withBorder={block.data.withBorder}
                                    stretched={block.data.stretched}
                                    withBackground={block.data.withBackground}
                                    src={block.data.file.url}
                                    dangerouslySetInnerHTML={{
                                        __html: block.data.caption
                                    }}
                                    container_show={container_show}
                                />
                            );
                        case "simpleImage":
                            return (
                                <Image
                                    key={idx}
                                    withBorder={block.data.withBorder}
                                    stretched={block.data.stretched}
                                    withBackground={block.data.withBackground}
                                    src={block.data.url}
                                    caption={block.data.caption}
                                    container_show={container_show}
                                />
                            );
                        case "embed":
                            return (
                                <Embed
                                    key={idx}
                                    embed={block.data.embed}
                                    service={block.data.service}
                                    caption={block.data.caption}
                                    container_show={container_show}
                                />
                            );
                        case "warning":
                            return (
                                <Warning
                                    key={idx}
                                    title={block.data.title}
                                    message={block.data.message}
                                    container_show={container_show}
                                />
                            );
                        case "checklist":
                            return (
                                <Checklist
                                    key={idx}
                                    container_show={container_show}
                                >
                                    {block.data.items.map((i, idx) => (
                                        <ChecklistItem
                                            key={idx}
                                            checked={i.checked}
                                            dangerouslySetInnerHTML={{
                                                __html: i.text
                                            }}
                                        />
                                    ))}
                                </Checklist>
                            );
                        case "attaches":
                            return (
                                <File
                                    key={idx}
                                    type={block.data.file.type}
                                    href={block.data.file.url}
                                    name={block.data.title}
                                    size={block.data.file.size}
                                    container_show={container_show}
                                />
                            );
                        case "component":
                            return block.data.component === "company_map" ? (
                                <CompanyMap key={idx} />
                            ) : null;
                        default:
                            return (
                                <Paragraph key={idx}>
                                    Неизвестный тип: {block.type}
                                </Paragraph>
                            );
                    }
                })}
                {/* <pre>
                    {JSON.stringify(JSON.parse(blocks_json), undefined, "   ")}
                </pre> */}
            </SEditorJs>
        );
    }
}

const SEditorJs = styled.div`
    line-height: ${global.lineheight};
    b {
        font-weight: 600;
    }
`;
