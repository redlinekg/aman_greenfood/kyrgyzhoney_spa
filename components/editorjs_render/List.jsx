import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";

import { ContentContainer } from "./";
import { global } from "./config";
import { down } from "~/config/var";

export default class List extends PureComponent {
    static defaultProps = {
        type: "unordered"
    };

    static propTypes = {
        type: PT.oneOf(["unordered", "ordered"]).isRequired,
        children: PT.any.isRequired,
        container_show: PT.bool
    };
    render() {
        const { type, children, container_show } = this.props;
        return (
            <ContentContainer show={container_show}>
                <SList className={`${type}`}>{children}</SList>
            </ContentContainer>
        );
    }
}

const SList = styled.ul`
    padding: ${global.block_padding};
    &.unordered {
        padding-left: 2em;
        list-style: disc;
        ${down("sm")} {
            padding-left: 1em;
        }
    }
    &.ordered {
        padding-left: 3em;
        list-style: decimal;
        ${down("sm")} {
            padding-left: 1em;
        }
    }
`;
