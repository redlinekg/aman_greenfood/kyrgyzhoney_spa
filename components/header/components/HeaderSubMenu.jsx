import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";
import scrollTo from "ssr-scroll-to";
import { isBrowser } from "browser-or-node";

import { HeaderPanelBackground } from "../";
import { HoneycombIcon } from "~/components/icons";
import { Container, HorizontalScroll } from "~/components";
import { Link } from "~/routes";
import { down, colors, header } from "~/config/var";
import { calculateScrollOffset } from "~/utils";

class Item extends PureComponent {
    static propTypes = {
        item: PT.object
    };

    render() {
        const {
            item: { route = "", params = {}, title }
        } = this.props;
        return (
            <SItem>
                <Link route={route} params={params}>
                    <a>
                        <span>{title}</span>
                        <HoneycombIcon />
                    </a>
                </Link>
            </SItem>
        );
    }
}

class ItemScroll extends PureComponent {
    static propTypes = {
        item: PT.object
    };

    componentDidMount() {
        const _link = document.getElementsByClassName("link_go_to")[0];
        _link.classList.add("active");
    }

    goToBlock = slug => {
        if (isBrowser) {
            [].forEach.call(
                document.getElementsByClassName("link_go_to"),
                function(el) {
                    el.classList.remove("active");
                }
            );
            const elem = document.getElementById(`${slug}`);
            const position = calculateScrollOffset(elem, 0);
            scrollTo(0, position, {
                ease: "in-out-sine",
                duration: 500
            });
            const _link = document.getElementById(`link-${slug}`);
            _link.classList.add("active");
        }
    };

    render() {
        const {
            item: { params, title }
        } = this.props;
        return (
            <SItem>
                <a
                    id={`link-${params.slug}`}
                    className="link_go_to"
                    href={`#${params.slug}`}
                    onClick={e => {
                        e.preventDefault();
                        this.goToBlock(params.slug);
                    }}
                >
                    <span>{title}</span>
                    <HoneycombIcon />
                </a>
            </SItem>
        );
    }
}

const SItem = styled.div`
    margin-right: 2em;
    ${down("sm")} {
        margin-right: 1.5em;
        &:first-of-type {
            padding-left: 15px;
        }
    }
    a {
        position: relative;
        color: ${colors.black};
        display: block;
        height: ${header.sub};
        display: flex;
        align-items: center;
        font-size: 0.9em;
        white-space: nowrap;
        svg {
            ${position("absolute", null, null, "-0.4em", "50%")};
            transform: translateX(-50%);
            width: 1em;
            height: 1em;
            color: ${colors.yellow_light};
            opacity: 0;
            transition: all 0.1s ease-in-out;
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
        &.active {
            color: ${colors.blue};
            cursor: default;
            svg {
                opacity: 1;
            }
        }
    }
`;

class HeaderSubMenu extends PureComponent {
    static propTypes = {
        dark: PT.bool,
        go_to: PT.bool,
        sub_menu: PT.array,
        children: PT.any
    };

    render() {
        const { dark, go_to, sub_menu, children } = this.props;
        return (
            <SHeaderSubMenu
                className={CN({
                    dark: dark,
                    sub_menu: sub_menu
                })}
            >
                <HeaderPanelBackground dark={dark} />
                <Container>
                    <SBlock>
                        <HorizontalScroll>
                            <SList>
                                {go_to ? (
                                    <>
                                        {sub_menu.map((item, idx) => (
                                            <ItemScroll key={idx} item={item} />
                                        ))}
                                    </>
                                ) : (
                                    <>
                                        {sub_menu.map((item, idx) => (
                                            <Item key={idx} item={item} />
                                        ))}
                                    </>
                                )}
                                {children}
                            </SList>
                        </HorizontalScroll>
                    </SBlock>
                </Container>
            </SHeaderSubMenu>
        );
    }
}

const SHeaderSubMenu = styled.div`
    ${position("fixed", "5.98em", null, null, "0")};
    width: 100%;
    padding: 0 40px;
    display: flex;
    align-items: center;
    transition: top 0.3s ease-out;
    z-index: 12;
    margin-top: -2px;
    ${down("md")} {
        padding: 0;
        top: ${header.height_md};
    }
    ${down("sm")} {
        top: ${header.height_sm};
    }
    .header_fixed & {
        top: ${header.height};
        ${down("md")} {
            top: ${header.height_md};
        }
        ${down("sm")} {
            top: ${header.height_sm};
        }
    }
`;

const SList = styled.div`
    display: flex;
`;
const SBlock = styled.div`
    position: relative;
    width: 100%;
    padding: 0 20px;
    z-index: 3;
    ${down("sm")} {
        padding: 0;
    }
`;

export default HeaderSubMenu;
