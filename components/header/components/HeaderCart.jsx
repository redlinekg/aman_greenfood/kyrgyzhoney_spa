import React, { Component, createRef } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { keyframes } from "@emotion/core";
import { position } from "polished";
import CN from "classnames";
import { reaction } from "mobx";

import { CartIcon } from "~/components/icons";
import { OpenModal } from "~/components";
import { colors, down } from "~/config/var";
import { inject, observer } from "mobx-react";

@inject("cart_store")
@observer
class HeaderCart extends Component {
    static propTypes = {
        dark: PT.bool,
        cart_store: PT.object.isRequired
    };

    element = createRef();

    animateIcon = () => {
        if (this.element && this.element.current) {
            this.element.current.classList.add("animated");
            setTimeout(() => {
                this.element.current.classList.remove("animated");
            }, 1000);
        }
    };

    render() {
        const { dark, cart_store } = this.props;
        return (
            <SHeaderCart
                ref={this.element}
                className={CN({
                    dark: dark,
                    disabled: cart_store.count === 0
                })}
            >
                <OpenModal name="order">
                    <CartIcon />
                    {cart_store.count ? (
                        <SCartCount>
                            <SCartCountNumber>
                                {cart_store.count > 999
                                    ? "999"
                                    : cart_store.count}
                            </SCartCountNumber>
                        </SCartCount>
                    ) : null}
                </OpenModal>
            </SHeaderCart>
        );
    }
}

const BounceKeyframes = keyframes(`
    0%, 20%, 60%, 100% {
        transform: translateX(0) rotate(0deg);
    }

    40% {
        transform: translateX(-3px) rotate(10deg);
    }

    80% {
        transform: translateX(-3px) rotate(-10deg);
    }
`);

let SHeaderCart = styled.div`
    position: relative;
    z-index: 2;
    color: ${colors.black};
    cursor: pointer;
    transition: all 0.2s ease-in-out;
    margin-left: 2em;
    ${down("lg")} {
        margin-left: 1.2em;
    }
    ${down("md")} {
        margin-left: 1em;
    }
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
    svg {
        width: 1.3em;
        height: 1.3em;
    }
    &.dark {
        color: ${colors.white};
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
    &.disabled {
        color: ${colors.grey_light};
        pointer-events: none;
    }
`;

const SCartCount = styled.div`
    ${position("absolute", "-10px", "-10px", null, null)}
    background-color: ${colors.yellow_light};
    border-radius: 50%;
    border-radius: 8px;
    padding: 0.1em 0.2em;
    ${SHeaderCart}.animated & {
        animation: ${BounceKeyframes} 1s ease 1;
    }
`;

const SCartCountNumber = styled.div`
    font-weight: 500;
    color: ${colors.black};
    font-size: 0.7em;
`;

export default HeaderCart;
