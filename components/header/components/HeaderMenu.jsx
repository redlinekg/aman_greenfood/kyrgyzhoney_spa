import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";

import { Link } from "~/routes";
import { Above } from "~/components";
import { withTranslation } from "~/i18n";
import { colors, down, header, grid } from "~/config/var";

class HeaderMenu extends PureComponent {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        t: PT.any.isRequired,
        mobile: PT.string,
        dark: PT.bool,
        fixed: PT.bool
    };

    render() {
        const { t, dark } = this.props;
        return (
            <Above from={grid.lg}>
                <SHeaderMenu
                    className={CN({
                        dark: dark
                    })}
                >
                    <SLi>
                        <Link route="catalog">
                            <a>
                                <span>{t("menu_catalog")}</span>
                            </a>
                        </Link>
                    </SLi>
                    <SLi>
                        <Link route="page" params={{ slug: "company" }}>
                            <a>
                                <span>{t("menu_about")}</span>
                            </a>
                        </Link>
                    </SLi>
                    <SLi>
                        <Link route="blog">
                            <a>
                                <span>{t("menu_blog")}</span>
                            </a>
                        </Link>
                    </SLi>
                    <SLi>
                        <Link route="contacts">
                            <a>
                                <span>{t("menu_contacts")}</span>
                            </a>
                        </Link>
                    </SLi>
                </SHeaderMenu>
            </Above>
        );
    }
}

const SHeaderMenu = styled.ul`
    display: flex;
    margin-left: 2.2em;
    position: relative;
    z-index: 2;
    ${down("lg")} {
        margin-left: 1.2em;
    }
    ${down("md")} {
    }
    ${down("sm")} {
    }
`;

const SLi = styled.li`
    margin-right: 2em;
    ${down("lg")} {
        margin-right: 1.6em;
    }
    &:last-of-type {
        margin-right: 0;
    }
    a {
        position: relative;
        font-weight: 500;
        color: ${colors.black};
        height: ${header.height};
        display: flex;
        align-items: center;
        ${SHeaderMenu}.dark & {
            color: ${colors.white};
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
                ${SHeaderMenu}.dark & {
                    color: ${colors.blue_light};
                }
            }
        }
        &.active {
            cursor: default;
            color: ${colors.blue};
        }
    }
    ${SHeaderMenu}.dark & {
    }
`;

export default withTranslation("common")(HeaderMenu);
