import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { Global, css } from "@emotion/core";

import { Below } from "~/components";
import { colors, grid } from "~/config/var";

class HeaderButtonMenu extends PureComponent {
    constructor(props) {
        super(props);
        this.toggleMenu = this.toggleMenu.bind(this);
    }
    static propTypes = {
        dark: PT.bool
    };

    toggleMenu() {
        const _body = document.getElementsByTagName("body")[0];
        if (_body.classList.contains("mobile_menu_active")) {
            _body.classList.remove("mobile_menu_active");
        } else {
            _body.classList.add("mobile_menu_active");
        }
    }

    render() {
        const { dark } = this.props;
        return (
            <>
                <Below from={grid.lg}>
                    <BtnMobileMenu
                        className={CN(`BtnMobileMenu`, {
                            dark: dark
                        })}
                        onClick={this.toggleMenu}
                    >
                        <span />
                    </BtnMobileMenu>
                </Below>
                <SHeaderStyle />
            </>
        );
    }
}

const BtnMobileMenu = styled.button`
    background: none;
    border: 0;
    padding: 0;
    position: relative;
    z-index: 22;
    cursor: pointer;
    transition: all 250ms cubic-bezier(0.4, 0, 0.2, 1);
    margin-left: 1em;
    span {
        width: 24px;
        height: 3px;
        margin: 10px 0;
        background: ${colors.black};
        position: relative;
        border-radius: 2px;
        transition: all 250ms cubic-bezier(0.68, -0.55, 0.265, 1.55);
        display: block;
        &:before,
        &:after {
            content: "";
            border-radius: 2px;
            height: 3px;
            background: ${colors.black};
            position: absolute;
            transition: all 250ms cubic-bezier(0.4, 0, 0.2, 1);
            right: 0;
        }
        &:before {
            top: -7px;
            width: 20px;
        }
        &:after {
            content: " ";
            top: 7px;
            width: 16px;
        }
    }
    &.dark {
        span {
            background: ${colors.white};
            &:before,
            &:after {
                background: ${colors.white};
            }
        }
    }
`;

const SHeaderStyle = () => (
    <Global
        styles={css`
            body.mobile_menu_active {
                .BtnMobileMenu {
                    span {
                        background: 0 0;
                        &:before,
                        &:after {
                            background: ${colors.blue};
                            top: 0;
                            width: 24px;
                        }
                        &:before {
                            transform: rotate(45deg);
                        }
                        &:after {
                            transform: rotate(-45deg);
                        }
                    }
                    &.white {
                        span {
                            &:before,
                            &:after {
                                background: ${colors.green};
                            }
                        }
                    }
                }
            }
        `}
    />
);

export default HeaderButtonMenu;
