import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import Router from "next/router";
import { position } from "polished";

import { HeaderPanelBackground } from "../";
import { withTranslation } from "~/i18n";
import { Link } from "~/routes";
import { ArrowLeftIcon, ArrowRightIcon } from "~/components/icons";
import { Container } from "~/components";
import { colors, down, header } from "~/config/var";

class HeaderNavigation extends PureComponent {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        t: PT.any,
        prev: PT.object,
        next: PT.object
    };

    render() {
        const { t, prev, next } = this.props;
        return (
            <SHeaderNavigation>
                <HeaderPanelBackground />
                <Container>
                    <SBlock>
                        {prev ? (
                            <SAction className="SPrev">
                                <Link route={prev.route} params={prev.params}>
                                    <a>
                                        <ArrowLeftIcon />
                                        <SActionText>{prev.title}</SActionText>
                                    </a>
                                </Link>
                            </SAction>
                        ) : (
                            <SAction />
                        )}
                        <SBack onClick={() => Router.back()}>
                            {t("blog_back")}
                        </SBack>
                        {next ? (
                            <SAction className="SNext">
                                <Link route={next.route} params={next.params}>
                                    <a>
                                        <SActionText>{next.title}</SActionText>
                                        <ArrowRightIcon />
                                    </a>
                                </Link>
                            </SAction>
                        ) : (
                            <SAction />
                        )}
                    </SBlock>
                </Container>
            </SHeaderNavigation>
        );
    }
}

const SHeaderNavigation = styled.div`
    ${position("fixed", "5.98em", null, null, "0")};
    width: 100%;
    padding: 0 40px;
    display: flex;
    align-items: center;
    transition: top 0.3s ease-out;
    z-index: 12;
    margin-top: -2px;
    ${down("md")} {
        padding: 0;
        top: ${header.height_md};
    }
    ${down("sm")} {
        top: ${header.height_sm};
    }
    .header_fixed & {
        top: ${header.height};
        ${down("md")} {
            top: ${header.height_md};
        }
        ${down("sm")} {
            top: ${header.height_sm};
        }
    }
`;
const SBlock = styled.div`
    position: relative;
    width: 100%;
    padding: 0 20px;
    z-index: 3;
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 42px;
`;
const SAction = styled.div`
    flex: 0 0 30%;
    width: 30%;
    a {
        display: flex;
        align-items: center;
        width: 100%;
    }
    svg {
        width: 0.8em;
        height: 0.8em;
    }
    &.SNext {
        justify-content: flex-end;
        text-align: right;
    }
    ${down("md")} {
        display: none;
    }
`;
const SActionText = styled.div`
    padding: 0 0.5em;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    width: 100%;
`;
const SBack = styled.div`
    width: 40%;
    text-align: center;
    font-weight: 600;
    color: ${colors.blue};
    cursor: pointer;
    transition: all 0.3s ease-out;
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
        }
    }
    ${down("md")} {
        width: 100%;
    }
`;

export default withTranslation()(HeaderNavigation);
