import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";

import { colors, up, down, global, max_width, header } from "~/config/var";

class Header extends PureComponent {
    static propTypes = {
        dark: PT.bool
    };

    render() {
        const { dark } = this.props;
        return (
            <SHeaderImage
                className={CN({
                    dark: dark
                })}
            />
        );
    }
}

const SHeaderImage = styled.div`
    ${position("absolute", "0", null, null, "50%")};
    transform: translateX(-50%);
    height: 100%;
    background-color: ${colors.white};
    /* border-radius: ${global.border_radius}; */
    bottom: 1px solid ${colors.grey_light5};
    box-shadow: 0px 2px 8px 0 rgba(0, 0, 0, 0.05);
    transition: ${header.transition_panel_bg};
    z-index: 2;
    ${up("md")} {
        .header_fixed & {
            width: 100%;
            border-radius: 0;
        }
    }
    ${down("md")} {
        width: 100%;
    }
    ${up("lg")} {
        width: calc(${max_width.lg}px - 30px);
    }
    ${up("xl")} {
        width: calc(${max_width.xl}px - 30px);
    }
    ${up("xga")} {
        width: calc(${max_width.xga}px - 30px);
    }
    &.dark {
        background-color: ${colors.black_one};
        border: 1px solid ${colors.black_bor};
    }
`;

export default Header;
