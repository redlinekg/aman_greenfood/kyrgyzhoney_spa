import React, { Component } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";

import { withTranslation } from "~/i18n";
import {
    PlainGrid,
    ProductCard,
    Container,
    Button,
    Scrollbars
} from "~/components";
import { down, colors, header } from "~/config/var";
import { observer, inject } from "mobx-react";

// import { categories } from "~/config/temp/catalog";

@inject("categories_store")
@observer
class HeaderMenuCatalog extends Component {
    static propTypes = {
        t: PT.any,
        fixed: PT.bool,
        open: PT.bool,
        handleLeave: PT.func,
        handleHover: PT.func,
        categories_store: PT.object.isRequired
    };

    state = {
        active: 0
    };

    selectItem = id => {
        this.setState({ active: id });
    };

    renderCategories = () => {
        const {
            categories_store: { objects }
        } = this.props;
        return objects.map((category, idx) => {
            return (
                <SCategoriesItem
                    key={idx}
                    className={CN({
                        active: this.state.active == idx
                    })}
                >
                    <span onMouseEnter={this.selectItem.bind(this, idx)}>
                        {category.title}
                    </span>
                </SCategoriesItem>
            );
        });
    };
    renderProducts = () => {
        const {
            categories_store: { objects }
        } = this.props;
        return objects.map((i, idx) => {
            return (
                <SProductsList
                    key={idx}
                    className={CN({
                        active: this.state.active == idx
                    })}
                >
                    <SProductsDesc>{i.description}</SProductsDesc>
                    <PlainGrid
                        xga={5}
                        xl={5}
                        lg={3}
                        md={2}
                        sm={1}
                        padding_small
                    >
                        {i.products.map((l, idx) => (
                            <ProductCard key={idx} card={l} small />
                        ))}
                    </PlainGrid>
                </SProductsList>
            );
        });
    };

    render() {
        const { t } = this.props;
        return (
            <SHeaderMenuCatalog
                className={CN({
                    fixed: this.props.fixed,
                    open: this.props.open
                })}
            >
                <SBg onMouseEnter={this.props.handleLeave} />
                <Container>
                    <SBlock onMouseEnter={this.props.handleHover}>
                        <SSidebar>
                            <SCategoriesList>
                                {this.renderCategories()}
                            </SCategoriesList>
                            <SButton href="#">
                                <Button color="yellow">
                                    {t("menu_pricelist")}
                                </Button>
                            </SButton>
                        </SSidebar>
                        <SProducts>
                            <Scrollbars
                                autoHeight
                                autoHeightMin={100}
                                autoHeightMax="100%"
                            >
                                {this.renderProducts()}
                            </Scrollbars>
                        </SProducts>
                    </SBlock>
                </Container>
            </SHeaderMenuCatalog>
        );
    }
}

const SHeaderMenuCatalog = styled.div`
    ${position("fixed", "50px", "0", null, null)};
    width: 100%;
    list-style-type: none;
    transition: all 0.1s ease;
    padding: 0;
    opacity: 0;
    visibility: hidden;
    height: 100vh;
    z-index: 13;
    &.open {
        opacity: 1;
        visibility: visible;
        top: 0;
    }
    &.fixed {
    }
`;
const SBg = styled.div`
    background-color: rgba(32, 32, 32, 0.7);
    ${position("absolute", "0", "0", "0", "0")};
    background-color: rgba(32, 32, 32, 0.7);
`;

const SBlock = styled.div`
    display: flex;
    align-items: flex-start;
    width: 100%;
    position: relative;
    z-index: 2;
    background-color: ${colors.white};
    border: 1px solid ${colors.grey_light5};
    box-shadow: 0px 2px 8px 0 rgba(0, 0, 0, 0.05);
    margin-top: 114px;
    height: calc(100vh - 8em);
    transition: ${header.transition_dropdown};
    ${SHeaderMenuCatalog}.fixed & {
        margin-top: ${header.height};
    }
    ${down("sm")} {
    }
    &:before {
        content: "";
        ${position("absolute", "0", null, null, "0")};
        background-color: ${colors.blue_light3};
        height: 100%;
        width: 20%;
        ${down("lg")} {
            width: 26%;
        }
    }
`;
const SSidebar = styled.div`
    width: 20%;
    padding: 2em;
    position: relative;
    ${down("lg")} {
        padding: 1.5em;
        width: 26%;
    }
`;
const SCategoriesList = styled.ul``;
const SCategoriesItem = styled.li`
    font-size: 1.1em;
    margin-bottom: 0.5em;
    &:last-child {
        margin-bottom: 0;
    }
    &.active {
        span {
            color: ${colors.blue};
        }
    }
    span {
        transition: all 0.1s ease-in-out;
        cursor: pointer;
        display: inline-block;
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
            }
        }
    }
`;

const SButton = styled.a`
    display: block;
    margin-top: 1.2em;
`;

const SProducts = styled.div`
    width: 80%;
    height: 100%;
    ${down("lg")} {
        width: 74%;
    }
`;
const SProductsList = styled.div`
    display: none;
    padding: 2em;
    ${down("lg")} {
        padding: 1.5em;
    }
    &.active {
        display: block;
    }
`;
const SProductsDesc = styled.div`
    margin-bottom: 1em;
`;

export default withTranslation("common")(HeaderMenuCatalog);
