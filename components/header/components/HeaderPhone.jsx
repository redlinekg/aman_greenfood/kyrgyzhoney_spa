import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import CN from "classnames";
import { position } from "polished";

import { withTranslation } from "~/i18n";
import { HoneycombIcon, PhoneIcon } from "~/components/icons";
import { OpenModal, Above } from "~/components";
import { colors, down, grid } from "~/config/var";

class HeaderPhone extends Component {
    static propTypes = {
        dark: PT.bool,
        t: PT.any
    };
    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
    render() {
        const { t, dark } = this.props;
        return (
            <Above from={grid.xl}>
                <SHeaderPhone
                    className={CN({
                        dark: dark
                    })}
                >
                    <SPhone href="tel:+996312882296">
                        +996 (312) 88
                        <i />
                        22
                        <i />
                        96
                    </SPhone>
                    <OpenModal name="callme">
                        <SCallme>
                            <SIcon>
                                <SHoneycomb>
                                    <HoneycombIcon />
                                </SHoneycomb>
                                <SPhoneIcon>
                                    <PhoneIcon />
                                </SPhoneIcon>
                            </SIcon>
                            <SCallmeText>{t("callme")}</SCallmeText>
                        </SCallme>
                    </OpenModal>
                </SHeaderPhone>
            </Above>
        );
    }
}

const SHeaderPhone = styled.div`
    display: flex;
    align-items: center;
    ${down("sm")} {
    }
`;
const SPhone = styled.a`
    color: ${colors.black};
    ${SHeaderPhone}.dark & {
        color: ${colors.white};
    }
    i {
        width: 5px;
        height: 5px;
        border-radius: 50%;
        display: inline-block;
        margin: 0 0.2em;
        opacity: 0.35;
        background-color: ${colors.blue_light};
        position: relative;
        top: -0.2em;
        transition: all 0.2s ease-in-out;
        ${SHeaderPhone}.dark & {
            opacity: 0.8;
        }
    }
    @media (hover) {
        &:hover {
            color: ${colors.blue_light};
            i {
                background-color: ${colors.yellow_light};
            }
            ${SHeaderPhone}.dark & {
                color: ${colors.blue_light};
                i {
                    opacity: 1;
                }
            }
        }
    }
    ${down("xl")} {
        display: none;
    }
`;
//
const SIcon = styled.div`
    padding: 0 1em;
    position: relative;
`;
const SHoneycomb = styled.div`
    color: ${colors.yellow_light};
    svg {
        width: 2.4em;
        height: 2.4em;
    }
`;
const SPhoneIcon = styled.div`
    ${position("absolute", "50%", null, null, "50%")};
    transform: translateX(-50%) translateY(-50%);
    svg {
        width: 0.8em;
        height: 0.8em;
    }
`;
//
const SCallme = styled.div`
    display: flex;
    align-items: center;
    cursor: pointer;
`;
const SCallmeText = styled.div`
    color: ${colors.black};
    transition: all 0.2s ease-in-out;
    ${SHeaderPhone}.dark & {
        color: ${colors.white};
    }
    @media (hover) {
        ${SCallme}:hover & {
            color: ${colors.blue_light};
            ${SHeaderPhone}.dark & {
                color: ${colors.blue_light};
            }
        }
    }
`;

export default withTranslation("common")(HeaderPhone);
