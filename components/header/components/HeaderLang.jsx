import React, { Component } from "react";
import PT from "prop-types";
import CN from "classnames";
import styled from "@emotion/styled";
import Select from "react-select";

import { i18n, withTranslation } from "~/i18n";
import { Above, Below } from "~/components";
import { colors, down, fonts, grid } from "~/config/var";

const options = [
    { value: "ru", label: "Russian", lang: "ru" },
    { value: "en", label: "English", lang: "en" },
    { value: "kg", label: "Kyrgyz", lang: "kg" },
    { value: "cn", label: "Chinese", lang: "cn" }
];

const options_mobile = [
    { value: "ru", label: "Rus", lang: "ru" },
    { value: "en", label: "Eng", lang: "en" },
    { value: "kg", label: "Kyr", lang: "kg" },
    { value: "cn", label: "Chi", lang: "cn" }
];

class HeaderLang extends Component {
    static propTypes = {
        dark: PT.bool,
        value: PT.string
    };
    handleChange = value => {
        if (value.lang === "ru") {
            i18n.changeLanguage("ru");
        } else if (value.lang === "en") {
            i18n.changeLanguage("en");
        } else if (value.lang === "kg") {
            i18n.changeLanguage("kg");
        } else if (value.lang === "cn") {
            i18n.changeLanguage("cn");
        }
    };
    render() {
        let { dark } = this.props;
        return (
            <SHeaderLang
                className={CN({
                    dark: dark
                })}
            >
                <SBlock>
                    <Above from={grid.xs}>
                        <Select
                            classNamePrefix="select_lang"
                            options={options}
                            defaultValue={
                                i18n.language === "ru"
                                    ? options[0]
                                    : i18n.language === "en"
                                    ? options[1]
                                    : i18n.language === "kg"
                                    ? options[2]
                                    : i18n.language === "cn"
                                    ? options[3]
                                    : options[0]
                            }
                            onChange={this.handleChange}
                            value={this.props.value}
                        />
                    </Above>
                    <Below from={grid.xs}>
                        <Select
                            classNamePrefix="select_lang"
                            options={options_mobile}
                            defaultValue={
                                i18n.language === "ru"
                                    ? options_mobile[0]
                                    : i18n.language === "en"
                                    ? options_mobile[1]
                                    : i18n.language === "kg"
                                    ? options_mobile[2]
                                    : i18n.language === "cn"
                                    ? options_mobile[3]
                                    : options_mobile[0]
                            }
                            onChange={this.handleChange}
                            value={this.props.value}
                        />
                    </Below>
                </SBlock>
            </SHeaderLang>
        );
    }
}

const SHeaderLang = styled.div``;
const SBlock = styled.div`
    margin-left: 2em;
    font-family: ${fonts.ff_global};
    ${down("lg")} {
        margin-left: 1.2em;
    }
    .select_lang__menu {
        box-shadow: 0 6px 14px 5px rgba(0, 0, 0, 0.1);
        min-width: 110px;
        right: 0;
    }
    .select_lang__control {
        border: 0 !important;
        box-shadow: none !important;
        background-color: transparent !important;
        cursor: pointer;
        @media (hover) {
            &:hover {
                .select_lang__single-value {
                    color: ${colors.blue_light};
                    ${SHeaderLang}.dark & {
                        color: ${colors.blue_light};
                    }
                }
            }
        }
        .select_lang__dropdown-indicator {
            transition: all 0.2s ease-in-out;
        }
    }
    .select_lang__value-container {
        justify-content: flex-end;
        padding: 0 0.6em 0 0;
        .select_lang__single-value {
            position: relative;
            transform: none;
            margin: 0;
            top: 0;
            max-width: none;
            transition: all 0.2s;
            ${SHeaderLang}.dark & {
                color: ${colors.white};
            }
        }
        .css-b8ldur-Input {
            position: absolute;
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
    }
    .select_lang__indicator-separator {
        display: none;
    }
    .select_lang__indicator {
        background: ${colors.blue_light2};
        color: ${colors.black};
        border-radius: 50%;
        padding: 2px;
        svg {
            width: 0.9em;
            height: 0.9em;
        }
    }
    .select_lang__option {
        transition: all 0.2s ease-in-out;
        font-weight: 400;
        border-bottom: 1px solid ${colors.grey_light4};
        padding: 0.4em 1.2em;
        color: ${colors.black};
        text-align: right;
        &:last-child {
            border-bottom: 0;
        }
        &.select_lang__option--is-focused {
            background-color: transparent;
            color: ${colors.grey};
            cursor: pointer;
        }
        &.select_lang__option--is-selected {
            background-color: transparent;
            color: ${colors.blue};
            cursor: default;
        }
        &:active {
            background-color: transparent;
            color: ${colors.blue};
        }
    }
    .select_lang__control--menu-is-open {
        .select_lang__single-value {
            color: ${colors.blue};
            ${SHeaderLang}.dark & {
                color: ${colors.blue_light};
            }
        }
        .select_lang__dropdown-indicator {
            transform: rotate(180deg);
            color: ${colors.black};
        }
        .select_lang__indicator {
        }
    }
`;

export default withTranslation("common")(HeaderLang);
