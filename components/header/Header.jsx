import styled from "@emotion/styled";
import CN from "classnames";
import debounce from "lodash/debounce";
import { position } from "polished";
import PT from "prop-types";
import React, { PureComponent } from "react";
import dynamic from "next/dynamic";
import { Above, Container } from "~/components";
import { down, grid, header } from "~/config/var";
import { withTranslation } from "~/i18n";
import {
    HeaderButtonMenu,
    HeaderLang,
    HeaderLogo,
    HeaderMenu,
    HeaderPanelBackground,
    HeaderPhone
} from "./";

const DynamicHeaderCart = dynamic(() => import("./components/HeaderCart"), {
    ssr: false
});

class Header extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { fixed: false };
    }
    static propTypes = {
        header_background_image: PT.string,
        header_background_pattern: PT.bool,
        dark_theme: PT.bool,
        header_sub_menu: PT.object,
        header_navigation: PT.object,
        header_type: PT.oneOf(["default", "index", "sub"])
    };

    componentDidMount() {
        window.addEventListener("scroll", this.scrollHeaderFixsed);
        this.scrollHeaderFixsed();
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.scrollHeaderFixsed);
    }

    scrollHeaderFixsed = debounce(() => {
        const scrollPx = document.documentElement.scrollTop;
        // const scroll_top_fixed = this.props.header_type === "index" ? 500 : 80;
        const next_value = scrollPx >= 80;
        if (this.state.fixed === next_value) return;
        this.setState({
            fixed: next_value
        });
    }, 10);

    render() {
        const {
            header_background_image,
            header_background_pattern,
            dark_theme,
            header_type
        } = this.props;
        const { fixed } = this.state;
        return (
            <SHeaderFixed
                className={CN(`SHeaderFixed `, {
                    header_fixed: fixed,
                    sub: header_type === "sub",
                    header_index: header_type === "index",
                    header_error: header_type === "error"
                })}
            >
                <Above from={grid.lg}>
                    <>
                        {header_background_image ? (
                            <SBgImage
                                className={CN({
                                    pattern: header_background_pattern
                                })}
                                style={{
                                    backgroundImage: `url(${header_background_image}?method=fit&width=620&height=620)`
                                }}
                            />
                        ) : null}
                    </>
                </Above>
                <SHeader>
                    <HeaderPanelBackground dark={dark_theme} />
                    <Container>
                        <SBlock>
                            <HeaderLogo dark={dark_theme} fixed={fixed} />
                            <HeaderMenu dark={dark_theme} fixed={fixed} />
                            <SRight>
                                <HeaderPhone dark={dark_theme} />
                                <HeaderLang dark={dark_theme} />
                                <DynamicHeaderCart dark={dark_theme} />
                                <HeaderButtonMenu dark={dark_theme} />
                            </SRight>
                        </SBlock>
                    </Container>
                </SHeader>
                <div id="header_portal"></div>
                <SHeaderDropdown id="SHeaderDropdown" />
            </SHeaderFixed>
        );
    }
}

const SHeaderFixed = styled.div`
    position: relative;
    height: 116px;
    ${down("md")} {
        height: ${header.height_md};
    }
    ${down("sm")} {
        height: ${header.height_sm};
    }
    &.header_fixed {
    }
    &.header_index,
    &.header_error {
        height: auto;
    }
    &.sub {
        height: calc(${header.height} + ${header.sub});
        ${down("md")} {
            height: calc(${header.height_md} + ${header.sub});
        }
        ${down("sm")} {
            height: calc(${header.height_sm} + ${header.sub});
        }
    }
`;

const SHeader = styled.header`
    ${position("fixed", header.top, null, null, "0")};
    width: 100%;
    display: flex;
    align-items: center;
    height: ${header.height};
    transition: ${header.transition_top};
    z-index: 15;
    padding: 0 40px;
    ${SHeaderFixed}.header_fixed & {
        top: 0;
    }
    ${SHeaderFixed}.header_index & {
    }
    ${down("lg")} {
        padding: 0 20px;
    }
    ${down("md")} {
        height: ${header.height_md};
        padding: 0;
        top: 0;
    }
    ${down("sm")} {
        height: ${header.height_sm};
    }
`;

const SHeaderDropdown = styled.div``;
const SBgImage = styled.div`
    width: 100%;
    height: 77px;
    background-size: cover;
    background-position: top center;
    ${position("fixed", "0", null, null, "0")};
    z-index: 11;
    transition: all 0.3s ease-out;
    &.pattern {
        background-size: 340px;
    }
    ${SHeaderFixed}.header_fixed & {
        height: 66px;
    }
`;

const SBlock = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    padding: 0 20px;
    z-index: 3;
    ${down("sm")} {
        padding: 0;
    }
`;

const SRight = styled.div`
    display: flex;
    align-items: center;
    margin-left: auto;
    position: relative;
    z-index: 2;
`;

export default withTranslation("common")(Header);
