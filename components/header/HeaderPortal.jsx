import PT from "prop-types";
import ReactDOM from "react-dom";
import { isNode } from "browser-or-node";

const HeaderPortal = ({ children }) => {
    if (isNode) return null;
    return ReactDOM.createPortal(
        children,
        document.querySelector("#header_portal")
    );
};

HeaderPortal.propTypes = {
    childrend: PT.any
};

export default HeaderPortal;
