import React, { Component } from "react";
import PT from "prop-types";
import { position } from "polished";
import styled from "@emotion/styled";
import { Global, css, keyframes } from "@emotion/core";

import { OpenModal } from "~/components";
import { Link } from "~/routes";
import { withTranslation } from "~/i18n";
import { colors, down, global } from "~/config/var";

import { withRouter } from "next/router";

@withRouter
class MobileMenu extends Component {
    static propTypes = {
        router: PT.object,
        t: PT.any.isRequired
    };
    constructor() {
        super();
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        const _body = document.getElementsByTagName("body")[0];
        if (_body.classList.contains("mobile_menu_active")) {
            _body.classList.remove("mobile_menu_active");
        } else {
            _body.classList.add("mobile_menu_active");
        }
    }

    componentDidUpdate(prevProps) {
        const _body = document.getElementsByTagName("body")[0];
        if (this.props.router.asPath !== prevProps.router.asPath) {
            _body.classList.remove("mobile_menu_active");
        }
    }

    render() {
        const { t } = this.props;
        return (
            <SMobileMenu>
                <div className="mobile_menu_mask" onClick={this.toggle} />
                <div className="mobile_menu">
                    <SBlock>
                        <SMenu>
                            <SLi>
                                <OpenModal name="callme">
                                    <a>
                                        <span>{t("callme_mobile")}</span>
                                    </a>
                                </OpenModal>
                            </SLi>
                            <SLi>
                                <Link route="catalog">
                                    <a>
                                        <span>{t("menu_catalog")}</span>
                                    </a>
                                </Link>
                            </SLi>
                            <SLi>
                                <Link route="page" params={{ slug: "company" }}>
                                    <a>
                                        <span>{t("menu_about")}</span>
                                    </a>
                                </Link>
                            </SLi>
                            <SLi>
                                <Link route="blog">
                                    <a>
                                        <span>{t("menu_blog")}</span>
                                    </a>
                                </Link>
                            </SLi>
                            <SLi>
                                <Link route="contacts">
                                    <a>
                                        <span>{t("menu_contacts")}</span>
                                    </a>
                                </Link>
                            </SLi>
                        </SMenu>
                        <SPhoneBlock>
                            <SPhone href="tel">
                                +996 (555) 30
                                <i />
                                30
                                <i />
                                30
                            </SPhone>
                        </SPhoneBlock>
                    </SBlock>
                </div>
                <MobileMenuStyle />
            </SMobileMenu>
        );
    }
}

const SMobileMenu = styled.div`
    .mobile_menu {
        ${position("fixed", null, "0", "0", "0")};
        z-index: 9;
        margin: 0;
        background-color: ${colors.white};
        border-radius: ${global.border_radius} ${global.border_radius} 0 0;
        display: none;
        ${down("md")} {
        }
        ${down("sm")} {
        }
    }
    .mobile_menu_mask {
        content: "";
        ${position("fixed", null, "0", "0", "0")};
        width: 100%;
        height: 100%;
        background-color: rgba(60, 60, 75, 0.6);
        z-index: 9;
        display: none;
    }

    ${down("md")} {
    }
`;

const SBlock = styled.div`
    overflow-y: auto;
    height: 100%;
    -webkit-overflow-scrolling: touch;
`;

const SMenu = styled.ul`
    padding: 2em 15px 1em;
    background: ${colors.beige_light};
`;

const animation = keyframes`
  0% {
    opacity: 0;
    transform: translate(-1.25rem);
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: 1;
    transform: translate(0);
  }
`;

const SLi = styled.li`
    font-weight: 500;
    margin-bottom: 0.8em;
    display: block;
    opacity: 0;
    text-align: center;
    font-size: 1.2em;
    &:nth-of-type(1) {
        animation: ${animation} 0.2s 0.1s 1 forwards;
    }
    &:nth-of-type(2) {
        animation: ${animation} 0.2s 0.11s 1 forwards;
    }
    &:nth-of-type(3) {
        animation: ${animation} 0.2s 0.21s 1 forwards;
    }
    &:nth-of-type(4) {
        animation: ${animation} 0.2s 0.31s 1 forwards;
    }
    &:nth-of-type(5) {
        animation: ${animation} 0.2s 0.41s 1 forwards;
    }
    &:nth-of-type(6) {
        animation: ${animation} 0.2s 0.51s 1 forwards;
    }
    .SHeaderMenuLink {
        &.active {
            color: ${colors.blue};
            pointer-events: none;
        }
        &:active {
            color: ${colors.blue};
        }
    }
`;

const SPhoneBlock = styled.div`
    display: flex;
    justify-content: center;
`;

const SPhone = styled.a`
    color: ${colors.white};
    display: block;
    padding: 1.4em 0;
    background-color: ${colors.blue};
    width: 100%;
    text-align: center;
    i {
        width: 5px;
        height: 5px;
        border-radius: 50%;
        display: inline-block;
        margin: 0 0.2em;
        opacity: 0.35;
        background-color: ${colors.blue_light};
        position: relative;
        top: -0.2em;
        transition: all 0.2s ease-in-out;
    }
`;
//

const MobileMenuStyle = () => (
    <Global
        styles={css`
            html body.mobile_menu_active {
                .mobile_menu {
                    display: block;
                }
                .mobile_menu_mask {
                    display: block;
                }
            }
        `}
    />
);

export default withTranslation("common")(MobileMenu);
