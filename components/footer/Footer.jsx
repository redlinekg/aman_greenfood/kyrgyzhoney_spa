import React, { PureComponent } from "react";
import CN from "classnames";
import PT from "prop-types";
import styled from "@emotion/styled";

import { LogoIcon } from "~/components/icons";
import { Container, Social } from "~/components";
import { withTranslation } from "~/i18n";
import { colors, down } from "~/config/var";

import DangerCactus from "./DangerCactus";

var date = new Date();
var dateStr = date.getFullYear();

class Footer extends PureComponent {
    static propTypes = {
        t: PT.any.isRequired,
        show_footer_margin: PT.bool,
        show_footer: PT.bool
    };
    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
    render() {
        const { t, show_footer_margin } = this.props;
        return (
            <SFooter
                className={CN({
                    show_footer_margin: show_footer_margin
                })}
            >
                <Container>
                    <Block>
                        <SIcon>
                            <LogoIcon />
                        </SIcon>
                        <SCopyright>
                            <SCopyrightTitle>
                                {t("footer_copyright_title")} {dateStr}
                            </SCopyrightTitle>
                            <SCopyrightText>
                                {t("footer_copyright_text")}
                            </SCopyrightText>
                        </SCopyright>
                        {/* <SCategories>
                            <SCategory>
                                <a href="">Kyrgyz Honey</a>
                            </SCategory>
                            <SCategory>
                                <a href="">Bal plus</a>
                            </SCategory>
                            <SCategory>
                                <a href="">Bal Azyk</a>
                            </SCategory>
                            <SCategory>
                                <a href="">Сопутствующие товары</a>
                            </SCategory>
                        </SCategories> */}
                        <SSocial>
                            <Social />
                        </SSocial>
                        <DangerCactus />
                    </Block>
                </Container>
            </SFooter>
        );
    }
}

const SFooter = styled.footer`
    padding: 2em 0;
    background-color: ${colors.grey_light6};
    &.show_footer_margin {
        margin-top: 5em;
        ${down("md")} {
            margin-top: 4em;
        }
        ${down("sm")} {
            margin-top: 3em;
        }
    }
`;
const Block = styled.div`
    display: flex;
    align-items: flex-end;
    ${down("md")} {
        flex-wrap: wrap;
    }
    ${down("sm")} {
    }
`;
////////////////////
const SIcon = styled.div`
    svg {
        width: 2.6em;
        height: 2.6em;
    }
    ${down("md")} {
        display: none;
    }
`;
////////////////////
const SCopyright = styled.div`
    padding-left: 1em;
    ${down("md")} {
        padding-left: 0;
    }
`;
const SCopyrightTitle = styled.div`
    color: ${colors.black};
    font-weight: 500;
    ${down("md")} {
        font-size: 0.9em;
    }
`;
const SCopyrightText = styled.div`
    font-size: 0.8em;
    color: ${colors.grey};
    max-width: 22em;
`;
////////////////////
// const SCategories = styled.ul`
//     flex-direction: column;
//     column-count: 2;
//     padding-left: 2em;
//     font-size: 0.9em;
// `;
// const SCategory = styled.li`
//     a {
//         color: ${colors.grey};
//         line-height: 1.6;
//         display: block;
//         @media (hover) {
//             &:hover {
//                 color: ${colors.blue_light};
//             }
//         }
//     }
// `;
////////////////////
const SSocial = styled.div`
    padding-left: 2em;
    display: grid;
    grid-template-columns: 1fr 1fr;
    ${down("md")} {
        padding-left: 1em;
    }
    ${down("sm")} {
        padding-top: 1em;
        padding-left: 0;
    }
`;

export default withTranslation(["common"])(Footer);
