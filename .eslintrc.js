module.exports = {
    env: {
        browser: true,
        node: true,
        commonjs: true,
        es6: true
    },
    extends: [
        "plugin:react/recommended",
        "eslint:recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:prettier/recommended"
    ],
    parser: "babel-eslint",
    parserOptions: {
        ecmaFeatures: {
            experimentalObjectRestSpread: true,
            experimentalDecorators: true,
            jsx: true
        },
        ecmaVersion: 6,
        sourceType: "module"
    },
    plugins: ["import", "react", "prettier"],
    rules: {
        "require-atomic-updates": "off",
        indent: [
            "off",
            4,
            {
                SwitchCase: 1
            }
        ],
        "react/prop-types": 2,
        "linebreak-style": 0,
        semi: ["error", "always"],
        "global-require": "off",
        quotes: [
            "error",
            "double",
            { avoidEscape: true, allowTemplateLiterals: true }
        ],
        "no-debugger": "off",
        "no-console": "off",
        // prettier
        "prettier/prettier": "error",
        // react
        "react/jsx-uses-vars": 1,
        "react/display-name": "off"
    },
    settings: {
        "import/resolver": {
            node: {
                extensions: [".js", ".jsx", ".es6"]
            },
            alias: {
                map: [
                  ['~/*', './'],
                ],
                extensions: ['.ts', '.js', '.jsx', '.json']
              }
        }
    }
};
