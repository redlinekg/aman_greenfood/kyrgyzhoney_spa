import FontsStyle from "./FontsStyle";
import GlobalStyle from "./GlobalStyle";
import SlickStyle from "./SlickStyle";

export { FontsStyle, GlobalStyle, SlickStyle };
