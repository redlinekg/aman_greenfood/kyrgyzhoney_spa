import { css } from "@emotion/core";
import { fontFace } from "polished";

const FontsStyle = css`
    ${fontFace({
        fontFamily: "Fredoka One",
        fontFilePath: "/static/fonts/FredokaOne-Regular",
        fileFormats: ["woff2", "woff"],
        fontWeight: "normal"
    })}
    ${fontFace({
        fontFamily: "Futura PT",
        fontFilePath: "/static/fonts/FuturaPT-Light",
        fileFormats: ["woff2", "woff"],
        fontWeight: "300"
    })}
    ${fontFace({
        fontFamily: "Futura PT",
        fontFilePath: "/static/fonts/FuturaPT-Book",
        fileFormats: ["woff2", "woff"],
        fontWeight: "400"
    })}
    ${fontFace({
        fontFamily: "Futura PT",
        fontFilePath: "/static/fonts/FuturaPT-Medium",
        fileFormats: ["woff2", "woff"],
        fontWeight: "500"
    })}
    /* ${fontFace({
        fontFamily: "Futura PT Demi",
        fontFilePath: "/static/fonts/FuturaPT-Demi",
        fileFormats: ["woff2", "woff"],
        fontWeight: "600"
    })} */
    ${fontFace({
        fontFamily: "Futura PT",
        fontFilePath: "/static/fonts/FuturaPT-Bold",
        fileFormats: ["woff2", "woff"],
        fontWeight: "700"
    })}
    /* ${fontFace({
        fontFamily: "Futura PT Extra",
        fontFilePath: "/static/fonts/FuturaPT-ExtraBold",
        fileFormats: ["woff2", "woff"],
        fontWeight: "800"
    })} */
    /* ${fontFace({
        fontFamily: "Futura PT",
        fontFilePath: "/static/fonts/FuturaPT-Heavy",
        fileFormats: ["woff2", "woff"],
        fontWeight: "900"
    })} */
`;

export default FontsStyle;
