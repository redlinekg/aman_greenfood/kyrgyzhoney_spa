import { css } from "@emotion/core";
import { colors } from "../config/var";

const GlobalStyle = css`
    * {
        box-sizing: border-box;
        outline: none !important;
    }
    html {
        font-size: 1.2rem;
    }
    body {
        background-color: white;
        font-family: "Futura PT";
        font-weight: 400;
        line-height: 1.2;
        color: black;
        overflow-y: scroll;
    }
    input:focus {
        outline: none !important;
    }
    select {
        -webkit-appearance: none;
    }
    a {
        text-decoration: none;
        transition: all 0.2s ease-in-out;
        cursor: pointer;
        color: ${colors.black};
        @media (hover) {
            &:hover {
                color: ${colors.blue_light};
                text-decoration: none;
            }
        }
    }
    .read_more {
        color: ${colors.blue_light};
        @media (hover) {
            &:hover {
                color: ${colors.blue};
                text-decoration: none;
            }
        }
    }
    svg {
        display: block;
    }
    i {
        font-style: italic;
    }
    /* lightgallery */
    body {
        .lg-outer .lg-img-wrap {
            padding: 2em 0;
        }
        .lg-toolbar {
            background-color: transparent;
        }
    }
`;

export default GlobalStyle;
