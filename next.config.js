const path = require("path");
const withPlugins = require("next-compose-plugins");
const withTM = require("next-transpile-modules");
const images = require("next-images");

module.exports = withPlugins([withTM(["kyrgyzhoney_mobx"]), images], {
    env: {
        API_URL: process.env.API_URL || "/",
        BASE_URL: process.env.BASE_URL || "/"
    },
    webpack: function(config) {
        config.resolve.alias = {
            "~": path.resolve("./"),
            ...config.resolve.alias
        };
        config.resolve.symlinks = false;
        return config;
    }
});
