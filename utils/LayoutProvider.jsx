import hoistNonReactStatic from "hoist-non-react-statics";
import PT from "prop-types";
import React, { Component } from "react";

const LayoutContext = React.createContext({
    changeTheme: () => {}
});

const withLayout = WrappedComponent => {
    const Wrapper = props => (
        <LayoutContext.Consumer>
            {layout_context => (
                <WrappedComponent {...props} {...layout_context} />
            )}
        </LayoutContext.Consumer>
    );
    hoistNonReactStatic(Wrapper, WrappedComponent);
    return Wrapper;
};

const contextPropTypes = {
    //header
    header_background_image: PT.string,
    header_background_pattern: PT.bool,
    header_sub_menu: PT.object,
    header_navigation: PT.object,
    header_type: PT.string,
    //global
    title: PT.string,
    show_container: PT.bool,
    show_content_padding: PT.bool,
    dark_theme: PT.bool,
    //footer
    show_footer_margin: PT.bool,
    show_footer: PT.bool,
    //
    changeLayoutValue: PT.func
};

const default_state = () => ({
    //header
    header_background_image: undefined,
    header_background_pattern: false,
    header_sub_menu: undefined,
    header_navigation: undefined,
    header_type: "default",
    //global
    title: undefined,
    show_container: true,
    show_content_padding: true,
    dark_theme: false,
    //footer
    show_footer_margin: true,
    show_footer: true
});

class LayoutProvider extends Component {
    static propTypes = {
        children: PT.any
    };
    state = default_state();

    setValue = values => {
        this.setState({
            ...default_state(),
            ...values
        });
    };

    render() {
        return (
            <LayoutContext.Provider
                value={{ ...this.state, changeLayoutValue: this.setValue }}
            >
                {this.props.children}
            </LayoutContext.Provider>
        );
    }
}

export default LayoutProvider;
export { withLayout, contextPropTypes };
