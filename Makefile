TAG=registry.gitlab.com/redlinekg/aman_greenfood/kyrgyzhoney_spa

.PHONY: all build_spa publish_spa

all: build_spa publish_spa

build_spa:
	telegram-send '[kyrgyzhoney build_spa] start'
	cp -RfL ./node_modules/kyrgyzhoney_mobx ./
	rm -Rf ./kyrgyzhoney_mobx/node_modules
	docker build -t $(TAG) .
	rm -Rf ./kyrgyzhoney_mobx
	telegram-send '[kyrgyzhoney build_spa] end'

publish_spa:
	telegram-send '[kyrgyzhoney publish_spa] start'
	docker push $(TAG)
	telegram-send '[kyrgyzhoney publish_spa] end'
