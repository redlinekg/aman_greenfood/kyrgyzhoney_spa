const express = require("express");
const next = require("next");
const nextI18NextMiddleware = require("next-i18next/middleware").default;
const { createProxyMiddleware } = require("http-proxy-middleware");
const nextI18next = require("./i18n");
const routes = require("./routes");
// envs
const PORT = process.env.PORT || 3000;
const HOST = process.env.HOST || "0.0.0.0";
const DEV = process.env.NODE_ENV !== "production";
const API_URL = process.env.API_URL;
const MEDIA_REDIRECT_URL = process.env.MEDIA_REDIRECT_URL;

const app = next({ dev: DEV });
const handle = routes.getRequestHandler(app);

(async () => {
    await app.prepare();
    const server = express();

    if (DEV) {
        server.use(
            createProxyMiddleware("/uploads", {
                target: MEDIA_REDIRECT_URL,
                changeOrigin: true,
                xfwd: true
            })
        );
        server.use(
            createProxyMiddleware("/api", {
                target: API_URL,
                changeOrigin: true,
                xfwd: true,
                followRedirects: true
            })
        );
    }

    server.use(nextI18NextMiddleware(nextI18next));

    server.get("*", (req, res) => handle(req, res));

    await server.listen(PORT, HOST, err => {
        if (err) {
            throw err;
        }
        console.log(`> Ready on ${HOST}:${PORT}`);
    });
})();
