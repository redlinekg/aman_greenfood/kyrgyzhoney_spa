FROM registry.gitlab.com/redlinekg/redline-node-container
WORKDIR /app/
COPY ./package.json ./yarn.lock /app/
RUN yarn install --ignore-engines && yarn cache clean
ENV API_URL=http://nestjs:8080
COPY . /app
COPY ./kyrgyzhoney_mobx /app/node_modules/kyrgyzhoney_mobx
RUN yarn build
EXPOSE 3000
CMD yarn prod
