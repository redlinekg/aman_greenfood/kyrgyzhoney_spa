export const BASE_URL = "";
export const API_PREFIX = "api/v1/";
export const PUBLICATION_ENTRY = `${BASE_URL}${API_PREFIX}publications/`;
export const PUBLICATION_CATEGORY_ENTRY = `${BASE_URL}${API_PREFIX}publication_categories/`;
