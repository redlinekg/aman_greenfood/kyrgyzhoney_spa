import * as urls from "./urls";
import * as statuses from "./statuses";

export { urls, statuses };
