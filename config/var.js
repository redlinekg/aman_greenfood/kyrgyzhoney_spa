export { up, down, between, only } from "styled-breakpoints";
import { lighten } from "polished";

export const grey_list = {
    grey: "#5E5E5E"
};

export const colors = {
    body: "#f4f7fe",
    green: "#458DF3",

    red: "#FA5562",
    white: "#fff",
    black: "#202020",
    black_one: "#000000",
    black_bor: "#1d1d1d",
    blue: "#1D668A",
    blue_light: "#00ADEA",
    blue_light2: "#A8E3F8",
    blue_light3: "#F6F7FB",
    // yellow
    yellow: "#FBA919",
    yellow_light: "#FFD400",
    gold: "#BD8927",
    gold_light: "#FBA919",
    // grey colors
    grey: grey_list.grey,
    grey_bor: lighten(0.58, grey_list.grey),
    grey_light: lighten(0.3, grey_list.grey),
    grey_light2: lighten(0.4, grey_list.grey),
    grey_light3: lighten(0.5, grey_list.grey),
    grey_light4: lighten(0.55, grey_list.grey),
    grey_light5: lighten(0.58, grey_list.grey),
    grey_light6: lighten(0.59, grey_list.grey)
};

export const global = {
    padding: "3em",
    padding_large: "2em",
    padding_small: "1.5em",
    border_radius: "2px"
};

export const fonts = {
    ff_global: "Futura PT",
    ff_title: "Fredoka One"
};

export const main_slider = {
    height: "72vh",
    min_height: "600px",
    height_md: "72vh",
    min_height_md: "500px",
    height_sm: "60vh",
    min_height_sm: "380px"
};

export const header = {
    height: "74px",
    height_md: "68px",
    height_sm: "62px",
    sub: "42px",
    sub_md: "68px",
    sub_sm: "62px",
    //
    transition_panel_bg: "width 0.3s ease-out",
    transition_top: "top 0.3s ease-out",
    transition_dropdown: "all 0.3s ease-out",
    //
    top: "42px",
    top_fixed: "",
    sub_top: "",
    sub_top_fixed: "",
    //
    dropdown_top: "35px",
    dropdown_top_fixed: "68px",
    dropdown_height: "",
    dropdown_height_fixed: ""
};

export const grid = {
    xs: 372,
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1200,
    xga: 1460
};

export const max_width = {
    xs: 372,
    sm: 540,
    md: 720,
    lg: 960,
    xl: 1140,
    xga: 1400
};

export const styled_breakpoints = {
    breakpoints: {
        xs: grid.xs + "px",
        sm: grid.sm + "px",
        md: grid.md + "px",
        lg: grid.lg + "px",
        xl: grid.xl + "px",
        xga: grid.xga + "px"
    }
};
