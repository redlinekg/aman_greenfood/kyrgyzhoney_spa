export const contacts = [
    {
        id: "1",
        title: "Global",
        global: true,
        list: [
            {
                type: "phone",
                title: "+996 (312) 88 22 96"
            },
            {
                type: "email",
                title: "info@kyrgyzstanhoney.com"
            },
            {
                type: "text",
                title:
                    "270000, Kyrgyz, Sokuluk district, The village of  Military Antonovka,  Raatbek Sanatbaev street №70"
            }
        ]
    },
    {
        id: "2",
        title: "Sales department",
        global: false,
        list: [
            {
                type: "phone",
                title: "+996 (507) 09 77 77"
            },
            {
                type: "phone",
                title: "+996 (705) 17 28 82"
            },
            {
                type: "email",
                title: "sales@kyrgyzstanhoney.com"
            }
        ]
    },
    {
        id: "3",
        title: "Marketing department",
        global: false,
        list: [
            {
                type: "phone",
                title: "+996 (703) 13 29 20"
            },
            {
                type: "email",
                title: "marketing@kyrgyzstanhoney.com"
            }
        ]
    },
    {
        id: "4",
        title: "Working hours",
        global: false,
        list: [
            {
                type: "text",
                title: "Mon-Fri 9:00 -17:00"
            },
            {
                type: "text",
                title: "Sunday 10:00 -16:00"
            }
        ]
    }
];
