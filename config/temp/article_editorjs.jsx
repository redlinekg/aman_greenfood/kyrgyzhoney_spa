import image2 from "../../static/images/temp/blog/article2.jpg";
import image3 from "../../static/images/temp/blog/article3.jpg";

export const article_editorjs = {
    time: 1570049276548,
    blocks: [
        {
            type: "header",
            data: {
                text:
                    "Honey is a sweet liquid made by bees using the nectar from flowers. It is graded by color, with the clear, golden amber honey often fetching a higher retail price than the darker varieties.",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "The flavor of a particular type of honey will vary based on the types of flower from which the nectar was harvested."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Both raw and pasteurized forms of honey are available. Raw honey is removed from the hive and bottled directly, and as such will contain trace amounts of yeast, wax, and pollen. Consuming local raw honey is believed to help with seasonal allergies, due to repeated exposure to the pollen in the area. Pasteurized honey has been heated and processed to remove impurities."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey has high levels of monosaccharides, fructose, and glucose, and it contains about 70 to 80 percent sugar, which provides its sweetness. Honey also has antiseptic and antibacterial properties. Modern medical science has managed to find uses for honey in chronic wound management and combating infection.  "
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "This MNT Knowledge Center article includes a brief history of honey in traditional medicine and explains some of its potential health benefits."
            }
        },
        {
            type: "header",
            data: {
                text: "Fast facts on honey",
                level: 2
            }
        },
        {
            type: "list",
            data: {
                style: "unordered",
                items: [
                    "Honey is linked to wound-healing properties and antibacterial action.",
                    "It has been used in medicine for over 5,000 years.",
                    "Honey can replace sugar in meals, providing a healthier option. However, they can also add browning and excess moisture to a dish.",
                    "Do not give honey to children under 12 months old."
                ]
            }
        },
        {
            type: "header",
            data: {
                text: "Benefits",
                level: 2
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Modern science is finding evidence for many of the historical uses of honey."
            }
        },
        {
            type: "image",
            data: {
                file: {
                    url: image2
                },
                caption: "",
                withBorder: false,
                stretched: true,
                withBackground: false
            }
        },
        {
            type: "header",
            data: {
                text: "1) Healing wounds and burns",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "There have been some cases in which people have reported positive effects of using honey in treating wounds."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "A review published in The Cochrane Library indicated that honey might be able to help heal burns. The lead author of the study said that topical honey is cheaper than other interventions, notably oral antibiotics, which are often used and may have other deleterious side effects."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "However, there is a lack of evidence to fully support this claim. In fact, a study published in The Lancet Infectious Diseases concluded that applying medical-grade honey to the wounds of patients has no advantage over normal antibiotics among patients undergoing dialysis."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey should never be given to young infants as it can cause botulism, a rare but severe type of food poisoning."
            }
        },
        {
            type: "header",
            data: {
                text: "2) Reducing the duration of diarrhea",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "According to research-based reviews on honey, it has been shown to decrease the severity and duration of diarrhea. Honey also promotes increased potassium and water intake, which is particularly helpful when experiencing diarrhea."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Research that took place in Lagos, Nigeria suggests that honey has also shown the ability to block the actions of pathogens that commonly cause diarrhea."
            }
        },
        {
            type: "header",
            data: {
                text: "3) Preventing acid reflux",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Recent research has shown that honey can reduce the upward flow of stomach acid and undigested food by lining the esophagus and stomach."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "This has helped to reduce the risk of gastroesophageal reflux disease (GERD). GERD can cause inflammation, acid reflux, and heartburn."
            }
        },
        {
            type: "header",
            data: {
                text: "4) Fighting infections",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "In 2010, scientists from the Academic Medical Center at the University of Amsterdam reported in FASEB Journal that honey's ability to kill bacteria lies in a protein called defensin-1."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "A more recent study in the European Journal of Clinical Microbiology & Infectious Diseases showed that a certain type of honey, called Manuka honey, can help prevent the bacteria Clostridium difficile from settling in the body. C. difficile is known for causing severe diarrhea and sickness."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "<b>Some studies have revealed that Manuka honey may even be effective for the treatment of MRSA infections.</b>"
            }
        },
        {
            type: "quote",
            data: {
                text:
                    "Manuka and other honeys have been known to have wound healing and anti-bacterial properties for some time. But the way in which they act is still not known. If we can discover exactly how Manuka honey inhibits MRSA, it could be used more frequently as a first-line treatment for infections with bacteria that are resistant to many currently available antibiotics.",
                caption: "Dr. Jenkins concluded",
                alignment: "left"
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Manuka honey may even help reverse bacterial resistance to antibiotics, according to research presented in the journal Letters in Applied Microbiology. This type of honey showed action against Ureaplasma urealyticum, a bacteria that is resistant to many different antibiotics."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "A study published in the journal Pediatrics, which compared honey to placebo in helping children with a cough during the night, found that honey was superior. The researchers concluded:"
            }
        },
        {
            type: "quote",
            data: {
                text:
                    "Parents rated the honey products higher than the silan date extract for symptomatic relief of their children's nocturnal cough and sleep difficulty due to URI (upper respiratory infection). Honey may be a preferable treatment for cough and sleep difficulty associated with childhood URI.",
                alignment: "left"
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "In The Scientific World Journal, researchers provided data confirming that natural honey was as effective as a eusol antiseptic solution in reducing wound infections."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "There is a great deal of evidence supporting the use of honey as a remedy for infection."
            }
        },
        {
            type: "image",
            data: {
                file: {
                    url: image3
                },
                caption: "",
                withBorder: false,
                stretched: false,
                withBackground: false
            }
        },
        {
            type: "header",
            data: {
                text: "5) Relieving cold and cough symptoms",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "The World Health Organization (WHO) recommends honey as a natural cough remedy."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "The American Academy of Pediatrics also recognizes honey as a treatment for a cough."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "<b>However, they advise that honey is not suitable for children under the age of one year.</b>"
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "A 2007 study by Penn State College of Medicine suggested that honey reduced night-time coughing and improved sleep quality in children with upper respiratory infection to a greater degree than the cough medicine dextromethorphan."
            }
        },
        {
            type: "header",
            data: {
                text: "6) Replacing added sugar in the diet",
                level: 3
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey's sweet flavor makes it an ideal substitute for sugar in the diet."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Added sugar in the diet provides excess calories with no nutritional benefit. This can lead to an increased body weight, which comes with an increased risk of high blood pressure and diabetes."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey can be added to food and beverages to sweeten the taste without the negative health impact of added sugars. However, since honey is still a sweetener, it is important to remain mindful of how much honey being is used."
            }
        },
        {
            type: "header",
            data: {
                text: "Medicinal use",
                level: 2
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey has been used to treat a wide array of illnesses, ailments, and injuries."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "It can be mixed with other remedies and consumed or rubbed onto the skin. Practitioners of Ayurvedic medicine have attempted to use honey as a remedy for the following:"
            }
        },
        {
            type: "list",
            data: {
                style: "ordered",
                items: [
                    "stress",
                    "weakness",
                    "sleep disturbance",
                    "vision problems",
                    "bad breath",
                    "teething pain, in children over a year old",
                    "cough and asthma",
                    "hiccups",
                    "stomach ulcers",
                    "diarrhea and dysentery",
                    "vomiting"
                ]
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "While not all uses of honey are confirmed as effective, trying it as treatment will not make conditions any worse or cause harm."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Honey is sometimes touted as a cosmetic solution for cracked, dry, pimply, or clogged skin."
            }
        }
    ],
    version: "2.15.0"
};

export default article_editorjs;
