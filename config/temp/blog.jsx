import image_one from "../../static/images/temp/blog/one.jpg";
import image1 from "../../static/images/temp/blog/1.jpg";
import image2 from "../../static/images/temp/blog/2.jpg";
import image3 from "../../static/images/temp/blog/3.jpg";
import image4 from "../../static/images/temp/blog/4.jpg";
import image5 from "../../static/images/temp/blog/5.jpg";
import image6 from "../../static/images/temp/blog/6.jpg";

export const articles = [
    {
        image: image_one,
        title:
            "The Ministry of Agriculture commented on the mass death of bees in the regions",
        desk:
            "The honey and nuts gathering place is the picturesque lands of Jalal-Abad region. The vast territories of fruit and nut forests in the Arslanbob River Valley are the largest in the world, with an area of ​​over 600,000 hectares. Up to 1,500 tons of nuts are harvested every year in Arslanbob. For many years now, Arslanbob, which means “king of the forests” in Kyrgyz, has been a national reserve.",
        slug: "image_one",
        created_at: "2018-01-26 04:07:31"
    },
    {
        image: image1,
        title:
            "The Ministry of Agriculture commented on the mass death of bees in the regions",
        desk: "Получая",
        slug: "article1",
        created_at: "2018-01-26 04:07:31"
    },
    {
        image: image2,
        title:
            "Bees need to be protected for global food security: FAO opinion",
        desk: "Адвокатура",
        slug: "article2",
        created_at: "2018-02-26 04:07:31"
    },
    {
        image: image3,
        title:
            "On the convocation of the All-Russian Congress of representatives of the beekeeping industry",
        desk: "ноября",
        slug: "article3",
        created_at: "2018-03-26 04:07:31"
    },
    {
        image: image4,
        title:
            "Winter losses of bee colonies in Russia: interim results of Questionnaire 2017",
        desk: "Юридические",
        slug: "article4",
        created_at: "2018-04-26 04:07:31"
    },
    {
        image: image5,
        title:
            "The situation in the beekeeping of the Republic of Bashkortostan",
        desk: "Уважаемые",
        slug: "article5",
        created_at: "2018-05-26 04:07:31"
    },
    {
        image: image6,
        title: "Start of the resource Beepedia Encyclopedia of Beekeeping",
        desk: "Уважаемые",
        slug: "article",
        created_at: "2018-06-26 04:07:31"
    }
];
