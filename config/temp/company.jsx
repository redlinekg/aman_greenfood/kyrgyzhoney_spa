export const sub_menu = [
    {
        title: "About company",
        route: "page",
        slug: "about-company",
        active: true
    },
    {
        title: "Geography",
        route: "page",
        slug: "geography"
    },
    {
        title: "Laboratory",
        route: "page",
        slug: "laboratory"
    },
    {
        title: "Varieties of honey",
        route: "page",
        slug: "varieties-of-honey"
    },
    {
        title: "Honey properties",
        route: "page",
        slug: "honey-properties"
    }
];
