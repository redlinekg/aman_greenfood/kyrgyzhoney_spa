import product1_1 from "../../static/images/temp/catalog/500/1.1.png";
import product1_2 from "../../static/images/temp/catalog/500/1.2.png";
import product1_3 from "../../static/images/temp/catalog/500/1.3.png";
import product1_4 from "../../static/images/temp/catalog/500/1.4.png";
import product1_5 from "../../static/images/temp/catalog/500/1.5.png";

import product2_1 from "../../static/images/temp/catalog/500/2.1.png";
import product2_2 from "../../static/images/temp/catalog/500/2.2.png";
import product2_3 from "../../static/images/temp/catalog/500/2.3.png";
import product2_4 from "../../static/images/temp/catalog/500/2.4.png";
import product2_5 from "../../static/images/temp/catalog/500/2.5.png";
import product2_6 from "../../static/images/temp/catalog/500/2.6.png";
import product2_7 from "../../static/images/temp/catalog/500/2.7.png";
import product2_8 from "../../static/images/temp/catalog/500/2.8.png";

import product3_1 from "../../static/images/temp/catalog/500/3.1.png";
import product3_2 from "../../static/images/temp/catalog/500/3.2.png";
import product3_3 from "../../static/images/temp/catalog/500/3.3.png";
import product3_4 from "../../static/images/temp/catalog/500/3.4.png";
import product3_5 from "../../static/images/temp/catalog/500/3.5.png";
import product3_6 from "../../static/images/temp/catalog/500/3.6.png";

export const categories = [
    {
        id: "1",
        title: "Kyrgyz Honey",
        description:
            "The honey and nuts gathering place is the picturesque lands of Jalal-Abad region. The vast territories of fruit and nut forests in the Arslanbob River Valley are the largest in the world, with an area of ​​over 600,000 hectares. Up to 1,500 tons of nuts are harvested every year in Arslanbob. For many years now, Arslanbob, which means “king of the forests” in Kyrgyz, has been a national reserve.",
        products: [
            {
                slug: "product1",
                image: product1_1,
                title: "White Honey SainFoin",
                volume: "250/500г",
                description:
                    "Это жемчужно-белый эспарцетовый мед собранный вгорах Ат-Башинских, Алайку и Чон Алайских регионов Кыргызстана. Этот мед является монофлорным и главнаяего особенность втом, что он обладаетгипоаллергенными свойствами, чем и завоевал большое признание вмире. Вкус: нежный с молочным отеноком и удовлетворит запросы самых искушенных гурманов"
            },
            {
                slug: "product2",
                image: product1_2,
                title: "Angels' Gift Sage",
                volume: "250/500г",
                description:
                    "Является уникальным медом по показателям высокого диастазного числа. Данный мед является монофлорным и включаетвсебя цветы шалфея от37% и выше. Вкусовые показатели заметно выделяются от других видов меда и имеют прекрсный цветочный аромат. Собирается он в предгорьях сказочного заповедника Сары-Челек."
            },
            {
                slug: "product3",
                image: product1_3,
                title: "Angels' Gift Thyme",
                volume: "250/500г",
                description:
                    "Особенный представитель медового семейства. Основной букет в меде это Темьян. Его процентная доля в составе - 70%, что делает этот медуникальным, выделяя его в категорию Ргетит. Чтобы получить этот мед, необходимы особые климатические условия, время и место."
            },
            {
                slug: "product4",
                image: product1_4,
                title: "White Honey SainFoin",
                volume: "250/500г",
                description:
                    "Это наиболее душистый сорт меда потому, что в ней богатая медоносная флора из лекарственных растений и горных, лесных цветов. ForestHoney имеет высокие пищевые и лечебно - профилактические свойства. Пчеловодческие зоны расположены на высоте 988 метров над уровнем моря и выше, в Джалал-Абадской области. Данная область обладаетособенной флорой иее горы богаты на реликтовые и орехо-плодные леса."
            },
            {
                slug: "product5",
                image: product1_5,
                title: "Angels' Gift Sage",
                volume: "250/500г",
                description:
                    "Данный мед собирается в буферной зоне навысоте 2000 м. над уровнем моря, в горах Кара- Кулжинского и Сусамырских районов. Подобные условия позволяют получить мед высшего качества Продуктимеет насыщенный вкус с терпко-сладкими нотами."
            }
        ]
    },
    {
        id: "2",
        title: "Bal Azyk",
        description:
            "The honey and nuts gathering place is the picturesque lands of Jalal-Abad region. The vast territories of fruit and nut forests in the Arslanbob River Valley are the largest in the world, with an area of ​​over 600,000 hectares. Up to 1,500 tons of nuts are harvested every year in Arslanbob. For many years now, Arslanbob, which means “king of the forests” in Kyrgyz, has been a national reserve.",
        products: [
            {
                slug: "product6",
                image: product2_1,
                title: "Honey with Nut Mix",
                volume: "280g",
                description:
                    "Ароматный, цветочно-полифлорный мёд, обогащённый свежими и полезными орешками как: семена подсолнуха, арахис, ядра грецкого ореха, орехи кешью, ядра сладкого миндаля и нежной фисташки. Микс хрустящих орехов в сочетании нежно- сладким мёдом оставляют приятный привкус."
            },
            {
                slug: "product6",
                image: product2_2,
                title: "Honey with cashew",
                volume: "285g",
                description:
                    "Нежный и лёгкий контрастянтарно - полифлорного мёда с цельными плодами кешью. Такой тандем красоты на вкус очень приятный. Орех доминирует но, хорошо чувствуются нотки цветочного мёда."
            },
            {
                slug: "product7",
                image: product2_3,
                title: "Honey with pictahios",
                volume: "280g",
                description:
                    "Масса из ядер фисташек, залитого натуральным, цветочным, полифлорным мёдом, имеет пестрый, но красивый оттенок. Мягкий запах мёда улучшаеттонкий аромат фисташек."
            },
            {
                slug: "product8",
                image: product2_4,
                title: "Honey with almond",
                volume: "270g",
                description:
                    "Сочетание янтарного полифлорного мёда с красными оттенками миндаля создаюточень красивую композицию. Мягкий медовый запах стонкими горьковатыми нотами аромата миндаля."
            },
            {
                slug: "product9",
                image: product2_5,
                title: "Honey with walnut",
                volume: "280g",
                description:
                    "Из прозрачного, светло-янтарного мёда отчетливо видны крутые плоды грецкого ореха. Классическое сочетание ингридиентов создают насыщенный и вто же время мягкий дует вкусового кчества."
            },
            {
                slug: "product10",
                image: product2_6,
                title: "Honey with figs",
                volume: "300g",
                description:
                    "Мягкий, сушеный инжир в больших кусочках, сянтарным вкусом и со вкусом и сочетанием родственных цветов."
            },
            {
                slug: "product11",
                image: product2_7,
                title: "Honey with sunflower seeds",
                volume: "260g",
                description:
                    "Дуэт полифлорного мёда и семён подсолнуха по своему красив. Сладкий и приятный мёд с выраженным привкусом и ароматом жаренных семечек для любителей нежного вкуса."
            },
            {
                slug: "product12",
                image: product2_8,
                title: "Honey with peanut",
                volume: "280g",
                description:
                    "Очищенные орешки, залитые янтарным полифлорным мёдом, создают красивое цветовое сочетание. Ароматлегкой прожарки арахиса и сладкого цветочного мёда отличаются своим необыкновенным вкусом и ароматом."
            }
        ]
    },
    {
        id: "1",
        title: "Bal Plus",
        description:
            "The honey and nuts gathering place is the picturesque lands of Jalal-Abad region. The vast territories of fruit and nut forests in the Arslanbob River Valley are the largest in the world, with an area of ​​over 600,000 hectares. Up to 1,500 tons of nuts are harvested every year in Arslanbob. For many years now, Arslanbob, which means “king of the forests” in Kyrgyz, has been a national reserve.",
        products: [
            {
                slug: "product13",
                image: product3_1,
                title: "Honey with strawberry",
                volume: "300g",
                description:
                    "Нежнейший эспарцетовый мед с ароматным клубничным концентратом отличается своим необыкновенным вкусом и насышенным оттенком. Яркий аромат спелой клубники доминируетв дуэте."
            },
            {
                slug: "product14",
                image: product3_2,
                title: "Honey with peach",
                volume: "300g",
                description:
                    "Красивый оттенок концентрата персика ещё больше раскрывается в сочетании воздушного эспарцетового мёда мпие Ропеу. Смесь владеет неповторимым вкусом детства, а аромат привлекает всех любителей сладкого."
            },
            {
                slug: "product15",
                image: product3_3,
                title: "Honey with chocolate",
                volume: "300g",
                description:
                    "Шоколад в сочетании белого меда на вкус очень даже изысканный. В результате контраста этих компонентов получается тёмно-коричневая паста. Доминирует аромат шоколада с нотками мёда."
            },
            {
                slug: "product16",
                image: product3_4,
                title: "Honey with currant",
                volume: "300g",
                description:
                    "Дессертс лиловым оттенком из белого меда и концентратом смородины напоминает спелую полевую лаванду, а привкус воздушный и нежный."
            },
            {
                slug: "product17",
                image: product3_5,
                title: "Honey with lemon",
                volume: "300g",
                description:
                    "Янтарный цветочный мёд обладает неповторимым ярким ароматом лимона. На вкус довольно насыщенный за счёт концетрата цитрусового растения."
            },
            {
                slug: "product18",
                image: product3_6,
                title: "Honey with raspberries",
                volume: "300g",
                description:
                    "Воздушная кремообразная компазиция из смеси м/ПКе Попеу и концентрата спелой малины выделяется необыкновенно ярким ароматом и насыщенным оттенком. Доминирует вкус мёда снежными нотами малины."
            }
        ]
    }
];

export const cart = [
    {
        slug: "product1",
        image: product1_1,
        title: "White Honey SainFoin",
        volume: "250/500г"
    },
    {
        slug: "product2",
        image: product1_2,
        title: "Angels' Gift Sage",
        volume: "250/500г"
    },
    {
        slug: "product3",
        image: product1_3,
        title: "Angels' Gift Thyme",
        volume: "250/500г"
    },
    {
        slug: "product4",
        image: product1_4,
        title: "White Honey SainFoin",
        volume: "250/500г"
    },
    {
        slug: "product5",
        image: product1_5,
        title: "Angels' Gift Sage",
        volume: "250/500г"
    },
    {
        slug: "product6",
        image: product2_1,
        title: "Honey with Nut Mix",
        volume: "280g"
    },
    {
        slug: "product6",
        image: product2_2,
        title: "Honey with cashew",
        volume: "285g"
    },
    {
        slug: "product7",
        image: product2_3,
        title: "Honey with pictahios",
        volume: "280g"
    },
    {
        slug: "product8",
        image: product2_4,
        title: "Honey with almond",
        volume: "270g"
    },
    {
        slug: "product9",
        image: product2_5,
        title: "Honey with walnut",
        volume: "280g"
    },
    {
        slug: "product10",
        image: product2_6,
        title: "Honey with figs",
        volume: "300g"
    },
    {
        slug: "product11",
        image: product2_7,
        title: "Honey with sunflower seeds",
        volume: "260g"
    },
    {
        slug: "product12",
        image: product2_8,
        title: "Honey with peanut",
        volume: "280g"
    },
    {
        slug: "product13",
        image: product3_1,
        title: "Honey with strawberry",
        volume: "300g"
    },
    {
        slug: "product14",
        image: product3_2,
        title: "Honey with peach",
        volume: "300g"
    },
    {
        slug: "product15",
        image: product3_3,
        title: "Honey with chocolate",
        volume: "300g"
    },
    {
        slug: "product16",
        image: product3_4,
        title: "Honey with currant",
        volume: "300g"
    },
    {
        slug: "product17",
        image: product3_5,
        title: "Honey with lemon",
        volume: "300g"
    },
    {
        slug: "product18",
        image: product3_6,
        title: "Honey with raspberries",
        volume: "300g"
    }
];

export const index_products = [
    {
        slug: "product1",
        image: product1_1,
        title: "White Honey SainFoin",
        volume: "250/500г"
    },
    {
        slug: "product4",
        image: product1_4,
        title: "White Honey SainFoin",
        volume: "250/500г"
    },
    {
        slug: "product5",
        image: product1_5,
        title: "Angels' Gift Sage",
        volume: "250/500г"
    },
    {
        slug: "product7",
        image: product2_3,
        title: "Honey with pictahios",
        volume: "280g"
    },
    {
        slug: "product10",
        image: product2_6,
        title: "Honey with figs",
        volume: "300g"
    },
    {
        slug: "product13",
        image: product3_1,
        title: "Honey with strawberry",
        volume: "300g"
    },
    {
        slug: "product15",
        image: product3_3,
        title: "Honey with chocolate",
        volume: "300g"
    },
    {
        slug: "product16",
        image: product3_4,
        title: "Honey with currant",
        volume: "300g"
    },
    {
        slug: "product17",
        image: product3_5,
        title: "Honey with lemon",
        volume: "300g"
    },
    {
        slug: "product18",
        image: product3_6,
        title: "Honey with raspberries",
        volume: "300g"
    }
];
