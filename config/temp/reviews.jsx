import avatar1 from "../../static/images/temp/reviews/1.jpg";
import avatar2 from "../../static/images/temp/reviews/2.jpg";
import avatar3 from "../../static/images/temp/reviews/3.jpg";

const reviews = [
    {
        avatar: avatar1,
        name: "Leon Fedorovich",
        country: "Kiy Chu, China",
        icon: "instagram",
        link: "https://instagram.com/",
        title: "Uneasy barton seeing remark happen his has.",
        text: {
            time: 1570049276548,
            blocks: [
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Uneasy barton seeing remark happen his has. Am possible offering at contempt mr distance stronger an. Attachment excellence announcing or reasonable am on if indulgence. Exeter talked in agreed spirit no he unable do. Betrayed shutters in vicinity it unpacked in. In so impossible appearance considered mr. Mrs him left find are good."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Dashwood contempt on mr unlocked resolved provided of of. Stanhill wondered it it welcomed oh. Hundred no prudent he however smiling at an offence. If earnestly extremity he he propriety something admitting convinced ye. Pleasant in to although as if differed horrible. Mirth his quick its set front enjoy hoped had there. Who connection imprudence middletons too but increasing celebrated principles joy. Herself too improve gay winding ask expense are compact. New all paid few hard pure she."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design number winter see. Oh be me sure wise sons no. Piqued ye of am spirit regret. Stimulated discretion impossible admiration in particular conviction up."
                    }
                }
            ],
            version: "2.15.0"
        },
        created_at: "2019-06-04 24:00:00.000"
    },
    {
        avatar: avatar2,
        name: "Leon Fedorovich",
        country: "Kiy Chu, China",
        icon: "facebook",
        link: "https://instagram.com/",
        title: "Uneasy barton seeing remark happen his has.",
        text: {
            time: 1570049276548,
            blocks: [
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Uneasy barton seeing remark happen his has. Am possible offering at contempt mr distance stronger an. Attachment excellence announcing or reasonable am on if indulgence. Exeter talked in agreed spirit no he unable do. Betrayed shutters in vicinity it unpacked in. In so impossible appearance considered mr. Mrs him left find are good."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Dashwood contempt on mr unlocked resolved provided of of. Stanhill wondered it it welcomed oh. Hundred no prudent he however smiling at an offence. If earnestly extremity he he propriety something admitting convinced ye. Pleasant in to although as if differed horrible. Mirth his quick its set front enjoy hoped had there. Who connection imprudence middletons too but increasing celebrated principles joy. Herself too improve gay winding ask expense are compact. New all paid few hard pure she."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design number winter see. Oh be me sure wise sons no. Piqued ye of am spirit regret. Stimulated discretion impossible admiration in particular conviction up."
                    }
                }
            ],
            version: "2.15.0"
        },
        created_at: "2019-06-04 24:00:00.000"
    },
    {
        avatar: avatar3,
        name: "Leon Fedorovich",
        country: "Kiy Chu, China",
        link: "https://instagram.com/",
        title: "Uneasy barton seeing remark happen his has.",
        text: {
            time: 1570049276548,
            blocks: [
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Uneasy barton seeing remark happen his has. Am possible offering at contempt mr distance stronger an. Attachment excellence announcing or reasonable am on if indulgence. Exeter talked in agreed spirit no he unable do. Betrayed shutters in vicinity it unpacked in. In so impossible appearance considered mr. Mrs him left find are good."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "Dashwood contempt on mr unlocked resolved provided of of. Stanhill wondered it it welcomed oh. Hundred no prudent he however smiling at an offence. If earnestly extremity he he propriety something admitting convinced ye. Pleasant in to although as if differed horrible. Mirth his quick its set front enjoy hoped had there. Who connection imprudence middletons too but increasing celebrated principles joy. Herself too improve gay winding ask expense are compact. New all paid few hard pure she."
                    }
                },
                {
                    type: "paragraph",
                    data: {
                        text:
                            "At as in understood an remarkably solicitude. Mean them very seen she she. Use totally written the observe pressed justice. Instantly cordially far intention recommend estimable yet her his. Ladies stairs enough esteem add fat all enable. Needed its design number winter see. Oh be me sure wise sons no. Piqued ye of am spirit regret. Stimulated discretion impossible admiration in particular conviction up."
                    }
                }
            ],
            version: "2.15.0"
        },
        created_at: "2019-06-04 24:00:00.000"
    }
];

export default reviews;
