import image1 from "../../static/images/temp/catalog/500/1.png";
import image2 from "../../static/images/temp/catalog/500/2.png";
import background from "../../static/images/temp/index_slider_bg.jpg";

export const index_slider = [
    {
        background_image: background,
        sub_title: "Bal Azyk",
        title: "Honey With",
        title_two: "Nut Mix",
        image: image1,
        description:
            "And the multicomponent composition is similar to therapeutic balms or complex drugs. Polyfler honey contains a unique selection of vitamins and minerals, due to which the beekeeping product can be attributed to drugs with strong medicinal effects.",
        button_link: "/",
        button_text: "Open product"
    },
    {
        background_color: "#000",
        sub_title: "Kyrgyz Honey",
        title: "White Honey",
        title_two: "SainFoin",
        video: "/static/images/temp/index_slider_video2.mp4",
        description:
            "Polyfler honey contains a unique selection of vitamins and minerals, due to which the beekeeping product can be attributed to drugs with strong medicinal effects, and the multicomponent composition is similar to therapeutic balms or complex drugs."
    },
    {
        background_image: background,
        sub_title: "Bal Plus",
        title: "Honey with",
        title_two: "raspberries",
        image: image2,
        description:
            "Polyfler honey contains a unique selection of vitamins and minerals, due to which the beekeeping product can be attributed to drugs with strong medicinal effects, and the multicomponent composition is similar to therapeutic balms or complex drugs.",
        button_link: "/",
        button_text: "Open product"
    }
];

export default index_slider;
