export const product_editorjs = {
    time: 1570049276548,
    blocks: [
        {
            type: "paragraph",
            data: {
                text:
                    "Polyfler honey contains a unique selection of vitamins and minerals, due to which the beekeeping product can be attributed to drugs with strong medicinal effects, and the multicomponent composition is similar to therapeutic balms or complex drugs."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "Walnuts - a source of complete protein, B vitamins, tannins, iodine, potassium, carotenoids, β-carotene. Honey is a supplier of fructose, glucose, B, C, E, K vitamins, folic acid, A-carotene, and unsaturated omega-3 fatty acids."
            }
        },
        {
            type: "paragraph",
            data: {
                text:
                    "In natural honey, nuts get a phenomenal therapeutic and preventive dish: nutritious and delicious. Useful properties of walnuts with honey The combination of walnuts with honey is an organic, easily and completely (100%) digestible combination that effectively enhances immunity. Honey with nuts is recommended not only for office workers as “food for the mind”, but also for athletes for quick recovery of energy and ordinary people during periods of increased stress (stress, exams, trip, difficult project). Walnuts with honey in everyday diets help people get rid of anemia, serve as an excellent prevention of cardiovascular diseases, increase the elasticity of blood vessel walls, and relieve atherosclerotic plaques."
            }
        }
    ],
    version: "2.15.0"
};

export default product_editorjs;
