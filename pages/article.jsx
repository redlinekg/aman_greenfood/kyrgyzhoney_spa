import styled from "@emotion/styled";
import { getStore } from "kyrgyzhoney_mobx";
import { inject, observer } from "mobx-react";
import dynamic from "next/dynamic";
import React, { Component } from "react";
import { ArticleHeader, EditorJs, Meta } from "~/components";
import { HeaderPortal } from "~/components/header";
import { down, global } from "~/config/var";
import header_bg from "~/static/images/header/header_bg_5.jpg";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";

const HeaderNavigation = dynamic(
    () => import("~/components/header/components/HeaderNavigation"),
    { ssr: false }
);

@inject("article_store")
@observer
class ArticlePage extends Component {
    static propTypes = {
        ...contextPropTypes
    };

    static async getInitialProps({ query: { slug } }) {
        const article_store = getStore("article_store");
        await article_store.getOne(slug);
        return {
            namespacesRequired: []
        };
    }

    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues = () => {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({
            header_type: "sub",
            header_background_image: header_bg
        });
    };

    render() {
        const {
            article_store: {
                item: { prev_entity, next_entity },
                item
            }
        } = this.props;

        const META_IMAGE = item.meta_image
            ? item.meta_image.absolute_path
            : item.image?.absolute_path
            ? item.image?.absolute_path
            : null;

        return (
            <>
                <Meta
                    title={item?.title}
                    meta_title={item?.meta_title}
                    description={item?.meta_description}
                    og_image={META_IMAGE}
                    author={item?.author.title}
                    published_time={item?.created_at}
                    modified_time={item?.updated_at}
                />
                <SArticleBlock>
                    <HeaderPortal>
                        <HeaderNavigation
                            prev={
                                prev_entity
                                    ? {
                                          title: prev_entity.title,
                                          route: "article",
                                          params: { slug: prev_entity.slug }
                                      }
                                    : null
                            }
                            next={
                                next_entity
                                    ? {
                                          title: next_entity.title,
                                          route: "article",
                                          params: { slug: next_entity.slug }
                                      }
                                    : null
                            }
                        />
                    </HeaderPortal>
                    <ArticleHeader
                        title={item.title}
                        created_at={item.created_at}
                        image={item.image.url_path}
                        name={item.author.title}
                        avatar={item.author.avatar.url_path}
                    />
                    <EditorJs blocks_json={item.body} />
                </SArticleBlock>
            </>
        );
    }
}

const SArticleBlock = styled.div`
    padding-top: ${global.padding_small};
    ${down("md")} {
        padding-top: 0;
    }
    .SContentContainer {
        margin: 0 auto;
    }
`;

export default withLayout(ArticlePage);
