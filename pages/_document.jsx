import Document, { Head, Main, NextScript } from "next/document";

import Helmet from "react-helmet";
import React from "react";
import apple_touch_icon from "../static/images/fv/apple-touch-icon.png";
import favicon_16x16 from "../static/images/fv/favicon-16x16.png";
import favicon_32x32 from "../static/images/fv/favicon-32x32.png";
import safari_pinned_tab from "../static/images/fv/safari-pinned-tab.svg";

export default class MyDocument extends Document {
    static async getInitialProps(...args) {
        const documentProps = await super.getInitialProps(...args);
        return { ...documentProps, helmet: Helmet.renderStatic() };
    }

    get helmetHtmlAttrComponents() {
        return this.props.helmet.htmlAttributes.toComponent();
    }

    get helmetBodyAttrComponents() {
        return this.props.helmet.bodyAttributes.toComponent();
    }

    get helmetHeadComponents() {
        return Object.keys(this.props.helmet)
            .filter(el => el !== "htmlAttributes" && el !== "bodyAttributes")
            .map(el => this.props.helmet[el].toComponent());
    }

    render() {
        return (
            <html {...this.helmetHtmlAttrComponents}>
                <Head>
                    {this.helmetHeadComponents}
                    <meta
                        name="viewport"
                        content="width=device-width, minimum-scale=1, maximum-scale=1, initial-scale=1, user-scalable=no"
                    />
                    <meta
                        httpEquiv="content-type"
                        content="text/html; charset=utf-8"
                    />

                    <link
                        rel="apple-touch-icon"
                        sizes="180x180"
                        href={apple_touch_icon}
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="32x32"
                        href={favicon_32x32}
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="16x16"
                        href={favicon_16x16}
                    />
                    <link
                        rel="manifest"
                        href="/static/images/fv/manifest.json"
                    />
                    <link
                        rel="mask-icon"
                        href={safari_pinned_tab}
                        color="#1e5896"
                    />
                    <meta name="msapplication-TileColor" content="#ffffff" />
                    <meta
                        name="apple-mobile-web-app-title"
                        content="KG Honey"
                    />
                    <link
                        href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.12/css/lightgallery.min.css"
                        rel="stylesheet"
                    />
                    <meta name="application-name" content="KG Honey" />
                    <meta name="theme-color" content="#ffffff" />
                    {this.props.styleTags}
                </Head>
                <body {...this.helmetBodyAttrComponents}>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}
