import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { position } from "polished";
import { isBrowser } from "browser-or-node";

import { ArrowLeftIcon } from "~/components/icons";
import { Meta, Container } from "~/components";
import { withTranslation } from "~/i18n";
import { Link } from "~/routes";
import { colors, down } from "~/config/var";

@withTranslation(["common"])
class Error extends PureComponent {
    static propTypes = {
        t: PT.any.isRequired
    };
    static getInitialProps({ res, err }) {
        const statusCode = res ? res.statusCode : err ? err.statusCode : null;
        return { statusCode, namespacesRequired: ["common"] };
    }

    componentDidMount() {
        if (isBrowser) {
            const _video = document.getElementById("error_video");
            _video.play();
        }
    }

    render() {
        const { t } = this.props;
        return (
            <SError>
                <Meta title={t("error_title")} />
                <Container>
                    <SBlock>
                        <SVideo>
                            <video
                                id="error_video"
                                autoPlay
                                loop
                                muted
                                playsInline
                            >
                                <source
                                    src="/static/images/error/raspberries280.mp4"
                                    type="video/mp4"
                                />
                            </video>
                        </SVideo>
                        <SInfo>
                            <STitle>{t("error_title")}</STitle>
                            <SText>{t("error_text")}</SText>
                            <SButton>
                                <Link route="index">
                                    <a>
                                        <ArrowLeftIcon />
                                        {t("error_button")}
                                    </a>
                                </Link>
                            </SButton>
                        </SInfo>
                    </SBlock>
                </Container>
            </SError>
        );
    }
}

const SError = styled.div`
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: ${colors.black_one};
    ${down("md")} {
    }
    ${down("sm")} {
    }
`;

const SBlock = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    ${down("sm")} {
        display: block;
    }
`;

const SVideo = styled.div`
    position: relative;
    width: 300px;
    height: 300px;
    ${down("lg")} {
        width: 240px;
        height: 240px;
    }
    ${down("sm")} {
        width: 280px;
        height: 280px;
    }
    video {
        ${position("absolute", "0", null, null, "0")};
        width: 100%;
        height: 100%;
    }
`;

const SInfo = styled.div`
    padding-left: 2em;
    color: ${colors.white};
    ${down("sm")} {
        padding-left: 0;
        padding-top: 1.4em;
    }
`;
const STitle = styled.h1`
    font-size: 2em;
    margin: 0;
    font-weight: 700;
    margin-bottom: 0.2em;
    ${down("lg")} {
        font-size: 1.6em;
    }
    ${down("sm")} {
        font-size: 1.4em;
    }
`;
const SText = styled.p``;
const SButton = styled.div`
    a {
        display: flex;
        align-items: center;
        margin-top: 2em;
        color: ${colors.blue_light};
        font-weight: 500;
        ${down("lg")} {
            font-size: 0.9em;
        }
        ${down("sm")} {
            margin-top: 1em;
        }
        @media (hover) {
            &:hover {
                color: ${colors.blue};
            }
        }
        svg {
            width: 0.9em;
            height: 0.9em;
            margin-right: 0.4em;
        }
    }
`;

export default Error;
