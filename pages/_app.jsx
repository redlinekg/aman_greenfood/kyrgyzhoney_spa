import { css, Global } from "@emotion/core";
import axios from "axios";
import { isNode } from "browser-or-node";
import { ThemeProvider } from "emotion-theming";
import { createStores } from "kyrgyzhoney_mobx";
import { Provider } from "mobx-react";
import App from "next/app";
import React from "react";
import { LightgalleryProvider } from "react-lightgallery";
import reset from "styled-reset";
import { Layout, Meta, ModalCallme, ModalOrder } from "../components";
import { styled_breakpoints } from "../config/var";
import { appWithTranslation } from "../i18n";
import { FontsStyle, GlobalStyle, SlickStyle } from "../styles";
import { logger } from "../utils";
import LayoutProvider from "../utils/LayoutProvider";
import Error from "./_error";

let stores = null;

class MyApp extends App {
    static async getInitialProps({ Component, ctx, ctx: { req } }) {
        console.log("getInitialProps at _app");
        let pageProps = {};
        let errorCode = null;
        const promises = [];
        // create new store on each request (on nodejs)
        try {
            if (isNode) {
                stores = createStores({
                    snapshots: {},
                    cookie: req.headers.cookie,
                    logger,
                    axios,
                    baseURL: process.env.API_URL
                });
            }
            if (Component.getInitialProps) {
                promises.push(Component.getInitialProps(ctx));
            } else {
                promises.push(Promise.resolve({}));
            }
            const settings_store = stores.getStore("settings_store");
            if (!settings_store.is_ready) {
                promises.push(settings_store.getSettings());
            }
            const categories_store = stores.getStore("categories_store");
            promises.push(
                categories_store.getList({
                    qparams: {
                        join: [
                            {
                                field: "products",
                                eager: true,
                                alias: "products"
                            },
                            {
                                field: "products.images",
                                eager: true
                            },
                            {
                                field: "products.image",
                                eager: true
                            }
                        ]
                    }
                })
            );
            [pageProps] = await Promise.all(promises);
        } catch (error) {
            logger.error(error);
            if (error.response) {
                errorCode = error.response.status;
            } else {
                errorCode = 500;
            }
        }
        return {
            pageProps,
            errorCode,
            stores: stores.toJS(),
            helmetContext: {}
        };
    }

    constructor(props) {
        super(props);
        if (!stores) {
            stores = createStores({
                snapshots: props.stores,
                logger,
                axios,
                baseURL: "/"
            });
        }
    }

    render() {
        const { Component, errorCode, pageProps } = this.props;
        return (
            <>
                <Provider {...stores.getStores()}>
                    <Meta title="KyrgyzHoney" part="" />
                    <ThemeProvider theme={styled_breakpoints}>
                        <LayoutProvider>
                            {errorCode ? (
                                <Error statusCode={errorCode} />
                            ) : (
                                <LightgalleryProvider
                                    lightgallerySettings={{
                                        download: false,
                                        thumbnail: false,
                                        videojs: true
                                    }}
                                >
                                    <>
                                        <div id="global_content">
                                            <Layout
                                                //header
                                                header_background_image={
                                                    undefined
                                                }
                                                header_background_pattern={
                                                    undefined
                                                }
                                                header_sub_menu={undefined}
                                                header_navigation={undefined}
                                                //global
                                                show_content_padding={undefined}
                                                dark_theme={undefined}
                                                //footer
                                                show_footer_margin={undefined}
                                                show_footer={undefined}
                                            >
                                                <Component {...pageProps} />
                                            </Layout>
                                        </div>
                                        <div id="modal_content">
                                            <ModalOrder />
                                            <ModalCallme />
                                        </div>
                                    </>
                                </LightgalleryProvider>
                            )}
                            <SdocumentStyle />
                        </LayoutProvider>
                    </ThemeProvider>
                </Provider>
            </>
        );
    }
}

const SdocumentStyle = () => (
    <Global
        styles={css`
            ${reset};
            ${FontsStyle};
            ${GlobalStyle};
            ${SlickStyle};
        `}
    />
);

export default appWithTranslation(MyApp);
