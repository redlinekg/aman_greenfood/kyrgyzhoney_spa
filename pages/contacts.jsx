import React, { PureComponent } from "react";
import styled from "@emotion/styled";
import PT from "prop-types";
import { inject, observer } from "mobx-react";
import { getStore } from "kyrgyzhoney_mobx";

import { withTranslation } from "~/i18n";
import {
    ContactsMap,
    ContactsList,
    Meta,
    Container,
    Title,
    Above,
    Below
} from "~/components";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";
import { global, colors, grid, down } from "~/config/var";

@inject("categories_store", "settings_store")
@observer
class ContactsPage extends PureComponent {
    static propTypes = {
        ...contextPropTypes,
        settings_store: PT.object.isRequired,
        i18n: PT.any
    };
    static async getInitialProps() {
        const contacts_store = getStore("contacts_store");
        await contacts_store.getList();
        return {
            namespacesRequired: ["common"]
        };
    }
    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues = () => {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({ show_footer: false, show_content_padding: false });
    };

    render() {
        const { settings_store, i18n } = this.props;

        let meta_title = settings_store.RU_CONTACTS_META_TITLE;
        let meta_desc = settings_store.RU_CONTACTS_META_DESC;

        if (i18n.language === "en") {
            meta_title = settings_store.EN_CONTACTS_META_TITLE;
            meta_desc = settings_store.EN_CONTACTS_META_DESC;
        } else if (i18n.language === "kg") {
            meta_title = settings_store.KG_CONTACTS_META_TITLE;
            meta_desc = settings_store.KG_CONTACTS_META_DESC;
        } else if (i18n.language === "cn") {
            meta_title = settings_store.CN_CONTACTS_META_TITLE;
            meta_desc = settings_store.CN_CONTACTS_META_DESC;
        }

        return (
            <>
                <Meta title={meta_title} description={meta_desc} />
                <SContactsPage>
                    <ContactsMap />
                    <SBlock>
                        <Container>
                            <STitle>
                                <Title>{meta_title}</Title>
                            </STitle>
                        </Container>
                        <Above from={grid.md}>
                            <Container>
                                <ContactsList />
                            </Container>
                        </Above>
                        <Below from={grid.sm}>
                            <ContactsList />
                        </Below>
                    </SBlock>
                </SContactsPage>
            </>
        );
    }
}

const SContactsPage = styled.div``;
const SBlock = styled.div`
    padding-bottom: ${global.padding_large};
    ${down("sm")} {
        padding-bottom: 0;
    }
    .ContainerBlock {
        pointer-events: none;
    }
`;
const STitle = styled.div`
    position: relative;
    z-index: 3;
    margin-bottom: 1.6em;
    text-shadow: 1px 1px 0 ${colors.white};
    font-size: 0.7em;
    margin-top: 2em;
    pointer-events: all;
    ${down("md")} {
        font-size: 0.9em;
        margin-top: 2em;
        margin-bottom: 0;
    }
`;

export default withTranslation("common")(withLayout(ContactsPage));
