import React, { Component } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { getStore } from "kyrgyzhoney_mobx";
import { inject, observer } from "mobx-react";
import { isNode } from "browser-or-node";

import {
    Container,
    Loader,
    Meta,
    PlainGrid,
    PostCard,
    PostCardBig,
    Title
} from "~/components/index";
import { withTranslation } from "~/i18n";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";
import { down } from "~/config/var";

import header_bg from "~/static/images/header/header_bg_5.jpg";

@inject("articles_store", "settings_store")
@observer
class Blog extends Component {
    static propTypes = {
        ...contextPropTypes,
        t: PT.any.isRequired,
        articles_store: PT.object.isRequired,
        settings_store: PT.object.isRequired,
        i18n: PT.any
    };
    static async getInitialProps() {
        const articles_store = getStore("articles_store");
        if (isNode) {
            await articles_store.getList();
        } else {
            articles_store.getList();
        }
        return {
            namespacesRequired: ["blog"]
        };
    }
    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues() {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({
            header_background_image: header_bg
        });
    }
    render() {
        const {
            t,
            articles_store: { objects, is_loading },
            settings_store,
            i18n
        } = this.props;

        let meta_title = settings_store.RU_BLOG_META_TITLE;
        let meta_desc = settings_store.RU_BLOG_META_DESC;

        if (i18n.language === "en") {
            meta_title = settings_store.EN_BLOG_META_TITLE;
            meta_desc = settings_store.EN_BLOG_META_DESC;
        } else if (i18n.language === "kg") {
            meta_title = settings_store.KG_BLOG_META_TITLE;
            meta_desc = settings_store.KG_BLOG_META_DESC;
        } else if (i18n.language === "cn") {
            meta_title = settings_store.CN_BLOG_META_TITLE;
            meta_desc = settings_store.CN_BLOG_META_DESC;
        }

        return (
            <>
                <Meta title={meta_title} description={meta_desc} />
                <Container>
                    <SBlock>
                        <Title description={t("blog_desc")}>
                            {t("blog_title")}
                        </Title>
                        <SFavorites>
                            {objects.slice(0, 1).map((l, idx) => (
                                <PostCardBig key={idx} publication={l} />
                            ))}
                        </SFavorites>
                        <PlainGrid xga={2} xl={2} lg={2} md={2} sm={1}>
                            {objects.slice(1, 7).map((l, idx) => (
                                <PostCard key={idx} publication={l} />
                            ))}
                        </PlainGrid>
                    </SBlock>
                    {is_loading ? <Loader size="large" type="block" /> : null}
                </Container>
            </>
        );
    }
}

const SBlock = styled.div`
    max-width: 900px;
    margin: 0 auto;
`;
const SFavorites = styled.div`
    margin: 0 0 4em;
    ${down("sm")} {
        margin: 0 0 2em;
    }
`;

export default withTranslation("common")(withLayout(Blog));
