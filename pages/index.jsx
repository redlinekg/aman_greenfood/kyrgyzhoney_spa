import React, { Component } from "react";
import PT from "prop-types";
import { inject, observer } from "mobx-react";
import styled from "@emotion/styled";
import dynamic from "next/dynamic";
import { getStore } from "kyrgyzhoney_mobx";

import {
    Meta,
    IndexProducts,
    IndexGeography,
    IndexBlog,
    IndexReviews
} from "~/components";
import { HeaderPortal } from "~/components/header";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";
import { withTranslation } from "~/i18n";

const DynamicIndexSlider = dynamic(
    () => import("~/components/index/IndexSlider"),
    { ssr: false }
);

@inject("settings_store")
@observer
class IndexPage extends Component {
    static propTypes = {
        ...contextPropTypes,
        settings_store: PT.object.isRequired,
        i18n: PT.any
    };
    static async getInitialProps() {
        const slides_store = getStore("slides_store");
        const products_store = getStore("products_store");
        const reviews_store = getStore("reviews_store");
        const articles_store = getStore("articles_store");
        const promises = [
            slides_store.getList(),
            products_store.getList({
                qparams: {
                    search: {
                        pin: true
                    }
                }
            }),
            reviews_store.getList(),
            articles_store.getList({
                qparams: {
                    limit: 3
                }
            })
        ];
        await Promise.all(promises);
        return {
            namespacesRequired: ["common"]
        };
    }
    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues() {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({
            header_type: "index",
            show_content_padding: false
        });
    }

    render() {
        const { settings_store, i18n } = this.props;

        let meta_title = settings_store.RU_INDEX_META_TITLE;
        let meta_desc = settings_store.RU_INDEX_META_DESC;

        if (i18n.language === "en") {
            meta_title = settings_store.EN_INDEX_META_TITLE;
            meta_desc = settings_store.EN_INDEX_META_DESC;
        } else if (i18n.language === "kg") {
            meta_title = settings_store.KG_INDEX_META_TITLE;
            meta_desc = settings_store.KG_INDEX_META_DESC;
        } else if (i18n.language === "cn") {
            meta_title = settings_store.CN_INDEX_META_TITLE;
            meta_desc = settings_store.CN_INDEX_META_DESC;
        }

        return (
            <>
                <Meta title={meta_title} description={meta_desc} part="" />
                <SBlock>
                    <HeaderPortal>
                        <DynamicIndexSlider />
                    </HeaderPortal>
                    <IndexProducts />
                    <IndexGeography />
                    <IndexReviews />
                    <IndexBlog />
                </SBlock>
            </>
        );
    }
}

const SBlock = styled.div``;

export default withTranslation("common")(withLayout(IndexPage));
