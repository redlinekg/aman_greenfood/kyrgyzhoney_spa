import styled from "@emotion/styled";
import { getStore } from "kyrgyzhoney_mobx";
import { inject, observer } from "mobx-react";
import PT from "prop-types";
import React, { PureComponent } from "react";
import {
    Container,
    EditorJs,
    Meta,
    OpenModal,
    ProductCartAdder,
    ProductGallery,
    Title,
    ModalPortal,
    ModalOneClick
} from "~/components";
import { colors, down } from "~/config/var";
import { withTranslation } from "~/i18n";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";

@withLayout
@withTranslation(["common"])
@inject("product_store", "cart_store")
@observer
class ProductPage extends PureComponent {
    static propTypes = {
        ...contextPropTypes,
        product_store: PT.object.isRequired,
        cart_store: PT.object.isRequired
    };
    static async getInitialProps({ query: { slug } }) {
        const product_store = getStore("product_store");
        await product_store.getOne(slug);
        return {
            namespacesRequired: ["common"]
        };
    }

    state = {
        selected_count: 10
    };

    componentDidMount() {
        this.setLayoutValues();
    }

    setSelectedCount = value => {
        this.setState({
            selected_count: value
        });
    };

    setLayoutValues() {
        const { changeLayoutValue } = this.props;
        const header_image = this.props.product_store.item.header_background
            .absolute_path;
        changeLayoutValue({
            header_background_image: header_image,
            header_background_pattern: true
        });
    }

    addProductToCart = count => {
        const {
            product_store: {
                item: { id, setCartCount }
            },
            cart_store: { getById }
        } = this.props;
        const already_added = getById(id);
        setCartCount(already_added ? already_added.quantity + count : count);
    };

    render() {
        const {
            t,
            product_store: {
                item: { title, body, properties, description, images, video },
                item
            }
        } = this.props;
        const { selected_count } = this.state;

        const META_IMAGE = item.meta_image
            ? item.meta_image.absolute_path
            : item.image?.absolute_path
            ? item.image?.absolute_path
            : null;

        return (
            <>
                <Meta
                    title={item?.title}
                    meta_title={item?.meta_title}
                    description={item?.meta_description}
                    og_image={META_IMAGE}
                    published_time={item?.created_at}
                    modified_time={item?.updated_at}
                />
                <Container>
                    <SBlock>
                        <SGallery>
                            <ProductGallery images={images} video={video} />
                        </SGallery>
                        <SContent>
                            <Title text={title}>{title}</Title>
                            <SDescription>{description}</SDescription>
                            <SInfo>
                                <SActions>
                                    <SActionsTop>
                                        <ProductCartAdder
                                            onAdd={this.addProductToCart}
                                            onChange={this.setSelectedCount}
                                        />
                                    </SActionsTop>
                                    <ModalPortal key={3333}>
                                        <ModalOneClick
                                            quantity={selected_count}
                                        />
                                    </ModalPortal>
                                    <OpenModal name="one_click">
                                        <SOneClick>
                                            {t("product_one_click")}
                                        </SOneClick>
                                    </OpenModal>
                                </SActions>
                                <SProperties>
                                    <SPropertiesTitle>
                                        {t("product_properties_title")}
                                    </SPropertiesTitle>
                                    {properties.map(property => (
                                        <SPropertiesBlock key={property.id}>
                                            <SProteinsType>
                                                {property.type}
                                            </SProteinsType>
                                            <SProteinsValue>
                                                {property.value}
                                            </SProteinsValue>
                                        </SPropertiesBlock>
                                    ))}
                                </SProperties>
                            </SInfo>
                            {body ? (
                                <SBody>
                                    <EditorJs
                                        blocks_json={body}
                                        container_show={false}
                                    />
                                </SBody>
                            ) : null}
                        </SContent>
                    </SBlock>
                </Container>
            </>
        );
    }
}

const SBlock = styled.div`
    display: flex;
    ${down("md")} {
        display: block;
    }
`;
const SGallery = styled.div`
    width: 30%;
    ${down("lg")} {
        width: 40%;
    }
    ${down("md")} {
        width: 100%;
    }
`;
const SContent = styled.div`
    padding-left: 2em;
    width: 70%;
    ${down("lg")} {
        width: 60%;
    }
    ${down("md")} {
        padding-left: 0;
        width: 100%;
    }
    ${down("sm")} {
        margin-top: 1em;
        h1 {
            font-size: 1.5em;
        }
    }
`;
const SDescription = styled.div`
    margin-top: 0.5em;
`;
const SInfo = styled.div`
    display: flex;
    align-items: flex-end;
    margin-top: 1.5em;
    border: 1px solid ${colors.grey_light4};
    ${down("md")} {
        display: block;
    }
`;
//
const SActions = styled.div`
    height: 100%;
    text-align: center;
`;
const SActionsTop = styled.div`
    border-bottom: 1px solid ${colors.grey_light4};
    padding: 1em 1em 0.8em;
`;

const SOneClick = styled.div`
    color: ${colors.gold};
    padding: 0.8em 1em;
    cursor: pointer;
    @media (hover) {
        &:hover {
            color: ${colors.blue};
        }
    }
`;
//
const SProperties = styled.div`
    padding: 1em;
    border-left: 1px solid ${colors.grey_light4};
`;
const SPropertiesTitle = styled.div`
    font-weight: 600;
    font-size: 1.1em;
    margin-bottom: 0.5em;
`;
const SPropertiesBlock = styled.div`
    /* box-shadow: 0 1px 0 0 ${colors.grey_light4}; */
    border-bottom: 1px dotted ${colors.grey_light4};
    margin-bottom: 0.4em;
    display: flex;
    align-items: center;
    justify-content: space-between;
    &:last-of-type {
        margin-bottom: 0;
    }
`;
const SProteinsType = styled.div`
    margin-right: 3em;
    font-weight: 500;
`;
const SProteinsValue = styled.div``;
const SBody = styled.div`
    margin-top: 1.5em;
`;

export default ProductPage;
