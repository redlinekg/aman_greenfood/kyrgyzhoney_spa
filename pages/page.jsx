import styled from "@emotion/styled";
import { getStore } from "kyrgyzhoney_mobx";
import { inject, observer } from "mobx-react";
import dynamic from "next/dynamic";
import PT from "prop-types";
import React, { PureComponent } from "react";
import { EditorJs, Meta, Title } from "~/components";
import { ContentContainer } from "~/components/editorjs_render";
import { HeaderPortal } from "~/components/header";
import header_bg from "~/static/images/header/header_bg_4.jpg";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";

const HeaderSubMenu = dynamic(
    () => import("~/components/header/components/HeaderSubMenu"),
    { ssr: false }
);

@inject("page_store", "pages_store")
@observer
class Page extends PureComponent {
    static propTypes = {
        page_store: PT.object.isRequired,
        ...contextPropTypes
    };

    static async getInitialProps({ query: { slug } }) {
        const page_store = getStore("page_store");
        const pages_store = getStore("pages_store");
        const promises = [
            page_store.getOne(slug),
            pages_store.getList({
                qparams: {
                    filter: [
                        {
                            field: "is_about_company",
                            operator: "eq",
                            value: true
                        }
                    ]
                }
            })
        ];
        await Promise.all(promises);
        return {
            namespacesRequired: []
        };
    }

    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues = () => {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({
            header_background_image: header_bg
        });
    };

    render() {
        const {
            page_store: { item },
            pages_store: { objects }
        } = this.props;
        return (
            <>
                <Meta
                    title={item?.title}
                    meta_title={item?.meta_title}
                    description={item?.meta_description}
                    og_image={item?.meta_image?.absolute_path}
                />
                <HeaderPortal>
                    <HeaderSubMenu
                        sub_menu={objects.map(page => ({
                            route: "page",
                            params: { slug: page.slug },
                            title: page.title
                        }))}
                        fixed={true}
                    />
                </HeaderPortal>
                <SPage>
                    <ContentContainer show={true}>
                        <Title>{item.title}</Title>
                    </ContentContainer>
                    <EditorJs blocks_json={item.body} show_max_width />
                </SPage>
            </>
        );
    }
}

const SPage = styled.div`
    padding-top: 1em;
`;

export default withLayout(Page);
