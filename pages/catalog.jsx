import React, { PureComponent } from "react";
import PT from "prop-types";
import styled from "@emotion/styled";
import { inject, observer } from "mobx-react";
import dynamic from "next/dynamic";

import { PricelistIcon } from "~/components/icons";
import { contextPropTypes, withLayout } from "~/utils/LayoutProvider";
import { HeaderPortal } from "~/components/header";
import { Category, Container, Meta, Title } from "~/components/index";
import { withTranslation } from "~/i18n";
import { global, colors, down } from "~/config/var";
const HeaderSubMenu = dynamic(
    () => import("~/components/header/components/HeaderSubMenu"),
    { ssr: false }
);

import header_bg from "~/static/images/header/header_bg_5.jpg";

@inject("categories_store", "settings_store")
@observer
class Catalog extends PureComponent {
    static propTypes = {
        ...contextPropTypes,
        categories_store: PT.object.isRequired,
        settings_store: PT.object.isRequired,
        t: PT.any.isRequired,
        i18n: PT.any
    };
    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
    componentDidMount() {
        this.setLayoutValues();
    }

    setLayoutValues() {
        const { changeLayoutValue } = this.props;
        changeLayoutValue({
            header_background_image: header_bg
        });
    }
    render() {
        const {
            t,
            categories_store: { objects },
            settings_store,
            i18n
        } = this.props;

        let meta_title = settings_store.RU_CATALOG_META_TITLE;
        let meta_desc = settings_store.RU_CATALOG_META_DESC;

        if (i18n.language === "en") {
            meta_title = settings_store.EN_CATALOG_META_TITLE;
            meta_desc = settings_store.EN_CATALOG_META_DESC;
        } else if (i18n.language === "kg") {
            meta_title = settings_store.KG_CATALOG_META_TITLE;
            meta_desc = settings_store.KG_CATALOG_META_DESC;
        } else if (i18n.language === "cn") {
            meta_title = settings_store.CN_CATALOG_META_TITLE;
            meta_desc = settings_store.CN_CATALOG_META_DESC;
        }

        return (
            <>
                <Meta title={meta_title} description={meta_desc} />
                <HeaderPortal key={222}>
                    <HeaderSubMenu
                        sub_menu={objects.map(category => ({
                            route: "page",
                            params: { slug: category.slug },
                            title: category.title
                        }))}
                        fixed={true}
                        go_to
                    ></HeaderSubMenu>
                </HeaderPortal>
                <SCatalog>
                    <Container>
                        <Title
                            description={t("catalog_desc")}
                            children_links={
                                <SPricelist
                                    href={settings_store.PRICELIST}
                                    target="_blank"
                                >
                                    <SPricelistIcon>
                                        <PricelistIcon />
                                    </SPricelistIcon>
                                    <SPricelistText>
                                        {t("download_pricelist")}
                                    </SPricelistText>
                                </SPricelist>
                            }
                        >
                            {t("catalog_title")}
                        </Title>

                        {objects.map(category => (
                            <Category key={category.id} category={category} />
                        ))}
                    </Container>
                </SCatalog>
            </>
        );
    }
}

const SCatalog = styled.div`
    padding: ${global.padding_small} 0 0;
`;

const SPricelist = styled.a`
    position: relative;
    color: ${colors.black};
    display: flex;
    align-items: center;
    font-size: 1.1em;
    margin-bottom: 1em;
    transition: all 0.2s;
    @media (hover) {
        &:hover {
            color: ${colors.black};
        }
    }
    ${down("md")} {
        margin-bottom: 0.6em;
    }
    ${down("sm")} {
        font-size: 0.9em;
    }
`;
const SPricelistIcon = styled.div`
    svg {
        width: 1.8em;
        height: 1.8em;
        color: ${colors.yellow_light};
        transition: all 0.2s;
        ${SPricelist}:hover & {
            color: ${colors.blue_light3};
        }
    }
`;
const SPricelistText = styled.div`
    padding-left: 0.4em;
    font-weight: 600;
`;

export default withTranslation("common")(withLayout(Catalog));
