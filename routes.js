const nextRoutes = require("@vlzh/next-routes");
const { Link, Router } = require("./i18n");

const routes = (module.exports = nextRoutes.default({
    Link,
    Router,
    hrefCorrector: href => {
        if (
            href.startsWith("/ru") ||
            href.startsWith("/en") ||
            href.startsWith("/kg") ||
            href.startsWith("/cn")
        ) {
            return href.slice(3);
        }
        return href;
    }
}));

routes
    .add({ name: "index", pattern: "/", page: "index" })
    .add({ name: "blog", pattern: "/blog", page: "blog" })
    .add({ name: "article", pattern: "/article/:slug", page: "article" })
    .add({ name: "catalog", pattern: "/catalog", page: "catalog" })
    .add({ name: "product", pattern: "/product/:slug", page: "product" })
    .add({ name: "contacts", pattern: "/contacts", page: "contacts" })
    .add({ name: "page", pattern: "/:slug", page: "page" });
